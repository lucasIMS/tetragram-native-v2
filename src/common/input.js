import React from 'react';
import { View, Text, StyleSheet, Image, Dimensions, TextInput, Platform } from 'react-native';

const { width } = Dimensions.get('window');

const Input = (props) => {
    return(
        <View>
            <TextInput 
                style={[styles.input, props.additionalStyle]}
                underlineColorAndroid="transparent"
                placeholder={props.placeholder}
                placeholderTextColor="gray"
                onChangeText={props.onChangeText}
                value={props.value}
                ref={props.reference}
                editable={props.editable}
                minHeight={props.minHeight}
                numberOfLines={props.numberOfLines}
                multiline={props.multiline}
                keyboardType={props.keyboardType}
                returnKeyType={props.returnKeyType}
                autoCapitalize="none"
                maxLength={props.maxLength}
                autoCorrect={false}
                autoFocus={props.autoFocus}
                onSubmitEditing={props.onSubmitEditing}
            />
            {props.source ? 
            <Image resizeMode="contain" source={props.source} style={styles.icon}/>
            : null }
        </View>
    )
}

const styles = StyleSheet.create({
    input: {
        width: width - 30,
        marginHorizontal: 15,
        marginTop: Platform.OS == 'ios' ? 15 : 5,
        paddingVertical: Platform.OS == 'ios' ? 8 : 5,
        borderBottomWidth: 1.5,
        borderBottomColor: '#E5E5E5',
        position: 'relative',
        paddingRight: 30,
        fontSize: 12,
        fontWeight: '500'
    },
    icon: {
        position: 'absolute',
        right: 25,
        height: 18,
        width: 18,
        bottom: 8
    }
})

export default Input;