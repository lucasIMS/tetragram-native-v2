import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TextInput, Animated, Platform } from 'react-native';

class SlideInput extends Component {
    state = {
        text: new Animated.Value(30),
        textSize: Platform.OS == 'ios' ? -1 : 0,
    }
    enimation = () => {
        this.setState({ textSize: 13 })
        Animated.timing(
            this.state.text,
            {
                toValue: 0,
                duration: 200,
            }
        ).start();

    }

    hide = () => {
        if (!this.state.email) {
            
             Animated.timing(
                this.state.text,
                {
                    toValue: 30,
                    duration: 200,
                }
            ).start();
            setTimeout(() => {this.setState({ textSize: Platform.OS == 'ios' ? -1 : 0 })}, 150)
            
        }
    }
    render() {
        let {inputName} = this.props;
        return (
            <View style={styles.inputView}>
                <Animated.Text style={[styles.text, { fontSize: this.state.textSize, top: this.state.text }]}>
                    {inputName}
                </Animated.Text>
                <TextInput
                    style={styles.textInput}
                    onFocus={this.enimation}
                    placeholder={this.state.textSize == (Platform.OS == 'ios' ? -1 : 0) ? inputName : ""}
                    onBlur={this.hide}
                    onChangeText={(email) => this.setState({ email })}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    textInput: {
        width: '90%',
        alignSelf: 'center',
        borderBottomWidth: 1,
        fontSize: 18,
        // backgroundColor: 'skyblue',
        paddingVertical: 10
    },
    text: {
        color: 'gray',
        paddingLeft: '5%'
    },
    inputView: {
        height: 60,
        // backgroundColor: 'red',
        flexDirection: 'column',
        justifyContent: 'space-between'
    }
})

export default SlideInput;