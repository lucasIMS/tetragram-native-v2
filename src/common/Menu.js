import React from 'react';
import PropTypes from 'prop-types';
import {
    Dimensions,
    StyleSheet,
    ScrollView,
    View,
    Image,
    Text,
    TouchableOpacity
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { removeToken,onShare } from '../services/utils';
import { LoginManager, AccessToken } from "react-native-fbsdk";
import { GoogleSignin, statusCodes } from 'react-native-google-signin';
import FastImage from 'react-native-fast-image'

const window = Dimensions.get('window');

const styles = StyleSheet.create({
    menu: {
        flex: 1,
        width: (window.width * 2) / 3,
        height: window.height,
        backgroundColor: 'white'
    },
    profileArea: {
        width: '100%',
        paddingTop: '20%',
        paddingBottom: 20,
        backgroundColor: '#0196E9',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    profileIcon: {
        height: 100,
        width: 100,
        borderRadius: 50,
        alignSelf: 'center'
    },
    titleText: {
        fontSize: 17,
        color: 'white',
        fontWeight: '600',
        marginTop: 5
    },
    editProfileText: {
        fontSize: 12,
        color: 'white',
        fontWeight: '500',
        marginTop: 2
    },
    itemView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        // margin:'10%',
        marginTop: 30,
        width: '100%'
    },
    menuIcon: {
        height: 18,
        width: 22,
        marginLeft: '10%'
    },
    menuText: {
        marginLeft: '10%'
    }
});

function getUserName(name) {
    if (!name) return ""
    let changeName = name.split(' ')
    let correctName = changeName.map(x => {
        return x[0].toUpperCase() + x.slice(1, x.length)
    })
    return correctName.join(' ')
}

const Menu = (props) => {
    let imageUrl = props.userImage
    return (
        <ScrollView scrollsToTop={false} style={styles.menu}>
            <View style={styles.profileArea}>
            {imageUrl ?  <FastImage
        style={styles.profileIcon}
        source={{
            uri: imageUrl,
            priority: FastImage.priority.normal,
        }}
        resizeMode={FastImage.resizeMode.contain}
    /> :  <FastImage
    style={styles.profileIcon}
    source={{
        uri: 'http://simple4marry.com/assets/extra-images/male.png',
        priority: FastImage.priority.normal,
    }}
    resizeMode={FastImage.resizeMode.contain}
/>}
           
                <Text style={styles.titleText}>{getUserName(props.username)}</Text>
                <TouchableOpacity onPress={() => Actions.profile({'tokenKey' :props.tokenKey})}>
                    <Text style={styles.editProfileText}>Edit Profile</Text>
                </TouchableOpacity>
            </View>
            <View style={{ width: '100%' }}>
                <TouchableOpacity onPress={() => Actions.dashboard()} style={styles.itemView}>
                    <Image source={require('../assets/images/home.png')} style={styles.menuIcon} />
                    <Text style={styles.menuText}>Home</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.itemView} onPress={() => Actions.messages()}>
                    <Image source={require('../assets/images/chat.png')} style={[styles.menuIcon, {}]} />
                    <Text style={styles.menuText}>Chat</Text>
                </TouchableOpacity>
                {/* <TouchableOpacity style={styles.itemView} onPress={() => Actions.paymentscreen()}>
                    <Image source={require('../assets/images/payment.png')} style={[styles.menuIcon, { height: 22 }]} />
                    <Text style={styles.menuText}>Payment</Text>
                </TouchableOpacity> */}
                <TouchableOpacity style={styles.itemView} onPress={() => Actions.loyaltypoints()}>
                    <Image source={require('../assets/images/points.png')} style={[styles.menuIcon, { width: 18, height: 22 }]} />
                    <Text style={styles.menuText}>Loyalty Points</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.itemView} onPress={() => Actions.servicehistory()}>
                    <Image source={require('../assets/images/service.png')} style={[styles.menuIcon, { width: 18, height: 22 }]} />
                    <Text style={styles.menuText}>Service History</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => Actions.help()} style={styles.itemView}>
                    <Image source={require('../assets/images/help.png')} style={[styles.menuIcon, { height: 22 }]} />
                    <Text style={styles.menuText}>Help</Text>
                </TouchableOpacity>
                <TouchableOpacity  style={styles.itemView} onPress ={()=>onShare()}>
                    <Image source={require('../assets/images/share.png')} style={[styles.menuIcon, { width: 20, height: 22 }]} />
                    <Text style={styles.menuText}>Share</Text>
                    </TouchableOpacity>            
                <TouchableOpacity style={styles.itemView} onPress={() => {
                    GoogleSignin.signOut();
                    LoginManager.logOut();
                    removeToken()
                    Actions.loginscreen()
                }}>
                    <Image source={require('../assets/images/lgt.png')} style={[styles.menuIcon, { height: 22 }]} />
                    <Text style={styles.menuText}>Logout</Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
}

export default Menu;