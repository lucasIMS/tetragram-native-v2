import React from 'react';
import { View, Text, Image, StyleSheet, Platform, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';

const Header = (props) => {
    return (
        <View style={[styles.header, {
            flex: Platform.OS == 'ios' ? 0.07 : 0.1
        }]}>
            <TouchableOpacity style={{ flex: 0.2, marginLeft: 10 }}
                onPress={() => Actions.pop()}
            >
                <Image source={require('../assets/images/bk.png')} style={{ height: 20, width: 20 }} />
            </TouchableOpacity>
            <View style={[styles.titleView, { flex: 0.6 }]}>
                <Text style={styles.titleText}>{props.title}</Text>
            </View>
            <View style={{ flex: 0.2 }}></View>
        </View>
    )
}

const styles = StyleSheet.create({
    header:{
		width:'100%',
		backgroundColor:'#0196E9',
		flexDirection:'row',
		justifyContent: 'space-between',
		alignItems: 'center'
    },
    titleView:{
		height:20,
		alignItems:'center',
		justifyContent:'center'
    },
    titleText:{
		fontSize:17,
		color:'white',
		fontWeight:'600'
	},
})

export default Header;