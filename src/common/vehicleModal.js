import React, { Component } from 'react';
import { View, Text, Modal, StyleSheet, Dimensions, FlatList, TouchableOpacity, Image } from 'react-native';

const { width, height } = Dimensions.get('window');
data = [1, 1, 1, 1, 1, 1, 1, 1, 1]
class VehicleModal extends Component {

    _selectVehicle = (item, type) => this.props.getSelectVehcile(item, type) // select the vehcile type and pass the selected item.

    render() {
        let { visible, cancelPress, renderDate, onRequestClose, type, icon } = this.props;
        return (
            <Modal
                animationType="none"
                transparent={true}
                visible={visible}
                onRequestClose={onRequestClose}
            >
                <View style={styles.modalContainer}>

                    <View style={styles.vehicleModal}>
                        <TouchableOpacity onPress={cancelPress} style={styles.cancelBtn}>
                            <Text style={styles.cancelText}>Cancel</Text>
                        </TouchableOpacity>
                        <FlatList
                            data={renderDate}
                            renderItem={({ item, index }) =>
                                <TouchableOpacity onPress={() => this._selectVehicle(item, type)} style={styles.productView}>
                                    {icon ?
                                        <Image resizeMode = 'contain' source={item.icon} style={{ height: 90, width: 90, }} />
                                        : <Text style={{ textAlign: 'center' }}>{item}</Text>}
                                </TouchableOpacity>
                            }
                            keyExtractor={(item, index) => index.toString()}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>
                </View>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    modalContainer: {
        flex: 1,
        backgroundColor: '#00000999',
        justifyContent: 'center'
    },
    vehicleModal: {
        width: width - 40,
        alignSelf: 'center',
        height: 'auto',
        marginTop: 70,
        backgroundColor: '#fff',
        paddingTop: 30,
        maxHeight: height - 140,
    },
    productView: {
        paddingVertical: 15,
      justifyContent:'center',alignItems:'center'
    },
    cancelBtn: {
        position: 'absolute',
        zIndex: 1,
        height: 60,
        width: 60,
        backgroundColor: '#0196E9',
        borderRadius: 30,
        right: -10,
        top: -25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    cancelText: {
        color: '#fff',
        fontWeight: '500'
    }
})

export default VehicleModal;