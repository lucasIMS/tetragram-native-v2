import React, { Component } from 'react';
import { 
    StyleSheet, 
    Image, 
    View,
    Text,
    TouchableOpacity,
    TextInput,
    Platform,
    ScrollView,
    FlatList,
    ActivityIndicator,
    Dimensions
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {styles} from './styles';

export const Chats = (props) => (
    <ScrollView style={{flex:1, backgroundColor: '#3A3749', height: '100%'}} showsVerticalScrollIndicator={false}>
        {
            props.chats_loading
            ?
            <ActivityIndicator size="large" color="#F1759B" style={{marginTop:'50%'}} />
            :
            <View>
                <View style={{
                    flexDirection:'row', 
                    justifyContent:'space-between', 
                    marginTop:15,
                    marginBottom:15,
                    width:'95%',
                    alignSelf:'center',
                }}>
                    <Text style={styles.login}>New Messages</Text>
                    <View style={styles.counterView}>
                        <Text style={[styles.tenText,{fontSize:14}]}> {props.chats.length} </Text>
                    </View>
                </View>
                {props.chats.map((item, index) => {
                    debugger
                    return(
                        <TouchableOpacity   style={styles.outerView} 
                                            onPress={() => Actions.chatscreen({
                                                user_id: item.provider_id,
                                                user_name: item.provider.first_name,
                                                room_id:item.id,
                                                avatar: item.provider.picture
                                            })}
                                            key={index}
                        >
                            <View style={styles.innerBox}>
                                <View style={[styles.photoBox,{paddingBottom:25}]}>
                                    <Text style={{color:'white', fontSize:15, fontWeight:'500', marginBottom:3}}>{item.provider.first_name}</Text>
                                    <Text style={{color:'rgba(255,255,255,0.6)', fontSize:13, fontWeight:'400'}}>{item.message}</Text>
                                </View>
                            </View>
                           
                            {
                                item.user.avatar
                                ?
                                <Image  source={{uri: item.user.avatar}} style={styles.profileImage} />
                                :
                                <View style={styles.noProfileImage}>
                                    <Image source={require('../assets/images/user-shape.png')} style={{height:35,width:35}} />
                                </View>
                            }
                            
                            {/* <Text style={{
                                color:'rgba(255,255,255,0.6)', 
                                fontSize:13, 
                                fontWeight:'400',
                                position:'absolute',
                                top:15,
                                right:10
                            }}>
                            {props.returnTime(item.chat.created_at)}</Text> */}
                        </TouchableOpacity>
                    )
                })}
            </View>
        }
    </ScrollView>
);