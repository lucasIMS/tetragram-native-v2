import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions } from 'react-native';

const Button = (props) => {
    return (
        <View style={[styles.loginButtonView, props.styles]}>
            <TouchableOpacity style={styles.buttonLogin} onPress={props.onPress}>
                <Text style={styles.loginText}>{props.buttonTitle}</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    loginButtonView:{
        justifyContent:'center',
        alignItems:'center',
        width:'90%',
        height:60
    },
    buttonLogin:{
    	backgroundColor:'#0197EA',
    	width:'100%',
    	padding:13,
    	borderRadius:30,
    	alignItems:'center',
        justifyContent:'center',
        borderColor: 'transparent',
    },
	loginText:{
		color:'white',
		fontWeight:'600',
		fontSize:15
	},
})

export default Button;