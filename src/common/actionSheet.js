import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Modal, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

const ActionSheet = (props) => {
    selectOption = (type) => props.selectOptions(type)
    return (
        <Modal
            visible={props.visible}
            transparent={true}
            animationType="none"
        >
            <View style={styles.modalContainer}>
                {/* <View></View> */}
                <View style={styles.optionsView}>
                    <View style={{ paddingVertical: 20, }}>
                        <Text style={[{ textAlign: 'center', color: 'gray', fontSize: 19,fontFamily:'OpenSans' }]}>Select Image From</Text>
                    </View>
                    <TouchableOpacity onPress={() => selectOption('camera')} style={styles.touch}>
                        <Text style={[styles.optionsText, { fontSize: 20, fontWeight: '200',fontFamily:'OpenSans' }]}>Camera</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => selectOption('gallery')} style={{ marginVertical: 15 }}>
                        <Text style={[styles.optionsText, { fontSize: 20, fontWeight: '200',fontFamily:'OpenSans' }]}>Gallery</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={props.closeActionSheet} style={[{ marginTop: 35 }]}>
                        <Text style={[styles.optionsText, { fontSize: 21, fontWeight: '600', color: '#1982F4' }]}>Cancel</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalContainer: {
        flex: 1,
        backgroundColor: '#00000444',
        alignItems: 'center',
        justifyContent: 'center'
    },
    optionsView: {
        width: width - 70,
        alignSelf: 'center',
        height: height / 2.2,
        backgroundColor: '#fff'
    },
    options: {
        backgroundColor: '#fff',
        borderRadius: 5,
    },
    optionsText: {
        color: '#000',
        fontSize: 22,
        fontWeight: '600',
        textAlign: 'center',

    },
    touch: {
        marginVertical: 10
    }
})

export default ActionSheet;