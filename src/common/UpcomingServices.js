import React, { Component } from 'react';
import { 
    StyleSheet, 
    Image, 
    View,
    Text,
    TouchableOpacity,
    TextInput,
    Platform,
    ScrollView,
    FlatList,
    ActivityIndicator,
    Dimensions
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {styles} from './styles';
const arr = [1,2,3,4];

export const UpcomingServices = (props) => (
    <ScrollView style={{ backgroundColor: '#fff', height: '100%', paddingTop:10}} showsVerticalScrollIndicator={false}>
        {arr.map((item, index) => {
            return(
                <View style={styles.itemView} key={index}>
                    <Image source={require('../assets/images/map.jpeg')} style={styles.map} />
                    <View style={styles.details}>
                        <Image source={require('../assets/images/ft1.png')} style={styles.avatar} />
                        <View>
                            <Text style={styles.name}>Anthony Powell</Text>
                            <Text style={styles.description}>Toyota Prius - GV8042 - Mini - White</Text>
                            <View style={{
                                flexDirection:'row',
                                alignItems:'center',
                                marginTop:3
                            }}>
                                <Image source={require('../assets/images/star.png')} style={styles.star} />
                                <Text style={styles.description}>4.2</Text>
                            </View>
                        </View>
                        <Text style={styles.date}>01-08-2018</Text>
                        <Text style={styles.price}>$60</Text>
                    </View>
                </View>
            )
        })}
    </ScrollView>
);