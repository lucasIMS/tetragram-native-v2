import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';

const checkedRadio = require('../assets/images/img/icon-radio--on.png')
const unCheckedRadio = require('../assets/images/img/icon-radio--off.png')

const RadioButton = props => (
    <TouchableOpacity onPress={props.pressRadioBtn} style={styles.radioImgWithText}>
            <Image source={props.isChecked ? checkedRadio : unCheckedRadio} resizeMode='contain' style={styles.radioBtn} />
        {props.icon ?
            <Image source={props.icon} style={{ height: 45, width: 45, alignSelf: 'center', marginLeft: 10 }} />
            : null}
        {props.radioValue ?
            <Text style={styles.radioText}>{props.radioValue}</Text>
            : null}
    </TouchableOpacity>
)

const styles = StyleSheet.create({
    radioText: {
        color: 'black',
        fontSize: 14,
        alignSelf: 'center',
        marginLeft: 5,
        fontFamily:'OpenSans'
    },
    radioImgWithText: {
        flexDirection: 'row',
        flex: 1,
        marginVertical: 5,
        alignItems: 'center'
    },
    radioBtn: {
        height: 20,
        width: 20
    }
})

export default RadioButton;