import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, Platform, ActivityIndicator } from 'react-native';

const PickerButton = (props) => {
    return(
        <TouchableOpacity onPress={props.onPress} style={[styles.pickerContainer, {width: props.width}]}>
            {props.loader ? <ActivityIndicator size="small" style={styles.loader}/>
            :<Text style={[styles.dateText, {color: props.color}]}>{props.text}</Text>
            }
            {props.source ? 
            <TouchableOpacity style={styles.icon} onPress={props.iconPress}>
                <Image resizeMode="cover" source={props.source} style={{width: 18, height: 18}}/>
            </TouchableOpacity>
            : null }
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    pickerContainer: {
        borderBottomWidth: 1.5,
        borderBottomColor: '#E5E5E5',
        paddingVertical: Platform.OS == 'ios' ? 8: 8,
        marginTop: Platform.OS == 'ios' ? 15: 10,
        alignSelf: 'center'
    },
    dateText: {
        fontSize: 12,
        fontWeight: '500',
        paddingRight: 30
    },
    icon: {
        position: 'absolute',
        right: 10,
        bottom: 8
    },
})

export default PickerButton;