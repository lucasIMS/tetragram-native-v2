import React, { Component } from 'react';
import { View, Text, StyleSheet, Animated } from 'react-native';

class BottomPopup extends Component {
    constructor(props){
        super(props);
        this.state = {
            animateValue:  new Animated.Value(-40),
        }
    }

    _showPopup = () => { // open popup from buttom on any error.
        Animated.timing(
            this.state.animateValue,
            {
                toValue: 0,
                duration: 200,
            }
        ).start();
        setTimeout(() => { // call close popup function.
            this._hidePopup()
        }, 2000)
    }

    _hidePopup = () => { // close popup in 1 sec.
        Animated.timing(
            this.state.animateValue,
            {
                toValue: -60,
                duration: 200,
            }
        ).start();
    }

    _getMessage = (message) => {
        this._showPopup() // call function for show popup
        return (
            <Text style={{ color: '#fff', marginHorizontal: 15 }}>{message}</Text>
        )
    }

    render() {
        let { message } = this.props;
        return (
            <Animated.View style={{ width: '100%', position: 'absolute', bottom: this.state.animateValue, backgroundColor: '#0197EA', paddingVertical: 12 }}>
                {message ? this._getMessage(message) : null}
            </Animated.View>
        )
    }
}

export default BottomPopup;