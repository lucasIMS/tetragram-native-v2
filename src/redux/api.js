import { API } from './config';

export function apiLogin(user_name, password) {
    return fetch(`${API}/login`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({'user_name': user_name, 'password': password})
    })
    .then(res => res.json())
    .then(res => {
        return res;
    })
    .catch(res => {
        var response = {status: 0};
        return response;
    });
}