import { createSelector } from 'reselect';

const salesSelector = state => state.sales;
const loginResponseSelector = createSelector([salesSelector], sales => sales.get('loginResponse'));

export {
	loginResponseSelector
}