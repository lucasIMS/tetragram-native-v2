import { put, call, takeLatest, takeEvery } from 'redux-saga/effects';

import {
    FETCH_LOGIN
} from './constants';

import {
    setLogin
} from './actions';

import {
	apiLogin
} from './api';

const asyncLogin = function* (payload) {
    const response = yield call(apiLogin, payload.user_name, payload.password);
    yield put(setLogin(response));
}

const sagaLogin = function* () {
    yield takeLatest(FETCH_LOGIN, asyncLogin);
}

export default [
    sagaLogin()
];