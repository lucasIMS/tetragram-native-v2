import {
  FETCH_LOGIN,
  SET_LOGIN
} from './constants';

export function getLogin(user_name, password) {
    return {
        type : FETCH_LOGIN,
        user_name: user_name,
        password: password
    };
};

export function setLogin(info){
  return{
    type: SET_LOGIN,
    info: info
  }
}