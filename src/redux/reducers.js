import { fromJS, List } from 'immutable';
import {
  SET_LOGIN
} from './constants';

const initialState = fromJS({
    loginResponse: null
});

export default function sales (state = initialState, action) {
    switch (action.type) {
        case SET_LOGIN:
            return state.set('loginResponse',action.info);
            break;
        default:
            return state;
    }
}