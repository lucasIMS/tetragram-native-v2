import { initialState } from './initialState';

export default (state = initialState.auth, action) => {
    switch (action.type) {
        case 'ERR_MESSAGE':
        return{
            ...state,
            err_Message: action.payload,
            status: '5001',
        }
        case 'LOADER':
        return{
            ...state,
            loader: action.payload
        }
        case 'LOGIN_SUCCESS':
        return{
            ...state,
            status: '200',
            userId: action.payload.user,
            loader: false,
            userDetail: action.payload
        }
        case 'LOGIN_SUCCESS1':
            return{
                ...state,
                status: '2000',
                loader: false,
                userDetail: action.payload
            }
        case 'LOGIN_TOKEN':
        return{
            ...state,
            status: '200',
            loader: false,
            userToken: action.payload.user
        }
      

     
        case 'LOGIN_FAILURE':
        return {
            ...state,
            status: '400',
            loader: false,
            err_Message: action.payload
        }

        case 'GET_CUSTOM_INFO_SUCCESS':
        return{
            ...state,
            status: '20000',
            loader: false,
            fetchUserInfo: action.payload
            
        }
        case 'UPDATE_SYMPTIONS_SUCCESS':
            return{
                ...state,
                status: '3000',
                loader: false,
                updataSymptios: action.payload
                
            }
            case 'GET_METHOD_LIST':
                return{
                    ...state,
                    status: '2902',
                    loader: false,
                    methodList: action.payload
                    
                }


        case 'GET_CUSTOM_INFO_FAILURE':
        return {
            ...state,
            status: '400',
            loader: false,
            err_Message: action.payload
        }


        case 'GET_SPECIFIC_METHOD_LIST':
            return {
                ...state,
                status: '2903',
                loader: false,
                methodSpecificList: action.payload
            }
            case 'GET_SPECIFIC_TYPE_LIST':
                return {
                    ...state,
                    status: '2904',
                    loader: false,
                    typeSpecificList: action.payload
                }



        case 'GET_SYMPTIONS_SUCCESS':
            return{
                ...state,
                status: '2000',
                loader: false,
                fetchSymptions: action.payload
                
            }



        case 'EDIT_SYMPTIONS_SUCCESS':
            return{
                ...state,
                status: '2050',
                loader: false,
                fetchSymptions: action.payload
                
            }



        case 'DEL_SYMPTIONS_SUCCESS':
            return{
                ...state,
                status: '2060',
                loader: false,
                fetchSymptions: action.payload
                
            }

            case 'DEL_SYMPTIONS_FAIL':
            return{
                ...state,
                status: '4444',
                loader: false,
                
            }

            case 'UPDATE_SYMPTIONS_SUCCESS':
                return{
                    ...state,
                    status: '20001',
                    loader: false,
                    fetchSymptions: action.payload
                    
                }

            case 'ADD_SYMPTIONS_SUCCESS':
                return{
                    ...state,
                    status: '2020',
                    loader: false,
                    fetchSymptions: action.payload
                    
                }


            case 'GET_SYMPTIONS_FAILURE':
            return {
                ...state,
                status: '400',
                loader: false,
                err_Message: action.payload
            }





        case 'SEARCH_STRAIN':
        return{
            ...state,
            status: '2222',
            loader: false,
            fetchStrain: action.payload
            
        }


        case 'SEARCH_DISPENSARIES':
            return{
                ...state,
                status: '2202',
                loader: false,
                fetchDispensaries: action.payload
                
            }

            case 'GET_EDIT_TIMELINE':
            return{
                ...state,
                status: '22100',
                loader: false,
                editTimeline: action.payload
                
            }



        
        case 'SIGNUP_SUCCESS':
        return {
            ...state,
            status: '201',
            loader: false,
            signUpText: action.payload
        }
        case 'SIGNUP_FAILURE':
        return {
            ...state,
            status: '401',
            err_Message: action.payload,
            loader: false
        }
        case 'FORGOT_PASS_SUCCESS':
        return {
            ...state,
            status: '202',
            userId: action.payload.user,
            loader: false,
            userDetail: action.payload.user
        }
        case 'FORGOT_PASS_FAILURE':
        return {
            ...state,
            status: '402',
            err_Message: action.payload,
            loader: false
        }
        case 'RESET_PASS_SUCCESS':
        return {
            ...state,
            status: '203',
            err_Message: action.payload.message,
            loader: false,
        }
        case 'RESET_PASS_FAILURE':
        return {
            ...state,
            status: '403',
            err_Message: action.payload,
            loader: false
        }
        case 'VERIFY_ACCOUNT_SUCCESS':
        return {
            ...state,
            status: '204',
            err_Message: action.payload.message,
            loader: false,
        }
        case 'VERIFY_ACCOUNT_FAILURE':
        return {
            ...state,
            status: '404',
            err_Message: action.payload,
            loader: false
        }
        case 'CLEAR_STATE':
        return {
            ...state,
            status: '',
            err_Message: '',
        }

        case 'FETCH_REVIEW':
            return{
                ...state,
                status: '2332',
                loader: false,
                fetchReviews: action.payload
                
            }
            case 'FETCH_REVIEW_DETAILS':
                return{
                    ...state,
                    status: '2333',
                    loader: false,
                    reviewDetails: action.payload
                    
                }

            case 'SEARCH_STRAIN':
                return {
                    ...state,
                    status: '4543',
                    err_Message: action.payload,
                    loader: false
                }


                case 'SAVE_SESSION':
                    return {
                        ...state,
                        status: '2099',
                        saveSessionResult: action.payload,
                        loader: false
                    }

                    case 'GET_SESSION':
                        return {
                            ...state,
                            status: '2095',
                            sessionResult: action.payload,
                            loader: false
                        }

                        case 'DEL_SESSION':
                            return {
                                ...state,
                                status: '2096',
                                delSession: action.payload,
                                loader: false
                            }
                            case 'DEL_SESSION_FAIL':
                                return {
                                    ...state,
                                    status: '4999',
                                    loader: false
                                }
                                case 'SAVE_SESSION_FAIL':
                                    return {
                                        ...state,
                                        status: '4555',
                                        err_Message: action.payload,
                                        loader: false
                                    }
                                case 'SAVE_Image':
                                    return {
                                        ...state,
                                        status: '2343',
                                        saveSessionImg: action.payload,
                                        loader: false
                                    }

                                    case 'REVIEW_POST':
                                        return {
                                            ...state,
                                            status: '2786',
                                            reiewSucess: action.payload,
                                            loader: false
                                        }
                                        case 'REVIEW_POST_FAIL':
                                            return {
                                                ...state,
                                                status: '4786',
                                                err_Message: action.payload,
                                                loader: false
                                            }



    }
    return state;
}