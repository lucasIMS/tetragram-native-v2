import { initialState } from './initialState';

export default (state = initialState.profile, action) => {
    switch(action.type) {
        case 'PROFILE_ERR_MESSAGE':
        return{
            ...state,
            profileErrMsg: action.payload,
            profileActionStatus: '5001',
        }
        case 'CLEAR_STATE':
        return {
            ...state,
            profileActionStatus: '',
            profileErrMsg: ''
        }
        case 'EDIT_PROFILE_SUCCESS':
        return {
            ...state,
            bookingActionStatus: '201',
            bookingErrMsg: ''
        }
    }
    return state;
}