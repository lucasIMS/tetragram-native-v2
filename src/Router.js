import React  				from	'react';
import { Scene, Router,ActionConst } 		from	'react-native-router-flux';
import Splash 				from	'./screens/Splashscreen';
import WelcomeScreen       from    './screens/WelcomeScreen';

import Login 				from	'./screens/Loginscreen';

import SignUp               from    './screens/Signupscreen';
import ForgotPassword       from    './screens/ForgotPassScreen';
import ForgotPasswordSent       from    './screens/ForgotPasswordSent';
import ChooseNewPassword       from    './screens/NewPasswordScreen';
import NewLogin       from    './screens/NewLogin';
import ProfileScreen       from    './screens/Profile';
import Experince_Timeline       from    './screens/Experince_Timeline';
import LoginSection       from    './screens/LoginSession';
import LoginSessionSearch       from    './screens/LoginSessionSearch';
import SymptomsScreen       from    './screens/SymptomsScreen';
import ReviewScreen       from    './screens/ReviewDetails';
import ReviewProduct from './screens/ReviewProduct'
import editTimeline from './screens/EditTimeline'





const RouterComponent = () => {
	return (
		<Router>
			<Scene key="root">
				<Scene  key='splash' 				hideNavBar  component={Splash}  									/>
				<Scene  key='welcome' 			hideNavBar  component={WelcomeScreen}  			type={ActionConst.RESET}initial		/>

				<Scene  key='loginscreen' 			hideNavBar  component={Login}  			type={ActionConst.RESET}		/>
				<Scene 	key='signup'				hideNavBar	component={SignUp}											/>
				<Scene  key='forgotpassword'        hideNavBar  component={ForgotPassword}                                  />
				<Scene  key='forgotPasswordSent'        hideNavBar  component={ForgotPasswordSent}                                  />
				<Scene  key='chooseNewPassword'        hideNavBar  component={ChooseNewPassword}                                  />
				<Scene  key='newLogin'        hideNavBar  component={NewLogin}                                  />
				<Scene  key='ProfileScreen'        hideNavBar  component={ProfileScreen}                                  />
				<Scene  key='Experince_Timeline'        hideNavBar  component={Experince_Timeline}                                  />
				<Scene  key='LoginSection'        hideNavBar  component={LoginSection}                                  />
				<Scene  key='LoginSessionSearch'        hideNavBar  component={LoginSessionSearch}                                  />
				<Scene  key='SymptomsScreen'        hideNavBar  component={SymptomsScreen}                                  />
				<Scene  key='ReviewScreen'        hideNavBar  component={ReviewScreen}                                  />
				<Scene  key='ReviewProduct'        hideNavBar  component={ReviewProduct}                                  />
				<Scene  key='editTimeline'        hideNavBar  component={editTimeline}                                  />


			</Scene>
		</Router>
	);
};

export default RouterComponent;