import React, { Component } from 'react';
import { View, Text, StatusBar, SafeAreaView,Alert,AsyncStorage } from 'react-native';
import Router from './Router';
import { Provider } from 'react-redux';
import store from './settings/store';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
		};

	}
	render() {
		return (
			<Provider store={store}>
					<View style={{ flex: 1 }}>
						<Router />
					</View>
			</Provider>
		);
	}
	
	
	
}

export default App;