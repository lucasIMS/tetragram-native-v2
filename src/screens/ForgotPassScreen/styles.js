import {Dimensions} from 'react-native';

export const styles = {
	img_container:{
		flex:1,
		backgroundColor:'white',
	},
	splash:{
		height: Dimensions.get('window').height,
		width: Dimensions.get('window').width
	},
	middleText:{
		fontSize:18
	},
	logo:{
		flex:0.32,
		justifyContent:'center',
		alignItems:'center',
		width:'100%',
		// backgroundColor: 'red'
	},
	fields:{
		// flex:0.25,
		justifyContent:'space-between',
		alignItems:'center',
		width:'100%',
		padding:20,
		// backgroundColor: 'red'
	},
	fieldView:{
		flexDirection:'row',
		alignItems:'center',
		width:'100%',
		// flex:0.2,
		// backgroundColor: 'blue'
	},
	input:{
		width:'90%',
		fontSize:15,
		fontWeight:'500',
		// paddingBottom:8,
		borderBottomWidth:1.5,
		paddingVertical: 8,
		borderColor:'#E5E5E5'
	},
	forgotPassdescrip: {
		alignItems: 'center',
		marginBottom: 20
	},
	forgotPassHeading: {
		fontSize: 23,
		fontWeight: '500',
		color: '#000',
		marginBottom: 10
	},
	forgotPassSubHead: {
		marginHorizontal: 20
	},
	forgotPassdesText: {
		textAlign: 'center',
		color: 'gray',
		fontSize: 11,
		fontWeight: '500',
		lineHeight: 20
	},
	resendView: {
		marginTop: 10,
		alignItems: 'center'
	},
	resendText: {
		color: '#0197EA',
		fontSize: 13
	},
	backButton: {
		top: 30,
		left: 10,
		position: 'absolute'
	}
};

export default styles;