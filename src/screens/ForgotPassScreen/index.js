import React, { Component } from 'react';
import {
    StyleSheet,
    Image,
    View,
    Text,
    TextInput,
    Dimensions,
    TouchableOpacity,
    ImageBackground,
    SafeAreaView,
    Platform
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { styles } from './styles';
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";
import Loader from '../../common/loader';

// components
import Button from '../../common/button';
import BottomPopup from '../../common/bottomPopup';

// redux Action 
import { ForgotPass, ClearAction } from "../../actions/authAction";
function wp (percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const slideHeight = viewportHeight * 0.36;
const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);

 const sliderWidth = viewportWidth;
 const itemWidth = slideWidth + itemHorizontalMargin * 2;
class ForgotPassword extends Component {
    state = {
        email: '',
        message: '',
        loader: false
    }

    async componentWillReceiveProps(nextProps) {
        debugger
        if (Actions.currentScene == "forgotpassword") await this.setState({ loader: nextProps.loader })
        if (nextProps.status === '5001') {
            this.setState({ ErrMsg: nextProps.err_Message, })
        }
        else if (nextProps.status === '200') {
            debugger
            Actions.forgotPasswordSent()
            this.clearReducerState();
            this.setState({ email: '', password: '' })
        }
    
        else if (nextProps.status === '402') {
            debugger
            this.setState({ ErrMsg: nextProps.err_Message })
        }
    }



    _resetPassword = () => {
        this._resetPasswordAPI()
    }

    onForgotpassword = ()=>{
        if(this.state.email == ''){
            alert('Please fill email')
        }
        else
        {
            let formData = new FormData();
            formData.append('username',this.state.email)
            this.props.action.ForgotPass(formData)
        }

        // Actions.forgotPasswordSent()
    }

   

    render() {
        return (
            <ImageBackground style={{ height: '100%', width: '100%' }} resizeMode='cover' source={require('../../assets/images/img/background.png')}>
              <SafeAreaView style={{ flex: 1 }}>

    <TouchableOpacity onPress = {()=>Actions.pop()} style = {{marginLeft:20 }}>

<Image resizeMode='contain' style={{ height: 40, width: 50}}source={require('../../assets/images/img/back_arrow_white.png')} />

</TouchableOpacity>
        
            <View style={{ overflow: 'visible',marginTop:40,backgroundColor:'white',width:viewportWidth-40,borderRadius:10,alignSelf:'center'}}>

     <Text style = {{fontSize:36,
        color:'#6559A0',margin:20,
        marginRight:40,
        fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
        lineHeight:36,
        fontWeight:'400'
        }}>Forgot password</Text>
                <Text style = {{fontSize:18,color:'black',marginLeft:20,fontFamily:'OpenSans',fontWeight:'normal'}}>Enter your Email Address</Text>
                <TextInput
                 placeholder = 'johnny@appleseed.com' 
                 keyboardType = 'email-address'
                 onChangeText={(email) => this.setState({email})}
                                 value={this.state.email}
                 style = {{borderWidth:0.5,borderRadius:10,height:44,marginTop:10,marginHorizontal:20,paddingHorizontal:10}}>
                </TextInput>
                <TouchableOpacity onPress = {()=> this.onForgotpassword()} style = {{borderWidth:1,borderRadius:10,height:44,margin:20,justifyContent:'center',alignItems:'center',backgroundColor:'#3089CA',borderColor:'transparent'}}>
                    <Text style = {{color:'white',fontWeight:'bold',fontSize:16,fontFamily:'OpenSans',fontWeight:'bold'}}> Send password reset</Text>
                </TouchableOpacity>                   
                
                    </View>
            </SafeAreaView>
            <BottomPopup message={this.state.ErrMsg} />
                    <Loader visible={this.state.loader} />
            </ImageBackground>
        );
    }
}

const mapStateToProps = state => {
    return {
        status: state.auth.status,
        err_Message: state.auth.err_Message,
        loader: state.auth.loader
    }
}

const mapDispatchToProps = dispatch => {
    return {
        action: bindActionCreators({ ForgotPass, ClearAction }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);