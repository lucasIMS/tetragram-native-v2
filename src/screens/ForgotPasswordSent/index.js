import React, { Component } from 'react';
import {
    StyleSheet,
    Image,
    View,
    Text,
    TextInput,
    Dimensions,
    TouchableOpacity,
    ImageBackground,
    SafeAreaView,
    Platform
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { styles } from './styles';
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";
import Loader from '../../common/loader';

// components
import Button from '../../common/button';
import BottomPopup from '../../common/bottomPopup';

// redux Action 
import { ForgotPass, ClearAction } from "../../actions/authAction";
function wp(percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const slideHeight = viewportHeight * 0.36;
const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);

const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;
class ForgotPasswordSent extends Component {
    state = {
        email: '',
        message: '',
        loader: false
    }

    _resetPassword = () => {
        this._resetPasswordAPI()
    }



    render() {
        return (
            <ImageBackground style={{ height: '100%', width: '100%' }} resizeMode='cover' source={require('../../assets/images/img/background.png')}>
                <SafeAreaView style={{ flex: 1 }}>
                    <TouchableOpacity onPress={() => Actions.pop()} style={{ marginLeft: 20 }}>
                        <Image resizeMode='contain' style={{ height: 40, width: 50 }} source={require('../../assets/images/img/back_arrow_white.png')} />

                    </TouchableOpacity>
                    <View style={{ overflow: 'visible', marginTop: 40, backgroundColor: 'white', width: viewportWidth - 40, borderRadius: 10, alignSelf: 'center' }}>

                        <Text style={{
                            fontSize: 36, color: '#6559A0',
                            margin: 20, marginRight: 40,
                            fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                            lineHeight: 36,
                            fontWeight: '400'
                        }}>Forgot password</Text>
                        <Text style={{ fontSize: 18, color: 'black', marginLeft: 20, fontFamily: 'OpenSans', fontWeight: 'normal', marginTop: 11 }}>We sent you an email!</Text>
                        <Text style={{ fontSize: 14, color: 'black', marginHorizontal: 20, marginTop: 11, marginBottom: 40, fontFamily: 'OpenSans', lineHeight: 20 }}>
                            check your email for the password reset link, it will expire in 24 hours
                </Text>

                    </View>
                </SafeAreaView>
            </ImageBackground>
        );
    }
}

const mapStateToProps = state => {
    return {
        status: state.auth.status,
        err_Message: state.auth.err_Message,
        loader: state.auth.loader
    }
}

const mapDispatchToProps = dispatch => {
    return {
        action: bindActionCreators({ ForgotPass, ClearAction }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordSent);