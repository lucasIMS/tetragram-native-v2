import React, { Component } from 'react';
import {
    Image,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    SafeAreaView,
    Platform,
    ScrollView,
    StyleSheet,
    ImageBackground,
    Dimensions,
    FlatList,
    TouchableWithoutFeedback
} from 'react-native';
import { Actions } from 'react-native-router-flux';
// import  styles  from './styles';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";

import Modal from "react-native-modal";
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import RadioButton1 from '../../common/radioButton'

// components
import Button from '../../common/button';
import BottomPopup from '../../common/bottomPopup';
import Loader from '../../common/loader';
import { ratingReview,getSymptoms,ClearAction } from "../../actions/authAction";
import StarRating from 'react-native-star-rating';

// redux Action 
function wp(percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const slideHeight = viewportHeight * 0.36;
const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);

const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;
import GestureRecognizer, { swipeDirections } from 'react-native-swipe-gestures';


class ReviewScreen extends Component {
    constructor(props) {
        super(props);
    this.state = { // initilize state
        isCheckedPhyscial:'',
        isCheckedPhys:'',
        isCheckedPain:'',
        isCheckedSort:'',
        loader: false,
        isSecure: true,
        sysPain:[],
        sysPhychology:[],
        sysPhyscial:[],
        types1: [{ label: 'Feature', value: 0 }, { label: 'By rating', value: 1 }, { label: 'A-Z', value: 3 }],
        types2: [{ label: 'Depression', value: 0 }],
        sortBy:0,
         sortData : [
         {name: "By Rating", category: "Physical"},
         {name: "A - Z", category: "Physical"}],
         popularData : [{name: "All Symptoms", category: "all_symptoms"}],

       
        }
        this.handleScroll = this.handleScroll.bind(this)

    }

    componentDidMount() {
        this.setState({ loader: true })
        debugger
        this.props.action.ratingReview(this.props.userDetail,'api/reviews')
        let data = this.props.fetchSymptions
        pain = data.filter(function (item) {
            return item.category == 'Pain';
        }).map(function ({ name, category }) {
            return { name, category };
        });

        physchology = data.filter(function (item) {
            return item.category == 'Psychological';
        }).map(function ({ name, category }) {
            return { name, category };
        });

        physical = data.filter(function (item) {
            return item.category == 'Physical';
        }).map(function ({ name, category }) {
            return { name, category };
        });

        this.setState({ 
            sysPain: pain, 
            sysPhychology: physchology,
             sysPhyscial: physical
             })
    }



    async componentWillReceiveProps(nextProps) {
        if (Actions.currentScene == "ReviewScreen") await this.setState({ loader: nextProps.loader })
        if (nextProps.status === '2332') {
            debugger
            this.setState({ symptionData: nextProps.fetchReviews })
        }
       else if (nextProps.status === '2000') {
            let data = nextProps.fetchSymptions
            pain = data.filter(function (item) {
                return item.category == 'Pain';
            }).map(function ({ name, category }) {
                return { name, category };
            });

            physchology = data.filter(function (item) {
                return item.category == 'Psychological';
            }).map(function ({ name, category }) {
                return { name, category };
            });

            physical = data.filter(function (item) {
                return item.category == 'Physical';
            }).map(function ({ name, category }) {
                return { name, category };
            });

            this.setState({ 
                sysPain: pain, 
                sysPhychology: physchology,
                 sysPhyscial: physical
                 })

        }
        else if (nextProps.status === '4543') {

        }

    }

    componentWillUnmount() {
        this.clearReducerState();
    }
    
    clearReducerState = () => { // reset initial state.
        this.props.action.ClearAction()
    }


    async handleScroll(event) {
        if (event.nativeEvent.contentOffset.y == -3 || event.nativeEvent.contentOffset.y == -2 || event.nativeEvent.contentOffset.y == -4 || event.nativeEvent.contentOffset.y == -5 || event.nativeEvent.contentOffset.y == -6) {
            this.setState({ isModalVisibleSymptomsScreen: false })

        }
        console.log(event.nativeEvent.contentOffset.y);
    }




    onCheckInvision() {
        setTimeout(() => {
            this.setState({ isSecure: !this.state.isSecure })
        }, 200);
    }
    renderMactItem = ({ item, index }) => {
        debugger
        let itemName = item.strain.type
        firstCara = itemName.charAt(0)
        return (
            <TouchableOpacity onPress={() => Actions.ReviewProduct({postData:item})} 
            style={{ borderWidth: 0.5, 
                borderRadius: 16,
             marginHorizontal: 25, 
             marginTop: 20,
             borderColor:'#E7E7E9' 
             }}>
                <View style={{ 
                    flexDirection: 'row', 
                marginHorizontal: 15, 
                marginVertical: 10 
                }}>
                    <Text style={{
                         fontSize: 20,
                          fontFamily: 'Merriweather-Light', 
                          color: 'black', 
                          }}>
                        {item.strain.name}
                    </Text>
                    <View style={{
                                            backgroundColor: '#BFDEEC',
                                            borderRadius: 4,
                                            alignItems: 'center',
                                            height: 20,
                                            width: 18,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            alignSelf: 'center',
                                            marginLeft: 4
                                        }}>
                                            <Text style={{
                                                fontSize: 10,
                                                fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                                textAlign: 'center',

                                                fontWeight: 'bold',
                                                lineHeight: 12
                                            }}>{firstCara}</Text>
                                        </View>
                </View>
                <Text style={{ fontSize: 14, fontWeight: '500', color: 'black', fontFamily: 'OpenSans', marginLeft: 20 }}>{item.text}</Text>
                {/* <Image style={{ height: 80, width: viewportWidth - 50 }} resizeMode='cover' source= {item.image == null ? require('../../assets/images/img/food.jpeg'): {uri:item.image}} /> */}
                <View style={{ flexDirection: 'row',
                 marginBottom: 30, }}>
                    <View style={{ flex: 1 }}>
                        <Text style={{ fontSize: 18, 
                            fontWeight: '600',
                             color: 'black', 
                            fontFamily: 'OpenSans',
                             marginLeft: 15,
                             marginTop: 15 
                             }}> Rating</Text>
                        <View style={{
                             marginHorizontal: 20,
                             width: 90, 
                             marginTop: 5 
                             }}>

                            <StarRating
                                disabled={true}
                                starSize={18}
                                maxStars={5}
                                starStyle={{ marginRight: 5, 
                                    color: '#2E7FAE' }}
                                rating={item.overall_rating}
                            />

                        </View>
                    </View>

                    <View style={{ flex: 1, marginLeft: 15 }}>
                        <Text style={{ 
                             fontSize: 16, 
                            fontWeight: '600',
                             color: 'black', 
                            fontFamily: 'OpenSans',
                             marginLeft: 15,
                             marginTop: 15 
                              }}> Popular for:</Text>
                        {item.review_symptoms.map((data) => {
                            return(
                    <Text style={{ fontSize: 14, fontWeight: '500', color: 'black', fontFamily: 'OpenSans', marginLeft: 20 }}>{data.symptom.name}</Text>
                            )
                        })}

                    </View>
                </View>
            </TouchableOpacity>

        )
    }
    async onSwipeDown() {
        await this.setState({ isModalVisibleSearch: false, isModalVisibleSymptomsScreen: false })
    }



    isPainChecked(item) {
        this.setState({ isCheckedPain: item })
    }


    isPhysChecked(item) {
        this.setState({ isCheckedPhys: item })
    }
    isSortChecked(item){
        this.setState({isCheckedSort:item})
    }

    isPhsycialChecked(item) {
        this.setState({ isCheckedPhyscial: item })
    }



    //render 
    renderPain = ({ item, index }) => {
        return (

            <View style={{ 
                paddingHorizontal: 25,
             width: '50%', 
             justifyContent: 'flex-start',
              marginTop: 5, }}>
                <RadioButton1 radioValue={item.name} isChecked={this.state.isCheckedPain == item.name} pressRadioBtn={() => this.isPainChecked(item.name)} />
            </View>
        )
    }


    renderPhys = ({ item, index }) => {
        return (

            <View style={{ 
                paddingHorizontal: 25,
             width: '50%',
              justifyContent: 'flex-start',
               }}>
                <RadioButton1 radioValue={item.name} isChecked={this.state.isCheckedPhys == item.name} pressRadioBtn={() => this.isPhysChecked(item.name)} />
            </View>
        )
    }


    renderSortData = ({ item, index }) => {
        return (

            <View style={{ 
                paddingHorizontal: 25,
             width: '33%',
              justifyContent: 'flex-start',
               }}>
                <RadioButton1 radioValue={item.name} isChecked={this.state.isCheckedSort == item.name} pressRadioBtn={() => this.isSortChecked(item.name)} />
            </View>
        )
    }



    renderPhyscal = ({ item, index }) => {
        return (

            <View style={{ paddingHorizontal: 25, width: '50%', justifyContent: 'flex-start', marginTop: 5, }}>
                <RadioButton1 radioValue={item.name} isChecked={this.state.isCheckedPhyscial == item.name} pressRadioBtn={() => this.isPhsycialChecked(item.name)} />
            </View>
        )
    }

filterButton(){
    debugger
        let postData = ''
        let checkedSymp = ''
        let orderBy = ''
        let uri = 'api/reviews?reviewSymptoms.symptom.name='
        if(this.state.isCheckedPhys != ''){
            if(this.state.isCheckedPhys == 'All Symptoms'){
                orderBy = 'order[strain.name]=ASC'
                this.setState({ isModalVisibleSymptomsScreen: false })
                this.props.action.ratingReview(this.props.userDetail,'api/reviews?'+orderBy)
        }
        else{
            orderBy = 'order[strain.name]=ASC'
            this.setState({ isModalVisibleSymptomsScreen: false })
            this.props.action.ratingReview(this.props.userDetail,uri+this.state.isCheckedPhys)
        }
    }
     
        else if(this.state.isCheckedPhyscial !=''){
                postData= this.state.isCheckedPhyscial
                uri = uri+ this.state.isCheckedPhyscial
                this.setState({ isModalVisibleSymptomsScreen: false })
                this.props.action.ratingReview(this.props.userDetail,uri+this.state.isCheckedPhyscial)


        }
       else if(this.state.isCheckedPain !=''){
            postData= this.state.isCheckedPain
            uri = uri+ this.state.isCheckedPain
            this.setState({ isModalVisibleSymptomsScreen: false })
            this.props.action.ratingReview(this.props.userDetail,uri+this.state.isCheckedPain)

        }
        else if(this.state.isCheckedSort !=''){
            debugger
            if(this.state.isCheckedSort == 'A-Z'){
                debugger
                orderBy = 'order[strain.name]=ASC'
                this.setState({ isModalVisibleSymptomsScreen: false })
                this.props.action.ratingReview(this.props.userDetail,'api/reviews?reviewSymptoms.symptom.name='+orderBy)

            }
            else if(this.state.isCheckedSort == "By Rating"){
                orderBy = 'order[overallRating]=DESC'
                this.setState({ isModalVisibleSymptomsScreen: false })
                this.props.action.ratingReview(this.props.userDetail,'api/reviews?'+orderBy)
            }
            else{
                debugger
                orderBy = 'order[strain.name]=ASC'
                this.setState({ isModalVisibleSymptomsScreen: false })
                this.props.action.ratingReview(this.props.userDetail,'api/reviews?'+orderBy)

            }
            
            }
            else{
                debugger
                this.setState({ isModalVisibleSymptomsScreen: false })
                this.props.action.ratingReview(this.props.userDetail,uri)

            }
            
            debugger
        
}


    render() {
        const config = {
            velocityThreshold: 2.0,
            directionalOffsetThreshold: 120

        };
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#2C7DB3' }}>

                    <View style={{ flex: 1 }}>
                        <View style={{
                            flex: Platform.OS == 'ios' ? 0.07 : 0.1,
                            width: '100%',
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center'
                        }}>
                            <TouchableOpacity
                                onPress={() => Actions.pop()}>

                                <Image resizeMode='contain' style={{ height: 20, width: 140 }} source={require('../../assets/images/img/logo_header.png')} />
                            </TouchableOpacity>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: 'white' }}>
                                    <Image resizeMode='contain' style={{ height: 20, width: 20, }} source={require('../../assets/images/img/icon_speech_bubble.png')} />
                                    <Text style={{ color: 'white', fontSize: 12, fontFamily: 'OpenSans' }}>
                                        Reviews
                         </Text>
                                </TouchableOpacity>


                                <TouchableOpacity onPress={() => Actions.ProfileScreen()} style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 10, }}>
                                    <Image resizeMode='contain' style={{ height: 20, width: 20 }} source={require('../../assets/images/img/icon_profile.png')} />
                                    <Text style={{ color: 'white', fontSize: 12, fontFamily: 'OpenSans' }}>
                                        Profile
                        </Text>
                                </TouchableOpacity>


                            </View>
                        </View>
                        <View style={{ flex: 1, backgroundColor: 'white' }}>
                            <ScrollView style={{ flex: 1 }}>
                                <Text style={{
                                     fontSize: 24,
                                     color: '#6559A0', 
                                     marginHorizontal: 25,
                                      marginTop: 30, 
                                      fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                    }}>Product reviews</Text>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 25 }}>
                                    <TextInput placeholder='Search strains' style={{ flex: 1, borderWidth: 1, borderRadius: 10, height: 40, marginTop: 10, paddingLeft: 10, paddingRight: 40 }}>

                                    </TextInput>
                                    <TouchableOpacity style={{ alignSelf: 'center', position: 'absolute', right: 15, marginRight: 10, }} onPress={() => this.onCheckInvision()}>
                                        <Image source={require('../../assets/images/img/icon-search.png')} style={{ height: 20, width: 20, marginRight: 10, marginTop: 8 }}></Image>

                                    </TouchableOpacity>

                                    <TouchableOpacity style={{ alignSelf: 'center' }} onPress={() => this.setState({ isModalVisibleSymptomsScreen: true })}>
                                        <Image source={require('../../assets/images/img/icon-filter.png')} style={{ height: 30, width: 20, marginTop: 8, marginLeft: 10 }}></Image>

                                    </TouchableOpacity>

                                </View>
                                <FlatList
                                    style={{ flex: 1, backgroundColor: 'white' }}
                                    data={this.state.symptionData}
                                    showsVerticalScrollIndicator={false}
                                    renderItem={this.renderMactItem
                                    }
                                    extraData={this.state}

                                />

                            </ScrollView>
                        </View>
                    </View>

        
                    <Modal
                        propagateSwipe={true}
                        onSwipeThreshold={20}
                        swipeDirection="down"
                        onSwipeComplete={() => this.setState({ isModalVisibleSymptomsScreen: false })}
                        style={{ margin: 0, marginTop: 60 }}
                        isVisible={this.state.isModalVisibleSymptomsScreen}>
                        <View style={{ flex: 1, backgroundColor: 'white' }}>

                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('../../assets/images/img/icon_pull_handle.png')} style={{ width: 90, height: 5, marginTop: 10 }} />
                            </View>
                            <ScrollView
                                onScroll={this.handleScroll.bind(this)}
                            >
                                  <TouchableWithoutFeedback>
                                    <View>
                                <Text style={{ fontSize: 26, 
                                    color: '#6559A0', 
                                    fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                    marginLeft: 25,
                                     marginTop: 30 
                                     }}>Filter and sort strains</Text>
                       
                                 <Text style={{
                                       fontSize: 16,
                                       color: 'black',
                                        marginLeft: 25,
                                         marginTop: 20, 
                                         fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                         fontWeight: 'bold'
                                }}>Sort by:</Text>
                                   <FlatList
                                            data={this.state.sortData}
                                            renderItem={this.renderSortData}
                                            //Setting the number of column
                                            numColumns={3}
                                            extraData={this.state}

                                            keyExtractor={(item, index) => index}
                                        />
         <View style = {{height:0.5,marginHorizontal:25,marginTop:15,backgroundColor:'gray'}}>

</View>
                                 <Text style={{
                                          fontSize: 16,
                                          color: 'black',
                                           marginLeft: 25,
                                            marginTop: 20, 
                                            fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                            fontWeight: 'bold'
                                }}>Popular for:</Text>
                                    <FlatList
                                        data={this.state.popularData}
                                        renderItem={this.renderPhys}
                                        style = {{height:50}}

                                        //Setting the number of column
                                        numColumns={3}
                                        extraData={this.state}

                                        keyExtractor={(item, index) => index}
                                    />
                                <View>
                                <Text style={{
                                   fontSize: 16,
                                   color: 'black',
                                    marginLeft: 25,
                                     marginTop: 0, 
                                     fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                     fontWeight: 'normal' 
                                }}>Psychological</Text>
                                    <FlatList
                                        data={this.state.sysPhychology}
                                        renderItem={this.renderPhys}
                                        style = {{height:60}}

                                        //Setting the number of column
                                        numColumns={2}
                                        extraData={this.state}

                                        keyExtractor={(item, index) => index}
                                    />
                                    </View>
                                    <Text style={{ fontSize: 16,
                                     color: 'black',
                                      marginLeft: 25,
                                       marginTop: 20, 
                                       fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                       fontWeight: 'normal'
                                     }}>Pain</Text>

                                    <View style={{ flexDirection: 'row' }}>

                                        <FlatList
                                            data={this.state.sysPain}
                                            renderItem={this.renderPain}

                                            //Setting the number of column
                                            numColumns={2}
                                            extraData={this.state}

                                            keyExtractor={(item, index) => index}
                                        />
                                    </View>

                                    <Text style={{ fontSize: 16, 
                                        color: 'black', 
                                        marginLeft: 25,
                                         marginTop: 20, 
                                         fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                         fontWeight: 'normal' 
                                         }}>Physical</Text>
                                    <FlatList
                                        data={this.state.sysPhyscial}
                                        renderItem={this.renderPhyscal}
                                        extraData={this.state}
                                        //Setting the number of column
                                        numColumns={2}
                                        keyExtractor={(item, index) => index}
                                    />


                                


                                <TouchableOpacity onPress={() => this.filterButton()}
                                 style={{ borderWidth: 1,
                                  borderRadius: 16,
                                   height: 40,
                                    marginHorizontal:40,
                                    marginVertical: 80, 
                                    justifyContent: 'center', 
                                    alignItems: 'center',
                                     backgroundColor: '#3089CA',
                                      borderColor: 'transparent' 
                                      }}>
                                    <Text style={{ color: 'white', fontWeight: '600', fontSize: 16, fontFamily: 'OpenSans' }}>Apply Filters</Text>
                                </TouchableOpacity>
                                </View>
                                </TouchableWithoutFeedback>






                            </ScrollView>
                        </View>

                    </Modal>

                    <BottomPopup message={this.state.ErrMsg} />
                    <Loader visible={this.state.loader} />
                  
            </SafeAreaView>


        );
    }


}



const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    },

    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderBottomWidth: .5,
        borderColor: '#000',
        height: 40,
        marginHorizontal: 20,
        marginVertical: 10,
    },

    ImageStyle: {
        padding: 5,
        margin: 5,
        height: 15,
        width: 13,
        resizeMode: 'stretch',
        alignItems: 'center'
    },

});

const mapStateToProps = state => {
    return {
        fetchSymptions: state.auth.fetchSymptions,
        fetchReviews: state.auth.fetchReviews,
        userDetail: state.auth.userDetail,
        status: state.auth.status,
        err_Message: state.auth.err_Message,
        loader: state.auth.loader
    }
}

const mapDispatchToProps = dispatch => {
    return {
        action: bindActionCreators({ ratingReview,getSymptoms,ClearAction }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReviewScreen);