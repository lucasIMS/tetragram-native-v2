export default styles = StyleSheet.create({
    header:{
		width:'100%',
		backgroundColor:'#0196E9',
		flexDirection:'row',
		justifyContent: 'space-between',
		alignItems: 'center'
    },
    titleView:{
		height:20,
		alignItems:'center',
		justifyContent:'center'
    },
    titleText:{
		fontSize:17,
		color:'white',
		fontWeight:'600'
	},
})

;