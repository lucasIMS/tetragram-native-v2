import React, { Component } from 'react';
import {
    Image,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    SafeAreaView,
    Platform,
    ScrollView,
    StyleSheet,
    ImageBackground,
    Dimensions,
    FlatList,
    TouchableWithoutFeedback
} from 'react-native';
import { Actions } from 'react-native-router-flux';
// import  styles  from './styles';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";


// components
import Button from '../../common/button';
import BottomPopup from '../../common/bottomPopup';
import Loader from '../../common/loader';

// redux Action 
import { getProfileDetails, PutProfileDetails, getSymptoms, editSymptoms, delSymptoms } from "../../actions/authAction";

function wp(percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const slideHeight = viewportHeight * 0.36;
const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);

const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;
import Modal from "react-native-modal";
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import GestureRecognizer, { swipeDirections } from 'react-native-swipe-gestures';


class ProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = { // initilize state
            email: '',
            ErrMsg: '',
            password: '',
            deviceToken: '',
            displayname: '',
            loader: false,
            isSecure: true,
            isCustomeSymtoms: false,
            filterData: [],
            types1: [{ label: 'Psychological', value: 0 }, { label: 'Pain', value: 1 }, { label: 'Physical', value: 3 }],
            symptionData: [],
            users: [],
            currentSysIndex: ''
        }
        this.symtomsList = this.symtomsList.bind(this)


    }

    componentDidMount() {
        this.setState({ loader: true })
        this.props.action.getProfileDetails(this.props.userDetail)

    }


    async componentWillReceiveProps(nextProps) {
        debugger
        if (Actions.currentScene == "ProfileScreen") await this.setState({ loader: nextProps.loader })
        if (nextProps.status === '2000') {

            this.setState({ symptionData: nextProps.fetchSymptions })
        }
        else if (nextProps.status === '20000') {
            this.props.action.getSymptoms('api/symptoms?createdBy.id=' + this.props.userDetail.user, this.props.userDetail)

            this.setState({ email: nextProps.fetchUserInfo.username, displayname: nextProps.fetchUserInfo.displayname, zipCode: nextProps.fetchUserInfo.zip_code })
            debugger
        }
        else if (nextProps.status === '3000') {

            this.setState({ symptionData: nextProps.fetchSymptions })
            debugger
        }
        else if (nextProps.status === '2050') {

            this.props.action.getSymptoms('api/symptoms?createdBy.id=' + this.props.userDetail.user, this.props.userDetail)
            debugger
        }
        else if (nextProps.status === '2060') {

            this.props.action.getSymptoms('api/symptoms?createdBy.id=' + this.props.userDetail.user, this.props.userDetail)
            debugger
        }

        else if (nextProps.status === '4444') {
            this.props.action.getSymptoms('api/symptoms?createdBy.id=' + this.props.userDetail.user, this.props.userDetail)

            setTimeout(() => {
                this.setState({ ErrMsg: '', })
            }, 2000);
            this.setState({ ErrMsg: nextProps.err_Message, })
        }
    }




    onCheckInvision() {
        setTimeout(() => {
            this.setState({ isSecure: !this.state.isSecure })
        }, 200);
    }

    onSaveProfile() {
        let PostData = []
        if (this.state.password !== '') {

        }
        if (this.state.zipCode == '') {
            alert('Zip code field cannot be empty')

        }
        else if (this.state.displayname == '') {
            alert('Display field cannot be empty')

        }
        else {
            PostData = {
                "zipCode": this.state.zipCode,
                "displayname": this.state.displayname
            }
            this.props.action.PutProfileDetails(PostData, this.props.userDetail)


        }

    }

    symtomsList = ({ item, index }) => {
        return (
            <View style={{ marginHorizontal: 30, flexDirection: 'row', justifyContent: 'space-between', paddingTop: 15, borderBottomWidth: 0.5, borderBottomColor: '#C7C8C9', paddingBottom: 5 }}>
                <Text style={{ fontSize: 14, color: 'black', fontFamily: 'OpenSans', }}>{item.name}</Text>
                <TouchableOpacity onPress={() => {
                    if (item.category == 'Psychological') {
                        this.setState({ isRadio: 0 })
                    }
                    else if (item.category == 'Pain') {
                        this.setState({ isRadio: 1 })

                    }
                    else {
                        this.setState({ isRadio: 2 })

                    }

                    this.setState({ isCustomeSymtoms: true, currentSysIndex: index, filterData: item, pickStrainValue: item.name })
                }} style={{ flexDirection: 'row' }}>
                    <Text style={{ fontSize: 10, color: 'black', fontFamily: 'OpenSans', opacity: 0.5 }}>{item.category}</Text>
                    <Image source={require('../../assets/images/img/icon-pencil--large.png')} style={{ height: 15, width: 15, marginLeft: 10 }}></Image>


                </TouchableOpacity>
            </View>

        )
    }

    saveSymtom() {
        debugger
        let category = ''
        if (this.state.isRadio == 0) {
            category = 'Psychological'
        }
        else if (this.state.isRadio == 1) {
            category = 'Pain'
        }
        else {
            category = 'Physical'
        }
        let formData = new FormData();
        formData.append('name', this.state.pickStrainValue)
        formData.append('category', category)
        formData.append('createdBy', this.props.userDetail.user)

        let PostData = {
            "name": this.state.pickStrainValue,
            "category": category,
            "createdBy": this.props.userDetail.user
        }
        let filterData = this.state.filterData['@id']
        let newData = filterData.substring(1)

        debugger

        this.props.action.editSymptoms(formData, newData, this.props.userDetail)
        this.setState({ isModalVisibleSearch: false, isCustomeSymtoms: false, isPickStrain: true })
    }


    delSymtom() {
        debugger

        let filterData = this.state.filterData['@id']
        let newData = filterData.substring(1)
        debugger

        this.props.action.delSymptoms(newData, this.props.userDetail)
        this.setState({ isModalVisibleSearch: false, isCustomeSymtoms: false, isPickStrain: true })
    }

    async onSwipeDown() {
        debugger
        await this.setState({ isCustomeSymtoms: false, isCustomeSymtoms: false })
    }


    render() {
        const config = {
            velocityThreshold: 2.0,
            directionalOffsetThreshold: 120

        };
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#2C7DB3' }}>

                <View style={{ flex: 1 }}>
                    <View style={{
                        flex: Platform.OS == 'ios' ? 0.07 : 0.1,
                        width: '100%',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center'
                    }}>
                        <TouchableOpacity
                            onPress={() => Actions.Experince_Timeline()}>

                            <Image resizeMode='contain' style={{ height: 20, width: 140 }} source={require('../../assets/images/img/logo_header.png')} />
                        </TouchableOpacity>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <TouchableOpacity onPress={() => Actions.ReviewScreen()} style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Image resizeMode='contain' style={{ height: 20, width: 20 }} source={require('../../assets/images/img/icon_speech_bubble.png')} />
                                <Text style={{ color: 'white', fontSize: 12, fontFamily: 'OpenSans' }}>
                                    Reviews
                         </Text>
                            </TouchableOpacity>


                            <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 10, borderBottomWidth: 1, borderBottomColor: 'white' }}>
                                <Image resizeMode='contain' style={{ height: 20, width: 20 }} source={require('../../assets/images/img/icon_profile.png')} />
                                <Text style={{ color: 'white', fontSize: 12, fontFamily: 'OpenSans' }}>
                                    Profile
                        </Text>
                            </View>


                        </View>
                    </View>
                    <View style={{ flex: 1, backgroundColor: 'white' }}>
                        <ScrollView style={{ flex: 1 }}>
                            <Text style={{
                                fontSize: 24,
                                color: '#6559A0',
                                fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                marginHorizontal: 30,
                                marginTop: 23,
                                marginBottom: 15
                            }}>Profile</Text>
                            <Text style={{ fontSize: 18, color: 'black', marginLeft: 30, fontFamily: 'OpenSans' }}>Email Address</Text>
                            <TextInput
                                value={this.state.email}
                                placeholder='johnny@appleseed.com'
                                style={{ borderWidth: 1, borderRadius: 10, height: 40, marginTop: 10, marginHorizontal: 30, paddingHorizontal: 10 }}>
                            </TextInput>

                            <Text style={{ fontSize: 18, color: 'black', marginLeft: 30, marginTop: 30, fontFamily: 'OpenSans' }}>Display Name</Text>
                            <TextInput
                                onChangeText={(displayname) => this.setState({ displayname })}
                                value={this.state.displayname}
                                style={{ borderWidth: 1, borderRadius: 10, height: 40, marginTop: 10, marginHorizontal: 30, paddingHorizontal: 10 }}>
                            </TextInput>

                            <Text style={{ fontSize: 18, color: 'black', marginLeft: 30, marginTop: 30, fontFamily: 'OpenSans' }}>Password</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <TextInput secureTextEntry={this.state.isSecure}
                                    onChangeText={(password) => this.setState({ password })}
                                    value={this.state.password}

                                    style={{ flex: 1, borderWidth: 1, borderRadius: 10, height: 40, marginTop: 10, marginHorizontal: 30, paddingLeft: 10, paddingRight: 40 }}>

                                </TextInput>
                                <TouchableOpacity style={{ alignSelf: 'center', position: 'absolute', right: 20, marginRight: 10, }} onPress={() => this.onCheckInvision()}>
                                    <Image source={this.state.isSecure ? require('../../assets/images/img/icon_eye_black.png') : require('../../assets/images/img/eyesCross.png')} style={{ height: 20, width: 20, marginRight: 10, marginTop: 8 }}></Image>

                                </TouchableOpacity>
                            </View>
                            <Text style={{ fontSize: 18, color: 'black', marginLeft: 30, marginTop: 30, fontFamily: 'OpenSans' }}>Zip Code</Text>
                            <TextInput
                                value={this.state.zipCode}
                                onChangeText={(zipCode) => this.setState({ zipCode })}

                                style={{ borderWidth: 1, borderRadius: 10, height: 40, marginTop: 10, marginHorizontal: 30, paddingHorizontal: 10 }}>
                            </TextInput>
                            {this.state.symptionData && this.state.symptionData.length > 0 ?
                                <Text style={{ fontSize: 18, color: 'black', marginLeft: 30, marginTop: 30, fontFamily: 'OpenSans' }}>Custom Symptoms</Text>
                                : null}


                            <FlatList
                                style={{ flex: 1, backgroundColor: 'white' }}
                                data={this.state.symptionData}
                                showsVerticalScrollIndicator={false}
                                renderItem={this.symtomsList
                                }
                                keyExtractor={item => item.email}

                            />


                            {this.state.isCustomeSymtoms ?
                                // <GestureRecognizer
                                //     onSwipeDown={(state) => this.onSwipeDown(state)}
                                //     config={config}
                                //     style={{
                                //         backgroundColor: 'red'
                                //     }}
                                // >
                                <Modal
                                    onSwipeComplete={() => this.setState({ isCustomeSymtoms: false })}
                                    onSwipeThreshold={20}
                                    swipeDirection="down"
                                    style={{ margin: 0, marginTop: 60 }}
                                    isVisible={this.state.isCustomeSymtoms}>
                                    <View style={{ flex: 1, }}>
                                        <View style={{ flex: 1, backgroundColor: 'white' }}>
                                            <ScrollView style={{ flex: 1 }}>
                                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                    <Image source={require('../../assets/images/img/icon_pull_handle.png')} style={{ width: 90, height: 5, marginTop: 10 }} />

                                                </View>
                                                <Text style={{
                                                    fontSize: 26,
                                                    color: '#6559A0',
                                                    fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                                    marginLeft: 25,
                                                    marginTop: 40
                                                }}>Edit Symptom</Text>


                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
                                                    <TextInput
                                                        onChangeText={(pickStrainValue) =>
                                                            this.setState({ pickStrainValue })}
                                                        value={this.state.pickStrainValue}
                                                        placeholder=''
                                                        style={{ flex: 1, borderWidth: 1, borderRadius: 10, height: 40, marginHorizontal: 20, paddingLeft: 10, paddingRight: 40 }}>

                                                    </TextInput>

                                                </View>
                                                <RadioForm
                                                    style={{ marginTop: 10, flex: 1, marginLeft: 20 }}
                                                    ref="radioForm"
                                                    radio_props={this.state.types1}
                                                    initial={this.state.isRadio}
                                                    formHorizontal={true}
                                                    labelHorizontal={true}
                                                    buttonColor={'gray'}
                                                    labelColor={'#000'}
                                                    selectedButtonColor={'#3089CA'}
                                                    animation={true}
                                                    buttonSize={6}
                                                    buttonOuterSize={16}
                                                    labelStyle={{ marginLeft: 5, marginRight: 15 }}
                                                    buttonWrapStyle={{ marginTop: 40 }}
                                                    onPress={(value, index) => {
                                                        this.setState({
                                                            value1Index: value,
                                                            value1Index: index
                                                        })
                                                    }}
                                                />

                                                <TouchableOpacity onPress={() => this.delSymtom()} style={{ borderWidth: 1, borderRadius: 16, height: 40, marginTop: 30, marginLeft: 20, width: 200, justifyContent: 'center', alignItems: 'center', backgroundColor: 'red', borderColor: 'transparent' }}>
                                                    <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 12, fontFamily: 'OpenSans', marginHorizontal: 10 }}>Delete custom symptom</Text>
                                                </TouchableOpacity>


                                                <TouchableOpacity onPress={() => this.saveSymtom()} style={{ borderWidth: 1, borderRadius: 16, height: 40, marginTop: 30, marginHorizontal: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: '#3089CA', marginBottom: 20, borderColor: 'transparent' }}>
                                                    <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 12, fontFamily: 'OpenSans' }}>Save Symptom</Text>
                                                </TouchableOpacity>

                                            </ScrollView>
                                        </View>
                                    </View>
                                </Modal>
                                : null}



                            <TouchableOpacity onPress={() => this.onSaveProfile()} style={{ borderWidth: 1, borderRadius: 16, height: 40, marginTop: 40, marginHorizontal: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: '#3089CA', marginBottom: 20, borderColor: 'transparent' }}>
                                <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 16, fontFamily: 'OpenSans' }}>Save Profile</Text>
                            </TouchableOpacity>
                        </ScrollView>
                    </View>
                </View>


                <BottomPopup message={this.state.ErrMsg} />
                <Loader visible={this.state.loader} />
            </SafeAreaView>


        );
    }


}



const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    },

    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderBottomWidth: .5,
        borderColor: '#000',
        height: 40,
        marginHorizontal: 20,
        marginVertical: 10,
    },

    ImageStyle: {
        padding: 10,
        margin: 5,
        height: 25,
        width: 25,
        resizeMode: 'stretch',
        alignItems: 'center'
    },

});

const mapStateToProps = state => {
    return {
        userDetail: state.auth.userDetail,
        fetchSymptions: state.auth.fetchSymptions,
        fetchUserInfo: state.auth.fetchUserInfo,
        status: state.auth.status,
        err_Message: state.auth.err_Message,
        loader: state.auth.loader,
        updataSymptios: state.auth.updataSymptios,

    }
}

const mapDispatchToProps = dispatch => {
    return {
        action: bindActionCreators({ getProfileDetails, PutProfileDetails, getSymptoms, editSymptoms, delSymptoms }, dispatch)
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);