import {Dimensions} from 'react-native';

export const styles = {
	img_container:{
		flex:1,
		backgroundColor:'white'
	},
	splash:{
		height: Dimensions.get('window').height,
		width: Dimensions.get('window').width
	},
	loginButtonView:{
        justifyContent:'center',
        alignItems:'center',
        width:'80%',
		height:60,
		borderRadius:1
    },
    buttonLogin:{
    	backgroundColor:'white',
    	width:'100%',
    	padding:13,
    	borderRadius:30,
    	alignItems:'center',
    	justifyContent:'center'
    },
	loginText:{
		color:'#6559A0',
		fontWeight:'600',
		fontSize:18
	},


	SignUpButtonView:{
		marginVertical: 20,
        justifyContent:'center',
        alignItems:'center',
        alignSelf: 'center',
        width:'80%',
		height:60,
		borderRadius:1,
    },
    buttonSignUp:{
    	backgroundColor:'white',
    	width:'100%',
    	padding:13,
    	borderRadius:30,
    	alignItems:'center',
		justifyContent:'center',
		backgroundColor: 'transparent',
		borderWidth: 1,
		borderColor: 'white',


    },
	signupText:{
		color:'white',
		fontWeight:'bold',
		fontSize:12,
		fontFamily: 'OpenSans'
	},
	ForgotText:{
		marginTop:45,
		color:'white',
		fontWeight:'bold',
		fontSize:12,
		textAlign:'center',
		fontFamily: 'OpenSans',
		marginBottom: 20,
	},

};

export default styles;