import React, { Component } from 'react';
import {
    StyleSheet,
    Image,
    View,
    Text,
    TextInput,
    Dimensions,
    TouchableOpacity,
    ImageBackground,
    SafeAreaView,
    Platform,
    ScrollView,
    Keyboard
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { styles } from './styles';
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";
import Loader from '../../common/loader';

// components
import Button from '../../common/button';
import BottomPopup from '../../common/bottomPopup';
import { WebView } from 'react-native-webview';
import Modal from "react-native-modal";

import { GoogleSignin, statusCodes } from 'react-native-google-signin';



// redux Action 
import { SignIn, ClearAction } from "../../actions/authAction";
function wp(percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const slideHeight = viewportHeight * 0.36;
const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);

const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;
class NewLogin extends Component {
    state = {
        email: '',
        message: '',
        loader: false,
        ErrMsg: '',

    }



    async componentWillReceiveProps(nextProps) {
        debugger
        if (Actions.currentScene == "newLogin") await this.setState({ loader: nextProps.loader })
        if (nextProps.status === '5001') {
            this.setState({ ErrMsg: nextProps.err_Message, })
        }
        else if (nextProps.status === '200') {
            debugger
            Actions.reset('Experince_Timeline') // navigate dashboard screen.
            this.clearReducerState();
            this.setState({ email: '', password: '' })
        }
    
        else if (nextProps.status === '401') {
            debugger
            this.setState({ ErrMsg: nextProps.err_Message })
        }
        else if (nextProps.status === '400') {
            debugger
            this.setState({ ErrMsg: nextProps.err_Message })
        }
    }

    componentDidMount() {
        this._setupGoogleSignin()
    }


    async _setupGoogleSignin() {
        try {
            await GoogleSignin.hasPlayServices({ autoResolve: true, showPlayServicesUpdateDialog: true });
            await GoogleSignin.configure({
                androidClientId: '1036113728659-eqfkmugrr16vghofook7ipht43gegptl.apps.googleusercontent.com',
                // iosClientId: '1036113728659-dtem9lc4pars7jgtpropdqmeg3q4ccrl.apps.googleusercontent.com',
                offlineAccess: true,
                hostedDomain: '',
                forceConsentPrompt: true,
                webClientId: '1036113728659-dtem9lc4pars7jgtpropdqmeg3q4ccrl.apps.googleusercontent.com',
            });
            // const user = await GoogleSignin.currentUserAsync();
            this.getCurrentUserInfo()
        }
        catch (err) {
            console.log("googleSignIn err===>", err)
        }
    }

    getCurrentUserInfo = async () => {
        try {
            const userInfo = await GoogleSignin.signInSilently();
            this.setState({ userInfo });
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_REQUIRED) {
                // user has not signed in yet
            } else {
                // some other error
            }
        }
    };




    _gmailSignIn = async () => {
        // GoogleSignin.signOut()
        // return
        this.setState({ loader: true })
        GoogleSignin.signIn()
            .then((googleData) => {
                // GoogleSignin.signOut()
                //     .then(() => {
                //     })
                //     .catch((err) => {

                //     });
                this.setState({ loader: false })
                console.log("googleSignIn====>", JSON.stringify(googleData))
                var googleReqData = {
                    "username": googleData.user.email || "",
                    "displayname": googleData.user.name || "",
                
                }
                Actions.reset('Experince_Timeline') // navigate dashboard screen.

                
            })
            .catch((err) => {
                this.setState({ loader: false })
                console.log("googleLoginerror====>", err)
                // this.cancelLoader()
            })
            .done();
    };





    componentWillUnmount() {
        this.clearReducerState();
    }

    clearReducerState = () => { // reset initial state.
        this.props.action.ClearAction()
    }


    _resetPassword = () => {
        this._resetPasswordAPI()
    }

    _loginApi = () =>{
        
        if(this.state.email == ''){
                alert('Please enter email')
        }
        else if(this.state.pass){
            alert('Please enter password')

        }
        else
        {
            let formData = new FormData();
            formData.append('_username',this.state.email)
            formData.append('_password',this.state.password)
            this.props.action.SignIn(formData)

        }
    }

    render() {
        return (
            <ImageBackground style={{ height: '100%', width: '100%' }} resizeMode='cover' source={require('../../assets/images/img/background.png')}>
                <SafeAreaView style={{ flex: 1 }}>
                    <ScrollView style={{ flex: 1 }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center',marginTop:20 }}>
                            <Image style={{ height: 120, width: 160, }} resizeMode='contain' source={require('../../assets/images/img/main_logo.png')} />
                        </View>
                        <View style={{ overflow: 'visible', marginTop: 40, backgroundColor: 'white', width: viewportWidth - 40, borderRadius: 10, alignSelf: 'center' }}>
                            <TouchableOpacity onPress={() =>this._gmailSignIn()} style={{ borderWidth: 1, borderRadius: 16, height: 44, marginHorizontal: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: '#3089CA', marginTop: 20,borderColor:'transparent' }}>
                                <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 12,fontFamily: 'OpenSans' }}>Login with Google</Text>

                            </TouchableOpacity>
                            <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 15,marginBottom:10, flexDirection: 'row', }}>
                                <View style={{ borderWidth: 0.3, borderColor: '#E7E7E9', flex: 1, marginLeft: 20 }}></View>
                                <View style={{ height: 26, width: 26, borderRadius: 13, backgroundColor: '#D8DAD9', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: 'black', textAlign: 'center', fontSize: 10, fontWeight: 'bold',fontFamily: 'OpenSans' }}>
                                        OR
                        </Text>

                                </View>
                                <View style={{ borderWidth: 0.3, borderColor: '#E7E7E9', flex: 1, marginRight: 20 }}></View>

                            </View>

                            <Text style={{ fontSize: 18, color: 'black', marginLeft: 20, fontFamily: 'OpenSans',fontWeight:'normal'}}>Email Address</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <TextInput
                                 onChangeText={(email) => this.setState({email})}
                                 value={this.state.email}
                                 keyboardType = 'email-address'

                                style={{ flex: 1, borderWidth: 1, borderRadius: 10, height: 40, marginTop: 10, marginHorizontal: 20, paddingLeft: 10, paddingRight: 40 }}>

                                </TextInput>
                            </View>
                            <Text style={{ fontSize: 18, color: 'black', marginLeft: 20, marginTop: 20,fontFamily: 'OpenSans',fontWeight:'normal' }}>Password</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <TextInput
                                secureTextEntry={true}
                                 onChangeText={(password) => this.setState({password})}
                                 value={this.state.password}
                                style={{ flex: 1, borderWidth: 1, borderRadius: 10, height: 40, marginTop: 10, marginHorizontal: 20, paddingLeft: 10, paddingRight: 40 }}>

                                </TextInput>
                            </View>
                            <TouchableOpacity onPress={() => this._loginApi()} style={{ borderColor:'transparent',borderWidth: 1, borderRadius: 16, height: 44, marginTop: 40, marginHorizontal: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: '#3089CA', marginTop: 20, marginBottom: 20 }}>
                                <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 16,fontFamily: 'OpenSans' }}>Sign In</Text>
                            </TouchableOpacity>
                          
                        </View>
                        <View style={[styles.SignUpButtonView]}>
                            <TouchableOpacity style={styles.buttonSignUp} onPress={() => Actions.forgotpassword()}>
                                <Text style={styles.signupText}>Forgot Password</Text>
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity onPress = {()=>this.setState({isModalVisible:true})}>
                        <Text style={styles.ForgotText}>Privacy Policy</Text>
                        </TouchableOpacity>
                            <Loader visible={this.state.loader} />
                    </ScrollView>
                    <BottomPopup message={this.state.ErrMsg} />
                    <Modal style={{
                                margin: 0,
                                
                            }} isVisible={this.state.isModalVisible}>
                                <SafeAreaView style= {{flex:1}}>
                                <View style={{ backgroundColor: 'white', borderRadius: 10 }}>
                                    <View style={{
                                        height: 30,
                                        justifyContent: 'flex-end', 
                                        flexDirection: 'row',
                                        backgroundColor: '#EDF3F6',
                                        borderRadius: 5
                                    }}>
                                      
                                        <TouchableOpacity onPress={() => this.setState({ isModalVisible: false })}>
                                            <Image source={require('../../assets/images/img/icon-close.png')} style={{ height: 10, width: 20, alignSelf: 'center', marginTop: 10, marginRight: 10 }} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{height: viewportHeight-60}}>
                                    <WebView source={{ uri: 'http://demo.ttragram.com/privacy' }} />

                                    </View>
                                 
                                </View>
                                </SafeAreaView>
                            </Modal>
                </SafeAreaView>
            </ImageBackground>
        );
    }
}

const mapStateToProps = state => {
    return {
        status: state.auth.status,
        err_Message: state.auth.err_Message,
        loader: state.auth.loader
    }
}

const mapDispatchToProps = dispatch => {
    return {
        action: bindActionCreators({ClearAction,SignIn }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewLogin);