
// include react-native-swipeout

import Swipeout from 'react-native-swipeout';
import { Actions } from 'react-native-router-flux';
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    SafeAreaView,
    Text,
    View,
    ImageBackground,
    TouchableWithoutFeedback,
    FlatList,
    Image,
    Platform,
    Keyboard
} from 'react-native';
import Modal from "react-native-modal";
import SplashScreen from 'react-native-splash-screen'
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import StarRating from 'react-native-star-rating';
import Loader from '../../common/loader';

import { getTimelineList, delTimline, getSymptoms, getMethods, getSpecificMethods, getSpecificType } from "../../actions/authAction";
import Moment from 'moment';





class Experince_Timeline extends Component {
    constructor() {
        super();
        this.state = {
            isModalVisibleSearch: false,
            sessionData: [],
            selectedModal: [],
            session_symptoms: []
        };
    }
    componentDidMount() {
        this.props.action.getTimelineList(this.props.userDetail)
        setTimeout(() => {
            SplashScreen.hide();
        }, 3000);
    }
    async componentWillReceiveProps(nextProps) {
        if (Actions.currentScene == "Experince_Timeline")
            if (nextProps.status === '2095') {
                let seesionFilter = []
                let data = nextProps.sessionResult
                sessionFiltter = data.filter(function (item) {
                    seesionFilter.push(item.strain.name)
                })
                const uniqueNames = seesionFilter.filter((val, id, array) => {
                    debugger
                    return array.indexOf(val) !== id;
                });
                var result = groupBy(data, function (item) {
                    return item.strain.name;
                });
                this.setState({ sessionData: result })
                this.props.action.getSymptoms('api/symptoms', this.props.userDetail)
            }
            else if (nextProps.status === '2096') {

                this.setState({ isModalVisibleSearch: false })
                this.props.action.getTimelineList(this.props.userDetail)

            }
            else if (nextProps.status === '4999') {
                debugger

                this.setState({ isModalVisibleSearch: false })
                this.props.action.getTimelineList(this.props.userDetail)

            }
            else if (nextProps.status === '2000') {

                this.props.action.getMethods(this.props.userDetail)
            }
            else if (nextProps.status === '2902') {
                let filterMethod = []
                var methodtype = nextProps.methodList.filter(function (item) {
                    filterMethod.push(item.name)
                    return item.name
                });
                this.props.action.getSpecificMethods(this.props.userDetail)
                this.setState({ method_list: nextProps.methodList, filterMethod: filterMethod })
            }
            else if (nextProps.status === '2903') {
                debugger
                await this.setState({ loader: nextProps.loader })

                this.props.action.getSpecificType(this.props.userDetail)

            }



    }




    swipeoutBtns = (value) => [
        {
            component:
                (
                    <TouchableOpacity onPress={() => this._delsessionSwape(value)} style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                        <Image
                            source={require('../../assets/images/img/icon-trash.png')}
                            style={{ alignSelf: 'center', height: 30, width: 30 }} />
                    </TouchableOpacity>
                )
        }
    ]

    swipeoutBtnsAdd = (value) => [

        {
            component:
                (
                    <TouchableOpacity onPress={() => Actions.editTimeline({ symptomPass: value })}
                        style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                        <Image
                            source={require('../../assets/images/img/icon_pencil.png')}
                            style={{ alignSelf: 'center', height: 30, width: 30 }} />
                    </TouchableOpacity>
                )
        }
    ]






    renderMactItem = ({ item, index }) => {
        Moment.locale('en');
        let firstCara = ''
        let itemName = item[0].strain.type
        if (itemName && itemName !== undefined) {


            firstCara = itemName[0].charAt(0)
        }
        if (item.length > 1) {
            return (
                <View>
                    {this.state.ischecked == index ?
                        <View>
                            {item.map((data) => {
                                var dt = data.date;

                                return (
                                    <View>
                                        <Swipeout
                                            autoClose={true}
                                            style={{
                                                borderRadius: 8,
                                                borderWidth: 1,
                                                marginHorizontal: 20,
                                                borderColor: '#E7E7E9',
                                                backgroundColor: 'white',
                                                borderRadius: 8,
                                            }}
                                            left={this.swipeoutBtnsAdd(data)}
                                            right={this.swipeoutBtns(data['@id'])}
                                        >

                                            <TouchableWithoutFeedback onPress={() => this.setState({
                                                isModalVisibleSearch: true,
                                                passData: data,
                                                strainName: data.strain.name !== '' ? data.strain.name : '',
                                                short_symtopms: data.strain.type.charAt(0),
                                                method: data.method.name,
                                                specific_type: data.specific_type ? data.specific_type.name : '',
                                                session_symptoms: data.session_symptoms,
                                                fetchDate: (Moment(dt).format('DD/MM/YYYY H:mm A')),
                                                treatment_rating: data.treatment_rating,
                                                specific_Text: data.text,
                                                deleteSessionID: data['@id'],
                                                userImg: data.image.originalUrl

                                            })}>
                                                <View style={styles.flatview}>
                                                    <View style={{
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                    }}>


                                                        <View
                                                            style={{
                                                                margin: 10, height: 80, borderRadius: 40
                                                            }}
                                                        >

                                                        </View>

                                                        <View>
                                                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                                                <Text style={{
                                                                    fontSize: 20,
                                                                    textAlign: 'center',
                                                                    fontWeight: 'normal',
                                                                    fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                                                    color: '#121824'
                                                                }}>{data.strain.name ? data.strain.name : ''}</Text>
                                                                <View style={{
                                                                    backgroundColor: '#BFDEEC',
                                                                    borderRadius: 4,
                                                                    alignItems: 'center',
                                                                    height: 20,
                                                                    width: 18,
                                                                    justifyContent: 'center',
                                                                    alignItems: 'center',
                                                                    alignSelf: 'center',
                                                                    marginLeft: 4
                                                                }}>
                                                                    <Text style={{
                                                                        fontSize: 10,
                                                                        fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                                                        textAlign: 'center',

                                                                        fontWeight: 'bold',
                                                                        lineHeight: 12
                                                                    }}>{data.strain.type.charAt(0)}</Text>
                                                                </View>

                                                            </View>
                                                            <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                                <Text style={{
                                                                    fontSize: 12,
                                                                    textAlign: 'center',
                                                                    fontFamily: 'OpenSans',
                                                                    color: '#121824',
                                                                    fontWeight: 'bold',
                                                                    lineHeight: 16
                                                                }}>{data.method.name ? data.method.name : ''}
                                                                </Text>
                                                                <Text style={{
                                                                    fontSize: 12,
                                                                    textAlign: 'center',
                                                                    fontFamily: 'OpenSans',
                                                                    marginHorizontal: 10,
                                                                    color: '#121824',
                                                                    fontWeight: 'bold',
                                                                    lineHeight: 16
                                                                }}>-</Text>

                                                                <Text style={{
                                                                    fontSize: 12,
                                                                    textAlign: 'center',
                                                                    fontFamily: 'OpenSans',
                                                                    color: '#121824',
                                                                    fontWeight: 'bold',
                                                                    lineHeight: 16
                                                                }}>{data.specific_type ? data.specific_type.name : ''}</Text>
                                                            </View>
                                                        </View>

                                                    </View>
                                                    <View style={{
                                                        justifyContent: 'space-between',
                                                        alignItems: 'center',
                                                        borderBottomEndRadius: 8,
                                                        borderBottomLeftRadius: 8,
                                                        backgroundColor: '#E7E7E9',
                                                        flexDirection: 'row',
                                                        borderWidth: 1,
                                                        borderColor: '#E5E5E7',
                                                        paddingLeft: 13,
                                                        paddingRight: 9,
                                                        marginBottom: 0.5
                                                    }} >
                                                        <Text style={{
                                                            fontFamily: 'OpenSans',
                                                            fontSize: 12,
                                                            color: '#121824',
                                                            fontWeight: 'bold',
                                                            lineHeight: 16
                                                        }}>
                                                            {Moment(dt).format('MM/DD/YYYY H:mm A')}
                                                        </Text>
                                                        <View>

                                                            <StarRating
                                                                activeOpacity={1}
                                                                disabled={false}
                                                                starSize={16}
                                                                maxStars={5}
                                                                starStyle={{
                                                                    marginVertical: 5,
                                                                    marginRight: 5,
                                                                    color: '#2E7FAE'
                                                                }}
                                                                rating={data.treatment_rating}
                                                            />
                                                        </View>
                                                    </View>
                                                    <View>

                                                    </View>

                                                </View>
                                            </TouchableWithoutFeedback>

                                        </Swipeout>



                                        <Image
                                            style={{
                                                width: 5,
                                                height: 10,
                                                alignSelf: 'center',
                                                backgroundColor: '#A0A3A7'
                                            }}
                                            resizeMode='contain'
                                            source={require('../../assets/images/img/Pipe.png')} />
                                    </View>
                                )
                            })}

                        </View> :
                        <View>
                            <View style={{
                                borderRadius: 8,
                                borderWidth: 1,
                                marginHorizontal: 20,
                                borderColor: '#E7E7E9'
                            }}>


                                <View style={styles.flatview}>
                                    <View style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                    }}>


                                        <View
                                            style={{
                                                margin: 10, height: 80, borderRadius: 40
                                            }}
                                        >

                                        </View>

                                        <View>
                                            <View style={{ flexDirection: 'row', justifyContent: 'center', width: '90%' }}>
                                                <Text style={{
                                                    fontSize: 20,
                                                    fontWeight: 'normal',

                                                    fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                                    color: '#121824'
                                                }}>{item[0].strain.name ? item[0].strain.name : ''}</Text>
                                                <View style={{
                                                    backgroundColor: '#BFDEEC',
                                                    borderRadius: 4,
                                                    alignItems: 'center',
                                                    height: 20,
                                                    width: 18,
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                    alignSelf: 'center',
                                                    marginLeft: 4
                                                }}>
                                                    <Text style={{
                                                        fontSize: 10,
                                                        fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                                        textAlign: 'center',

                                                        fontWeight: 'bold',
                                                        lineHeight: 12
                                                    }}>{firstCara}</Text>
                                                </View>

                                            </View>
                                            <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                <Text style={{
                                                    fontSize: 12,
                                                    textAlign: 'center',
                                                    fontFamily: 'OpenSans',
                                                    color: '#121824',
                                                    fontWeight: 'bold',
                                                    lineHeight: 16
                                                }}>{item[0].method.name ? item[0].method.name : ''}
                                                </Text>
                                                <Text style={{
                                                    fontSize: 12,
                                                    textAlign: 'center',
                                                    fontFamily: 'OpenSans',
                                                    marginHorizontal: 10,
                                                    color: '#121824',
                                                    fontWeight: 'bold',
                                                    lineHeight: 16
                                                }}>-</Text>

                                                <Text style={{
                                                    fontSize: 12,
                                                    textAlign: 'center',
                                                    fontFamily: 'OpenSans',
                                                    color: '#121824',
                                                    fontWeight: 'bold',
                                                    lineHeight: 16
                                                }}>{item[0].specific_type ? item[0].specific_type.name : ''}</Text>
                                            </View>
                                        </View>

                                    </View>
                                </View>

                                {item.length == 0 ?
                                    <View style={{
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                        borderBottomEndRadius: 8,
                                        borderBottomLeftRadius: 8,
                                        backgroundColor: '#E7E7E9',
                                        flexDirection: 'row',
                                        borderWidth: 1,
                                        borderColor: '#E5E5E7',
                                        paddingLeft: 13,
                                        paddingRight: 9,
                                        marginBottom: 0.5
                                    }} >
                                        <Text style={{
                                            fontFamily: 'OpenSans',
                                            fontSize: 12,
                                            color: '#121824',
                                            fontWeight: 'bold',
                                            lineHeight: 16
                                        }}>
                                            {item[0].date}
                                        </Text>
                                        <View>

                                            <StarRating
                                                activeOpacity={1}
                                                disabled={false}
                                                starSize={16}
                                                maxStars={5}
                                                starStyle={{
                                                    marginVertical: 5,
                                                    marginRight: 5,
                                                    color: '#2E7FAE'
                                                }}
                                                rating={item[0].treatment_rating}
                                            />
                                        </View>
                                    </View> :
                                    <View>
                                        {item.map((data) => {
                                            var dt = data.date;

                                            return (
                                                <TouchableOpacity onPress={() => this.setState({ ischecked: index })} style={{
                                                    justifyContent: 'space-between',
                                                    alignItems: 'center',
                                                    borderBottomEndRadius: 8,
                                                    borderBottomLeftRadius: 8,
                                                    backgroundColor: '#E7E7E9',
                                                    flexDirection: 'row',
                                                    borderWidth: 1,
                                                    borderColor: '#E5E5E7',
                                                    paddingLeft: 13,
                                                    paddingRight: 9,
                                                    marginBottom: 0.5
                                                }} >
                                                    <Text style={{
                                                        fontFamily: 'OpenSans',
                                                        fontSize: 12,
                                                        color: '#121824',
                                                        fontWeight: 'bold',
                                                        lineHeight: 16
                                                    }}>
                                                        {Moment(dt).format('MM/DD/YYYY H:mm A')}
                                                    </Text>
                                                    <View>

                                                        <StarRating
                                                            activeOpacity={1}
                                                            disabled={false}
                                                            starSize={16}
                                                            maxStars={5}
                                                            starStyle={{
                                                                marginVertical: 5,
                                                                marginRight: 5,
                                                                color: '#2E7FAE'
                                                            }}
                                                            rating={data.treatment_rating}
                                                        />
                                                    </View>
                                                </TouchableOpacity>
                                            )
                                        })}
                                    </View>
                                }

                            </View>
                            <Image
                                style={{
                                    width: 5,
                                    height: index == this.state.sessionData.length - 1 ? 150 : 30,
                                    alignSelf: 'center',
                                    backgroundColor: '#A0A3A7'
                                }}
                                resizeMode='contain'
                                source={require('../../assets/images/img/Pipe.png')} />
                        </View>
                    }
                </View>

            )
        }
        else {
            var dt = item[0].date;
            return (
                <View>

                    <Swipeout
                        autoClose='true'
                        style={{
                            borderRadius: 8,
                            borderWidth: 1,
                            marginHorizontal: 20,
                            borderColor: '#E7E7E9',
                            backgroundColor: 'white',
                            borderRadius: 8,
                        }}
                        left={this.swipeoutBtnsAdd(item[0])}
                        right={this.swipeoutBtns(item[0]['@id'])}
                    >

                        <TouchableWithoutFeedback onPress={() => this.setState({
                            passData: item[0],
                            isModalVisibleSearch: true,
                            strainName: item[0].strain.name !== '' ? item[0].strain.name : '',
                            short_symtopms: firstCara,
                            method: item[0].method.name,
                            specific_type: item[0].specific_type ? item[0].specific_type.name : '',
                            session_symptoms: item[0].session_symptoms,
                            fetchDate: (Moment(dt).format('MM/DD/YYYY H:mm A')),
                            treatment_rating: item[0].treatment_rating,
                            specific_Text: item[0].text,
                            deleteSessionID: item[0]['@id'],
                            userImg: item[0].image.originalUrl


                        })}>
                            <View style={styles.flatview}>
                                <View style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                }}>


                                    {/* <Image
                                                style={{
                                                    margin: 10, height: 80, width: 80, borderRadius: 40
                                                }}
                                                resizeMode='cover'
                                                source={require('../../assets/images/img/food.jpeg')}
                                            /> */}
                                    <View
                                        style={{
                                            margin: 10, height: 80, borderRadius: 40
                                        }}
                                    >

                                    </View>

                                    <View>
                                        <View style={{ flexDirection: 'row', width: '90%' }}>
                                            <Text style={{
                                                fontSize: 20,
                                                fontWeight: 'normal',
                                                fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                                color: '#121824'
                                            }}>{item[0].strain.name ? item[0].strain.name : ''}</Text>
                                            <View style={{
                                                backgroundColor: '#BFDEEC',
                                                borderRadius: 4,
                                                alignItems: 'center',
                                                height: 20,
                                                width: 18,
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                alignSelf: 'center',
                                                marginLeft: 4
                                            }}>
                                                <Text style={{
                                                    fontSize: 10,
                                                    fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                                    textAlign: 'center',

                                                    fontWeight: 'bold',
                                                    lineHeight: 12
                                                }}>{firstCara}</Text>
                                            </View>

                                        </View>
                                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                            <Text style={{
                                                fontSize: 12,
                                                textAlign: 'center',
                                                fontFamily: 'OpenSans',
                                                color: '#121824',
                                                fontWeight: 'bold',
                                                lineHeight: 16
                                            }}>{item[0].method.name ? item[0].method.name : ''}
                                            </Text>
                                            <Text style={{
                                                fontSize: 12,
                                                textAlign: 'center',
                                                fontFamily: 'OpenSans',
                                                marginHorizontal: 10,
                                                color: '#121824',
                                                fontWeight: 'bold',
                                                lineHeight: 16
                                            }}>-</Text>

                                            <Text style={{
                                                fontSize: 12,
                                                textAlign: 'center',
                                                fontFamily: 'OpenSans',
                                                color: '#121824',
                                                fontWeight: 'bold',
                                                lineHeight: 16
                                            }}>{item[0].specific_type ? item[0].specific_type.name : ''}</Text>
                                        </View>
                                    </View>

                                </View>

                                {item.length == 0 ?
                                    <View style={{
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                        borderBottomEndRadius: 8,
                                        borderBottomLeftRadius: 8,
                                        backgroundColor: '#E7E7E9',
                                        flexDirection: 'row',
                                        borderWidth: 1,
                                        borderColor: '#E5E5E7',
                                        paddingLeft: 13,
                                        paddingRight: 9,
                                        marginBottom: 0.5
                                    }} >
                                        <Text style={{
                                            fontFamily: 'OpenSans',
                                            fontSize: 12,
                                            color: '#121824',
                                            fontWeight: 'bold',
                                            lineHeight: 16
                                        }}>
                                            {Moment(dt1).format('MM/DD/YYYY H:mm A')}
                                        </Text>
                                        <View>

                                            <StarRating
                                                activeOpacity={1}
                                                disabled={false}
                                                starSize={16}
                                                maxStars={5}
                                                starStyle={{
                                                    marginVertical: 5,
                                                    marginRight: 5,
                                                    color: '#2E7FAE'
                                                }}
                                                rating={item[0].treatment_rating}
                                            />
                                        </View>
                                    </View> :
                                    <View>
                                        {item.map((data) => {
                                            var dt1 = data.date;
                                            return (
                                                <View style={{
                                                    justifyContent: 'space-between',
                                                    alignItems: 'center',
                                                    borderBottomEndRadius: 8,
                                                    borderBottomLeftRadius: 8,
                                                    backgroundColor: '#E7E7E9',
                                                    flexDirection: 'row',
                                                    borderWidth: 1,
                                                    borderColor: '#E5E5E7',
                                                    paddingLeft: 13,
                                                    paddingRight: 9,
                                                    marginBottom: 0.5
                                                }} >
                                                    <Text style={{
                                                        fontFamily: 'OpenSans',
                                                        fontSize: 12,
                                                        color: '#121824',
                                                        fontWeight: 'bold',
                                                        lineHeight: 16
                                                    }}>
                                                        {Moment(dt1).format('MM/DD/YYYY H:mm A')}
                                                    </Text>
                                                    <View>

                                                        <StarRating
                                                            activeOpacity={1}
                                                            disabled={false}
                                                            starSize={16}
                                                            maxStars={5}
                                                            starStyle={{
                                                                marginVertical: 5,
                                                                marginRight: 5,
                                                                color: '#2E7FAE'
                                                            }}
                                                            rating={data.treatment_rating}
                                                        />
                                                    </View>
                                                </View>
                                            )
                                        })}
                                    </View>
                                }
                            </View>
                        </TouchableWithoutFeedback>

                    </Swipeout>


                    <Image
                        style={{
                            width: 5,
                            height: 30,
                            alignSelf: 'center',
                            backgroundColor: '#A0A3A7'
                        }}
                        resizeMode='contain'
                        source={require('../../assets/images/img/Pipe.png')} />
                </View>

            )
        }


    }
    _delsessionSwape(value) {
        this.setState({ loader: true })

        let newData = value.substring(1)
        this.props.action.delTimline(newData, this.props.userDetail)
    }

    _delsession() {
        this.setState({ loader: true })

        let newData = this.state.deleteSessionID.substring(1)
        this.props.action.delTimline(newData, this.props.userDetail)
    }


    oneditTimeline() {

        this.setState({ isModalVisibleSearch: false })
        Actions.editTimeline({ symptomPass: this.state.passData, })

    }

    render() {
        const lapsList = () => {
            return

        }
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#2C7DB3' }}>
                <TouchableWithoutFeedback onPress={() => { Keyboard.dismiss() }}>
                    <View style={{ flex: 1 }}>
                        <Loader visible={this.state.loader} />

                        <View style={{
                            width: '100%',
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            height: 40,
                        }}>
                            <TouchableOpacity
                                onPress={() => console.log('test')}>
                                <Image resizeMode='contain' style={{ height: 20, width: 140 }} source={require('../../assets/images/img/logo_header.png')} />
                            </TouchableOpacity>

                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <TouchableOpacity onPress={() => Actions.ReviewScreen()}
                                    style=
                                    {{
                                        flexDirection: 'row',
                                        alignItems: 'center'
                                    }}>
                                    <Image resizeMode='contain' style={{ width: 20 }} source={require('../../assets/images/img/icon_speech_bubble.png')} />
                                    <Text style={{ color: 'white', fontSize: 12, fontFamily: 'OpenSans' }}>
                                        Reviews
                         </Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => Actions.ProfileScreen({ userData: this.props.userData })}
                                    style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 10, borderBottomColor: 'white' }}>
                                    <Image resizeMode='contain' style={{ height: 20, width: 20 }} source={require('../../assets/images/img/icon_profile.png')} />
                                    <Text style={{ color: 'white', fontSize: 12, fontFamily: 'OpenSans' }}>
                                        Profile
                        </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ backgroundColor: 'white', flex: 1 }}>


                            <FlatList
                                style={{ width: '100%' }}
                                data={this.state.sessionData}
                                showsVerticalScrollIndicator={false}
                                renderItem={this.renderMactItem}
                                ListHeaderComponent={() =>
                                    <View>
                                        <TouchableOpacity onPress={() => Actions.LoginSection()} style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginBottom: 20, marginTop: 20 }}>


                                            <Image
                                                style={{ alignSelf: 'center', marginLeft: 80 }}
                                                resizeMode='contain'
                                                source={require('../../assets/images/img/Group.png')}
                                            />


                                            <Text style={{
                                                fontFamily: 'OpenSans',
                                                fontSize: 14,
                                                color: '#007DB3',
                                                marginLeft: 13,
                                                fontWeight: 'normal',
                                                lineHeight: 20
                                            }}>New Session</Text>
                                        </TouchableOpacity>
                                    </View>
                                }
                                ListFooterComponent={() =>
                                    this.state.sessionData.length > 0 ? <Text style={{
                                        textAlign: 'center',
                                        fontFamily: 'Opensans',
                                        fontSize: 12,
                                        fontWeight: 'bold',
                                        color: '#9B9B9B'
                                    }}>That’s all, folks!</Text> : null}
                                keyExtractor={item => item.email} />
                            <Modal style={{
                                margin: 0,
                                marginHorizontal: 20,
                                borderRadius: 5
                            }} isVisible={this.state.isModalVisibleSearch}>
                                <View style={{ backgroundColor: 'white', borderRadius: 10 }}>
                                    <View style={{
                                        height: 30,
                                        justifyContent: 'space-between',
                                        flexDirection: 'row',
                                        backgroundColor: '#EDF3F6',
                                        borderRadius: 5
                                    }}>
                                        <View style={{ flexDirection: 'row', padding: 5, }}>
                                            <TouchableOpacity
                                                onPress={() => this.oneditTimeline()}>
                                                <Text style={{
                                                    fontFamily: 'OpenSans',
                                                    fontSize: 12,
                                                    color: '#007BB3',
                                                    fontWeight: 'bold',
                                                    height: 30
                                                }}>
                                                    Edit
                                                </Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this._delsession()}>
                                                <Text style={{
                                                    fontFamily: 'OpenSans',
                                                    fontSize: 12,
                                                    color: 'red',
                                                    marginLeft: 20,
                                                    fontWeight: 'bold'
                                                }}>
                                                    Delete
                                    </Text>
                                            </TouchableOpacity>
                                        </View>
                                        <TouchableOpacity onPress={() => this.setState({ isModalVisibleSearch: false })}>
                                            <Image source={require('../../assets/images/img/icon-close.png')} style={{ height: 10, width: 20, alignSelf: 'center', marginTop: 10, marginRight: 10 }} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ borderRadius: 5 }}>
                                        <View style={{
                                            flexDirection: 'row', alignItems: 'center',
                                        }}>

                                            <Image
                                                style={{
                                                    margin: 10,
                                                    height: 60,
                                                    width: 60,
                                                    borderRadius: 30
                                                }}
                                                resizeMode='cover'
                                                source={{
                                                    uri: this.state.userImg,
                                                }}
                                                onLoadStart={(e) => this.setState({ loader: true })}

                                            />
                                            <View>
                                                <View style={{ flexDirection: 'row', marginTop: 15, justifyContent: 'center' }}>
                                                    <Text style={{
                                                        fontSize: 20,
                                                        textAlign: 'center',
                                                        fontWeight: 'normal',
                                                        fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                                        color: '#121824',
                                                    }}>{this.state.strainName}</Text>
                                                    <View style={{
                                                        backgroundColor: '#BFDEEC',
                                                        borderRadius: 4,
                                                        alignItems: 'center',
                                                        height: 20,
                                                        width: 18,
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                        alignSelf: 'center', marginLeft: 4
                                                    }}>
                                                        <Text style={{
                                                            fontSize: 10,
                                                            fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                                            textAlign: 'center',
                                                            fontWeight: 'bold',
                                                            lineHeight: 16,
                                                        }}>{this.state.short_symtopms}</Text>
                                                    </View>

                                                </View>
                                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                    <Text style={{ fontSize: 12, textAlign: 'center', fontFamily: 'OpenSans', color: '#121824', fontWeight: 'bold', lineHeight: 16 }}>{this.state.method}</Text>
                                                    <Text style={{ fontSize: 12, textAlign: 'center', fontFamily: 'OpenSans', marginHorizontal: 10, color: '#121824', fontWeight: 'bold', lineHeight: 16 }}>-</Text>

                                                    <Text style={{ fontSize: 12, textAlign: 'center', fontFamily: 'OpenSans', color: '#121824', fontWeight: 'bold', lineHeight: 16 }}>{this.state.specific_type}</Text>
                                                </View>
                                            </View>

                                        </View>
                                        <View style={{ backgroundColor: '#EDF1F4', borderBottomEndRadius: 5, borderBottomStartRadius: 5 }}>

                                            <View style={{ justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', marginLeft: 13, marginRight: 9, borderBottomWidth: 2, borderBottomColor: '#D8D8D8' }} >
                                                <Text style={{ fontFamily: 'OpenSans', fontSize: 12, color: '#121824', fontWeight: 'bold', lineHeight: 16 }}>
                                                    {this.state.fetchDate}
                                                </Text>
                                                <View>

                                                    <StarRating
                                                        activeOpacity={1}
                                                        disabled={true}
                                                        starSize={16}
                                                        maxStars={5}
                                                        starStyle={{
                                                            marginVertical: 5,
                                                            marginRight: 5,
                                                            color: '#2E7FAE'
                                                        }}
                                                        rating={this.state.treatment_rating}
                                                    />
                                                </View>
                                            </View>

                                            <View style={{ flexDirection: 'row', marginHorizontal: 15, marginTop: 10 }}>
                                                <View style={{ flex: 1 }}>
                                                    <Text style={{ fontFamily: 'OpenSans', fontSize: 12, fontWeight: 'bold' }}>Symptoms</Text>
                                                    {this.state.session_symptoms.map((data) => {
                                                        return (
                                                            <View>
                                                                <Text style={{
                                                                    fontFamily: 'OpenSans',
                                                                    fontSize: 12, marginTop: 3
                                                                }}>{data.symptom.name}</Text>
                                                            </View>
                                                        )
                                                    })
                                                    }
                                                </View>
                                                <View style={{ flex: 1 }}>
                                                    <Text style={{ fontFamily: 'OpenSans', fontSize: 12, fontWeight: 'bold' }}>Methods</Text>
                                                    <Text style={{ fontFamily: 'OpenSans', fontSize: 12, }}>{this.state.method}</Text>
                                                </View>


                                            </View>
                                            <View style={{ margin: 15 }}>
                                                <Text style={{ fontFamily: 'OpenSans', fontSize: 12, fontWeight: 'bold', }}>Notes</Text>
                                                <Text style={{ fontFamily: 'OpenSans', fontSize: 12, }}>{this.state.specific_Text}</Text>
                                            </View>

                                        </View>
                                    </View>
                                </View>
                            </Modal>

                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </SafeAreaView>

        );
    }
}


function groupBy(array, f) {
    var groups = {};
    array.forEach(function (o) {
        var group = JSON.stringify(f(o));
        groups[group] = groups[group] || [];
        groups[group].push(o);
    });
    return Object.keys(groups).map(function (group) {
        return groups[group];
    })
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    },
    backgroundImg: {
        height: '100%',
        width: '100%',
        backgroundColor: '#2C7DB3'
    },
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderBottomWidth: .5,
        borderColor: '#E4E5E5',
        height: 40,
        marginHorizontal: 20,
        marginVertical: 10,
    },

    ImageStyle: {
        padding: 5,
        marginVertical: 8,
        marginHorizontal: 2,
        height: 15,
        width: 15,
        resizeMode: 'stretch',
        alignItems: 'center'
    },

});

const mapStateToProps = state => {
    return {
        delSession: state.auth.delSession,
        sessionResult: state.auth.sessionResult,
        userDetail: state.auth.userDetail,
        methodList: state.auth.methodList,

        status: state.auth.status,
        err_Message: state.auth.err_Message,
        loader: state.auth.loader
    }
}

const mapDispatchToProps = dispatch => {
    return {
        action: bindActionCreators({
            getTimelineList,
            delTimline,
            getSymptoms,
            getMethods,
            getSpecificMethods,
            getSpecificType
        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Experince_Timeline);
