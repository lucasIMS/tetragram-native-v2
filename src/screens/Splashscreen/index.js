import React, { Component } from 'react';
import { StyleSheet, Image, View, Text, SafeAreaView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { styles } from './styles';
import { getToken,getUserData } from '../../services/utils';

// Redux lib
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";

class Splash extends Component {
    componentDidMount() {
        setTimeout(() => {
            // Actions.loginscreen();
            this.getSession();
        }, 100);
    }

    getSession = async ()  => {
        getUserData().then(response => {
            console.log("getToken()==>", response)
            if(response)
            {
                let data = JSON.parse(response)

            }
            else Actions.welcome();
        })
        
    }

   


    render() {
        return (
                <View style={styles.img_container}>

                    <Image source={require('../../assets/images/img/splash_screen.png')} style={styles.splash} />
                </View>
        );
    }
}

const mapStateToProps = state => {
    return {
  
    }
}
const mapDispatchToProps = dispatch => {
    return {
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Splash);