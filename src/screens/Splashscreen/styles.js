import {Dimensions} from 'react-native';

export const styles = {
	img_container:{
		flex:1,
		justifyContent:'center',
		alignItems:'center',
	},
	splash:{
		height: Dimensions.get('window').height,
		width: Dimensions.get('window').width
	},
	middleText:{
		fontSize:18
	},
	logo:{
		height:70,
		width:200,
		position:'absolute',
		top:'25%'
	}
};

export default styles;