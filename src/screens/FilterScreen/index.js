import React, { Component } from 'react';
import {
    Image,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    SafeAreaView,
    Platform,
    ScrollView,
    StyleSheet,
    ImageBackground,
    Dimensions,
    FlatList
} from 'react-native';
import { Actions } from 'react-native-router-flux';
// import  styles  from './styles';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";


// components
import Button from '../../common/button';
import BottomPopup from '../../common/bottomPopup';
import Loader from '../../common/loader';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

// redux Action 
function wp(percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const slideHeight = viewportHeight * 0.36;
const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);

const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;


class FilterScreen extends Component {
    state = { // initilize state
        email: '',
        ErrMsg: '',
        password: '',
        deviceToken: '',
        loader: false,
        isSecure: true,
        types1: [{label: 'Anxiety', value: 0}, {label: 'Insomnia', value: 1}],
        types2: [{label: 'Depression', value: 0}, {label: 'Agitation', value: 1}],
        types3: [{label: 'Headache', value: 0}, {label: 'Joint', value: 1}],
        types4: [{label: 'Muscle', value: 0}, {label: 'Nerve', value: 1}],
        types5: [{label: 'Injury', value: 0}, {label: 'Cramping', value: 1}],
        types6: [{label: 'Seizure', value: 0}, {label: 'Muscle Spasms', value: 1}],
        types7: [{label: 'Tremors', value: 0}, {label: 'Vertigo', value: 1}],
        types8: [{label: 'Psychological', value: 0}, {label: 'pain', value: 1},{label: 'Physical', value: 3}],
        users:
            [
                {

                    "name":
                        "Proxima Midnight",
                },

                {

                    "name":
                        "Ebony Maw",

                },
                {

                    "name":
                        "Proxima Midnight",
                },

                {

                    "name":
                        "Ebony Maw",

                },


            ]
    }

    componentDidMount() {

    }
    onCheckInvision() {
        setTimeout(() => {
            this.setState({ isSecure: !this.state.isSecure })
        }, 200);
    }
    renderMactItem() {
        return (
            <View style={{ borderWidth: 0.5, borderRadius: 16, marginHorizontal: 25, marginTop: 20 }}>
                <View style={{ flexDirection: 'row', marginHorizontal: 15, marginVertical: 10 }}>
                    <Text style={{ 
                        fontSize: 24, 
                        fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                         color: 'black', }}>
                        Granddaddy Purple
                    </Text>
                    <View style={{ marginLeft: 10, backgroundColor: '#BFDEEC', justifyContent: 'center', alignItems: 'center', height: 30, width: 25, borderRadius: 6 }}>
                        <Text style={{ color: 'black' }}> I
                        </Text>
                    </View>
                </View>
                <Image style={{ height: 80, width: viewportWidth - 50 }} resizeMode='cover' source={require('../../assets/images/img/splash_screen.png')} />
                <View style={{ flexDirection: 'row', marginBottom: 30, }}>
                    <View style={{ flex: 1 }}>
                        <Text style={{ fontSize: 18, fontWeight: '800', color: 'black', fontFamily: 'OpenSans', marginLeft: 15, marginTop: 15 }}> Rating</Text>
                        <View style={{ flexDirection: 'row', marginHorizontal: 10 }}>

                            <Image source={require('../../assets/images/img/icon_star_hollow.png')} style={styles.ImageStyle} />
                            <Image source={require('../../assets/images/img/icon_star_hollow.png')} style={styles.ImageStyle} />
                            <Image source={require('../../assets/images/img/icon_star_hollow.png')} style={styles.ImageStyle} />

                            <Image source={require('../../assets/images/img/icon_star_hollow.png')} style={styles.ImageStyle} />
                            <Image source={require('../../assets/images/img/icon_star_hollow.png')} style={styles.ImageStyle} />

                        </View>
                    </View>

                    <View style={{ flex: 1, marginLeft: 15 }}>
                        <Text style={{ fontSize: 18, fontWeight: '800', color: 'black', fontFamily: 'OpenSans', marginLeft: 15, marginTop: 15 }}> Popular for:</Text>
                        <Text style={{ fontSize: 14, fontWeight: '500', color: 'black', fontFamily: 'OpenSans', marginLeft: 15 }}> Insomina</Text>
                        <Text style={{ fontSize: 14, fontWeight: '500', color: 'black', fontFamily: 'OpenSans', marginLeft: 15 }}> Anexity</Text>

                    </View>
                </View>
            </View>

        )
    }

    render() {
        return (
            <ImageBackground style={{ height: '100%', width: '100%' }} resizeMode='cover' source={require('../../assets/images/img/background.png')}>
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ flex: 1 }}>
                        <View style={{
                            flex: Platform.OS == 'ios' ? 0.07 : 0.1,
                            width: '100%',
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            opacity: 0.6
                        }}>
                            <TouchableOpacity
                                onPress={() => Actions.pop()}>

                                <Image resizeMode='contain' style={{ height: 20, width: 140 }} source={require('../../assets/images/img/logo_header.png')} />
                            </TouchableOpacity>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: 'white' }}>
                                    <Image resizeMode='contain' style={{ height: 30, width: 20, }} source={require('../../assets/images/img/icon_speech_bubble.png')} />
                                    <Text style={{ color: 'white', fontSize: 18, fontFamily: 'OpenSans' }}>
                                        Reviews
                         </Text>
                                </TouchableOpacity>


                                <TouchableOpacity onPress={() => Actions.ProfileScreen()} style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 10, }}>
                                    <Image resizeMode='contain' style={{ height: 30, width: 20 }} source={require('../../assets/images/img/icon_profile.png')} />
                                    <Text style={{ color: 'white', fontSize: 18, fontFamily: 'OpenSans' }}>
                                        Profile
                        </Text>
                                </TouchableOpacity>


                            </View>
                        </View>
                        <View style={{ flex: 1, backgroundColor: 'white' }}>
                            <ScrollView style={{ flex: 1 }}>
                                <Text style={{
                                     fontSize: 26, 
                                     color: '#6559A0',
                                     fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                 marginLeft: 25, 
                                 marginTop: 30 
                                 }}>Filter and sort strains</Text>
                                <Text style={{
                                     fontSize: 18, 
                                color: 'black',
                                 marginLeft: 25, 
                                marginTop: 30, 
                                fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                fontWeight: '800'
                                 }}>Sort By</Text>
                                <RadioForm
                                    style={{ marginTop: 20, flex: 1, marginLeft: 20 }}
                                    ref="radioForm"
                                    radio_props={this.state.types8}
                                    initial={0}
                                    formHorizontal={true}
                                    labelHorizontal={true}
                                    buttonColor={'#2196f3'}
                                    labelColor={'#000'}
                                    animation={true}
                                    buttonSize={15}
                                    buttonOuterSize={25}
                                    buttonWrapStyle={{ marginTop: 40,marginLeft:5 }}
                                    onPress={(value, index) => {
                                        this.setState({
                                            value1: value,
                                            value1Index: index
                                        })
                                    }}
                                />
                                <View style={{ flexDirection: 'row' }}>
                                    <RadioForm
                                        style={{ flex: 1, marginLeft: 20 }}
                                        ref="radioForm"
                                        radio_props={this.state.types4}
                                        initial={0}
                                        formHorizontal={false}
                                        labelHorizontal={true}
                                        buttonColor={'#2196f3'}
                                        labelColor={'#000'}
                                        animation={true}
                                        buttonSize={15}
                                        buttonOuterSize={25}
                                        buttonWrapStyle={{ marginTop: 40 }}
                                        onPress={(value, index) => {
                                            this.setState({
                                                value1: value,
                                                value1Index: index
                                            })
                                        }}
                                    />
                                    <RadioForm
                                        style={{ flex: 1 }}
                                        ref="radioForm"
                                        radio_props={this.state.types5}
                                        initial={0}
                                        formHorizontal={false}
                                        labelHorizontal={true}
                                        buttonColor={'#2196f3'}
                                        labelColor={'#000'}
                                        animation={true}
                                        buttonSize={15}
                                        buttonOuterSize={25}
                                        buttonWrapStyle={{ marginTop: 40 }}
                                        onPress={(value, index) => {
                                            this.setState({
                                                value1: value,
                                                value1Index: index
                                            })
                                        }}
                                    />
                                </View>

                                <Text style={{ fontSize: 18, 
                                    color: 'black',
                                     marginLeft: 25,
                                      marginTop: 30, 
                                      fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                       }}>Physical</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <RadioForm
                                        style={{ marginTop: 20, flex: 1, marginLeft: 20 }}
                                        ref="radioForm"
                                        radio_props={this.state.types6}
                                        initial={0}
                                        formHorizontal={false}
                                        labelHorizontal={true}
                                        buttonColor={'#2196f3'}
                                        labelColor={'#000'}
                                        animation={true}
                                        buttonSize={15}
                                        buttonOuterSize={25}
                                        buttonWrapStyle={{ marginTop: 40 }}
                                        onPress={(value, index) => {
                                            this.setState({
                                                value1: value,
                                                value1Index: index
                                            })
                                        }}
                                    />
                                    <RadioForm
                                        style={{ marginTop: 20, flex: 1, marginLeft: 20 }}
                                        ref="radioForm"
                                        radio_props={this.state.types7}
                                        initial={0}
                                        formHorizontal={false}
                                        labelHorizontal={true}
                                        buttonColor={'#2196f3'}
                                        labelColor={'#000'}
                                        animation={true}
                                        buttonSize={15}
                                        buttonOuterSize={25}
                                        buttonWrapStyle={{ marginTop: 40 }}
                                        onPress={(value, index) => {
                                            this.setState({
                                                value1: value,
                                                value1Index: index
                                            })
                                        }}
                                    />
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <TextInput placeholder='Enter a symptom' style={{ flex: 1, borderWidth: 1, borderRadius: 10, height: 40, marginTop: 10, marginHorizontal: 20, paddingLeft: 10, paddingRight: 40 }}>

                                    </TextInput>
                                </View>
                              






                                <TouchableOpacity onPress={() => Actions.Experince_Timeline()} style={{ marginLeft: 25, borderWidth: 1, borderRadius: 12, height: 40, marginTop: 15, justifyContent: 'center', alignItems: 'center', backgroundColor: '#D0D1D3', width: 160 }}>
                                    <Text style={{ color: 'white', fontWeight: '600', fontSize: 12 }}>Pick custom Symptoms</Text>
                                </TouchableOpacity>



                                <TouchableOpacity onPress={() => Actions.LoginSessionSearch()} style={{ borderWidth: 1, borderRadius: 16, height: 40, marginTop: 40, marginHorizontal: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: '#3089CA', marginBottom: 20 }}>
                                    <Text style={{ color: 'white', fontWeight: '600', fontSize: 16 }}>Save Symptions</Text>
                                </TouchableOpacity>







                            </ScrollView>
                        </View>
                    </View>

                    <BottomPopup message={this.state.ErrMsg} />
                    <Loader visible={this.state.loader} />
                </SafeAreaView>
            </ImageBackground>

        );
    }


}



const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    },

    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderBottomWidth: .5,
        borderColor: '#000',
        height: 40,
        marginHorizontal: 20,
        marginVertical: 10,
    },

    ImageStyle: {
        padding: 10,
        margin: 5,
        height: 25,
        width: 25,
        resizeMode: 'stretch',
        alignItems: 'center'
    },

});

export default FilterScreen;