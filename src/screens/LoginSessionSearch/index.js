import React, { Component } from 'react';
import {
    Image,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    SafeAreaView,
    Platform,
    ScrollView,
    StyleSheet,
    ImageBackground,
    Dimensions
} from 'react-native';
import { Actions } from 'react-native-router-flux';
// import  styles  from './styles';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";


// components
import Button from '../../common/button';
import BottomPopup from '../../common/bottomPopup';
import Loader from '../../common/loader';

// redux Action 
function wp(percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const slideHeight = viewportHeight * 0.36;
const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);

const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;


class LoginSessionSearch extends Component {
    state = { // initilize state
        email: '',
        ErrMsg: '',
        password: '',
        deviceToken: '',
        loader: false,
    }

    componentDidMount() {

    }


    render() {
        return (
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ flex: 1 }}>
                        <View style={{
                            opacity:0.5,
                            flex: Platform.OS == 'ios' ? 0.07 : 0.1,
                            width: '100%',
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center'
                        }}>
                            <TouchableOpacity style = {{flexDirection:'row',justifyContent:'center'}}
                                onPress={() => Actions.pop()}>

                                <Image resizeMode='contain' style={{ height: 20, width: 20,marginLeft:25 }} source={require('../../assets/images/img/icon_arrow_left.png')} />
                                <Text style = {{color:'#2A7BB4',textAlign:'auto',marginLeft:5,fontFamily:'OpenSans'}}>Timeline</Text>
                            </TouchableOpacity>
                          
                            <Text style = {{color:'#2A7BB4',position:'absolute',right:0,marginRight:25,fontFamily:'OpenSans'}}>february 28,2019</Text>


                        </View>
                       
                    </View>

                    <BottomPopup message={this.state.ErrMsg} />
                    <Loader visible={this.state.loader} />
                </SafeAreaView>

        );
    }


}



const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    },

    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderBottomWidth: .5,
        borderColor: '#000',
        height: 40,
        marginHorizontal: 20,
        marginVertical: 10,
    },

    ImageStyle: {
        padding: 10,
        margin: 5,
        height: 25,
        width: 25,
        resizeMode: 'stretch',
        alignItems: 'center'
    },

});

export default LoginSessionSearch;