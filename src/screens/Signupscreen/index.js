import React, { Component } from 'react';
import { Image, View, Text, TextInput, TouchableOpacity, Platform, SafeAreaView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { styles } from './styles';
import { connect } from 'react-redux'


// component
import BottomPopup from '../../common/bottomPopup';
import Button from '../../common/button';
import Loader from '../../common/loader';

// services


class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isChecked: false,
            name: '',
            email: '',
            password: '',
            confirmPass: '',
            message: '',
            loader: false,
            deviceToken:''
        }
    }

    
   

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
                        <View>
<Text>Signup</Text>
                        </View>
                       
                       
                    <Loader visible={this.state.loader} />
                    <BottomPopup message={this.state.message} />
            </SafeAreaView>
        );
    }
}

const mapStateToProps = state => {
    return {
     
    }
}

const mapDispatchToProps = dispatch => {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);