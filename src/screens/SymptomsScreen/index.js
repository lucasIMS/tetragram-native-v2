import React, { Component } from 'react';
import {
  Image,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  SafeAreaView,
  Platform,
  ScrollView,
  StyleSheet,
  ImageBackground,
  Dimensions
} from 'react-native';
import { Actions } from 'react-native-router-flux';
// import  styles  from './styles';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';


// components
import Button from '../../common/button';
import BottomPopup from '../../common/bottomPopup';
import Loader from '../../common/loader';

// redux Action 
function wp(percentage) {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
}
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const slideHeight = viewportHeight * 0.36;
const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);

const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;


class SymptomsScreen extends Component {
  state = { // initilize state
    email: '',
    ErrMsg: '',
    password: '',
    deviceToken: '',
    loader: false,
    types1: [{ label: 'Anxiety', value: 0 }, { label: 'Insomnia', value: 1 }],
    types2: [{ label: 'Depression', value: 0 }, { label: 'Agitation', value: 1 }],
    types3: [{ label: 'Headache', value: 0 }, { label: 'Joint', value: 1 }],
    types4: [{ label: 'Muscle', value: 0 }, { label: 'Nerve', value: 1 }],
    types5: [{ label: 'Injury', value: 0 }, { label: 'Cramping', value: 1 }],
    types6: [{ label: 'Seizure', value: 0 }, { label: 'Muscle Spasms', value: 1 }],
    types7: [{ label: 'Tremors', value: 0 }, { label: 'Vertigo', value: 1 }],
    types8: [{ label: 'Psychological', value: 0 }, { label: 'pain', value: 1 }, { label: 'Physical', value: 3 }],


  }


  componentDidMount() {

  }


  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          <View style={{
            flex: Platform.OS == 'ios' ? 0.07 : 0.1,
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center'
          }}>
            <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'center' }}
              onPress={() => Actions.pop()}>

              <Image resizeMode='contain' style={{ height: 20, width: 20, marginLeft: 25 }} source={require('../../assets/images/img/icon_arrow_left.png')} />
              <Text style={{ color: '#2A7BB4', textAlign: 'auto', marginLeft: 5 }}>Timeline</Text>
            </TouchableOpacity>

            <Text style={{ color: '#2A7BB4', position: 'absolute', right: 0, marginRight: 25 }}>february 28,2019</Text>


          </View>
          <View style={{ flex: 1, backgroundColor: 'white' }}>
            <ScrollView style={{ flex: 1 }}>
              <Text style={{ fontSize: 26, 
                color: '#6559A0', 
                fontFamily: Platform.OS == "ios" ? 'Merriweather':'Merriweather-Light', 
                 marginLeft: 25, 
                marginTop: 30 
                }}>Select Symptoms</Text>
              <Text style={{
                 fontSize: 18, 
                 color: 'black',
               marginLeft: 25,
                marginTop: 30, 
                fontFamily: Platform.OS == "ios" ? 'Merriweather':'Merriweather-Light'  
               }}>Psychological</Text>
              <View style={{ flexDirection: 'row' }}>
                <RadioForm
                  style={{ marginTop: 20, flex: 1, marginLeft: 20 }}
                  ref="radioForm"
                  radio_props={this.state.types1}
                  initial={0}
                  formHorizontal={false}
                  labelHorizontal={true}
                  buttonColor={'#2196f3'}
                  labelColor={'#000'}
                  animation={true}
                  buttonSize={15}
                  buttonOuterSize={25}
                  buttonWrapStyle={{ marginTop: 40 }}
                  onPress={(value, index) => {
                    this.setState({
                      value1: value,
                      value1Index: index
                    })
                  }}
                />
                <RadioForm
                  style={{ marginTop: 20, flex: 1 }}
                  ref="radioForm"
                  radio_props={this.state.types2}
                  initial={0}
                  formHorizontal={false}
                  labelHorizontal={true}
                  buttonColor={'#2196f3'}
                  labelColor={'#000'}
                  animation={true}
                  buttonSize={15}
                  buttonOuterSize={25}
                  buttonWrapStyle={{ marginTop: 40 }}
                  onPress={(value, index) => {
                    this.setState({
                      value1: value,
                      value1Index: index
                    })
                  }}
                />
              </View>

              <Text style={{ fontSize: 18, 
                color: 'black',
                 marginLeft: 25,
                 marginTop: 30, 
                 fontFamily: Platform.OS == "ios" ? 'Merriweather':'Merriweather-Light'  
                  }}>Pain</Text>
              <View style={{ flexDirection: 'row' }}>
                <RadioForm
                  style={{ marginTop: 20, flex: 1, marginLeft: 20 }}
                  ref="radioForm"
                  radio_props={this.state.types1}
                  initial={0}
                  formHorizontal={false}
                  labelHorizontal={true}
                  buttonColor={'#2196f3'}
                  labelColor={'#000'}
                  animation={true}
                  buttonSize={15}
                  buttonOuterSize={25}
                  buttonWrapStyle={{ marginTop: 40 }}
                  onPress={(value, index) => {
                    this.setState({
                      value1: value,
                      value1Index: index
                    })
                  }}
                />
                <RadioForm
                  style={{ marginTop: 20, flex: 1 }}
                  ref="radioForm"
                  radio_props={this.state.types3}
                  initial={0}
                  formHorizontal={false}
                  labelHorizontal={true}
                  buttonColor={'#2196f3'}
                  labelColor={'#000'}
                  animation={true}
                  buttonSize={15}
                  buttonOuterSize={25}
                  buttonWrapStyle={{ marginTop: 40 }}
                  onPress={(value, index) => {
                    this.setState({
                      value1: value,
                      value1Index: index
                    })
                  }}
                />
              </View>
              <View style={{ flexDirection: 'row' }}>
                <RadioForm
                  style={{ flex: 1, marginLeft: 20 }}
                  ref="radioForm"
                  radio_props={this.state.types4}
                  initial={0}
                  formHorizontal={false}
                  labelHorizontal={true}
                  buttonColor={'#2196f3'}
                  labelColor={'#000'}
                  animation={true}
                  buttonSize={15}
                  buttonOuterSize={25}
                  buttonWrapStyle={{ marginTop: 40 }}
                  onPress={(value, index) => {
                    this.setState({
                      value1: value,
                      value1Index: index
                    })
                  }}
                />
                <RadioForm
                  style={{ flex: 1 }}
                  ref="radioForm"
                  radio_props={this.state.types5}
                  initial={0}
                  formHorizontal={false}
                  labelHorizontal={true}
                  buttonColor={'#2196f3'}
                  labelColor={'#000'}
                  animation={true}
                  buttonSize={15}
                  buttonOuterSize={25}
                  buttonWrapStyle={{ marginTop: 40 }}
                  onPress={(value, index) => {
                    this.setState({
                      value1: value,
                      value1Index: index
                    })
                  }}
                />
              </View>

              <Text style={{ 
                fontSize: 18,
               color: 'black',
                marginLeft: 25, 
               marginTop: 30, 
               fontFamily: Platform.OS == "ios" ? 'Merriweather':'Merriweather-Light'      
                 }}>Physical</Text>
              <View style={{ flexDirection: 'row' }}>
                <RadioForm
                  style={{ marginTop: 20, flex: 1, marginLeft: 20 }}
                  ref="radioForm"
                  radio_props={this.state.types6}
                  initial={0}
                  formHorizontal={false}
                  labelHorizontal={true}
                  buttonColor={'#2196f3'}
                  labelColor={'#000'}
                  animation={true}
                  buttonSize={15}
                  buttonOuterSize={25}
                  buttonWrapStyle={{ marginTop: 40 }}
                  onPress={(value, index) => {
                    this.setState({
                      value1: value,
                      value1Index: index
                    })
                  }}
                />
                <RadioForm
                  style={{ marginTop: 20, flex: 1, marginLeft: 20 }}
                  ref="radioForm"
                  radio_props={this.state.types7}
                  initial={0}
                  formHorizontal={false}
                  labelHorizontal={true}
                  buttonColor={'#2196f3'}
                  labelColor={'#000'}
                  animation={true}
                  buttonSize={15}
                  buttonOuterSize={25}
                  buttonWrapStyle={{ marginTop: 40 }}
                  onPress={(value, index) => {
                    this.setState({
                      value1: value,
                      value1Index: index
                    })
                  }}
                />
              </View>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <TextInput placeholder='Enter a symptom' style={{ flex: 1, borderWidth: 1, borderRadius: 10, height: 40, marginTop: 10, marginHorizontal: 20, paddingLeft: 10, paddingRight: 40 }}>

                </TextInput>
              </View>
              <RadioForm
                style={{ marginTop: 20, flex: 1, marginLeft: 20 }}
                ref="radioForm"
                radio_props={this.state.types8}
                initial={0}
                formHorizontal={true}
                labelHorizontal={true}
                buttonColor={'#2196f3'}
                labelColor={'#000'}
                animation={true}
                buttonSize={15}
                buttonOuterSize={25}
                labelStyle={{marginLeft:5,marginRight:15}}
                buttonWrapStyle={{ marginTop: 40 }}
                onPress={(value, index) => {
                  this.setState({
                    value1: value,
                    value1Index: index
                  })
                }}
              />






              <TouchableOpacity onPress={() => Actions.Experince_Timeline()} style={{ marginLeft: 25, borderWidth: 1, borderRadius: 12, height: 40, marginTop: 15, justifyContent: 'center', alignItems: 'center', backgroundColor: '#D0D1D3', width: 160, borderColor: 'transparent' }}>
                <Text style={{ color: 'white', fontWeight: '600', fontSize: 12, fontFamily: 'OpenSans' }}>Pick custom Symptoms</Text>
              </TouchableOpacity>



              <TouchableOpacity onPress={() => Actions.LoginSessionSearch()} style={{ borderWidth: 1, borderRadius: 16, height: 40, marginTop: 40, marginHorizontal: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: '#3089CA', marginBottom: 20, borderColor: 'transparent' }}>
                <Text style={{ color: 'white', fontWeight: '600', fontSize: 16, fontFamily: 'OpenSans' }}>Save Symptions</Text>
              </TouchableOpacity>







            </ScrollView>
          </View>
        </View>

        <BottomPopup message={this.state.ErrMsg} />
        <Loader visible={this.state.loader} />
      </SafeAreaView>

    );
  }


}



const styles = StyleSheet.create({

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10
  },

  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: .5,
    borderColor: '#000',
    height: 40,
    marginHorizontal: 20,
    marginVertical: 10,
  },

  ImageStyle: {
    padding: 10,
    margin: 5,
    height: 25,
    width: 25,
    resizeMode: 'stretch',
    alignItems: 'center'
  },

});

export default SymptomsScreen;