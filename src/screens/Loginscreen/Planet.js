import React, { Component } from 'react';
import {
  Animated,
  Dimensions,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Platform,
  Image
} from 'react-native';


function wp(percentage) {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
}
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
const { width, height } = Dimensions.get('window');
const slideHeight = viewportHeight * 0.36;
const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);

const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;

export class Planet extends Component {
  static WIDTH = width;

  render = () => {
    const { animatedValue, planet, index } = this.props;

    return (
      <Animated.View style={styles.container}>
        {index == 1 ?
          <View style={{ overflow: 'visible', flex: 1, marginVertical: 100, backgroundColor: 'white', width: viewportWidth - 40, borderRadius: 10, alignSelf: 'center', }}>
            <Image style={{ width: 200, alignSelf: 'center', height: 100, marginTop: 30 }} resizeMode='contain' source={require('../../assets/images/img/rate_graphic.png')} />
            <Text style={{
              fontSize: 48,
              fontWeight: '600',
              marginLeft: 10,
              color: '#6559A0',
              marginTop: 15,
              fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
            }}> Rate</Text>
            <Text style={{ marginHorizontal: 20, fontSize: 16, marginTop: 15, lineHeight: 30, opacity: 0.9 }}>
              Creating a diary your cannabis experiences is the faster way to know what works for you, so you can focus on what matters.
                  </Text>
            <View style={{ position: 'absolute', bottom: 0, height: 60, borderWidth: 1, flexDirection: 'row', width: '100%', borderColor: '#AAB4AF' }}>

              <TouchableOpacity onPress={() => this.onTrackAction()} style={{ backgroundColor: 'white', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: 'black', fontSize: 16 }}>Track</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.onRateAction()} style={{ backgroundColor: '#2C7DB3', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: 'white', fontSize: 16 }}> Rate</Text>
              </TouchableOpacity>

            </View>

          </View>
          :
          <View style={{ overflow: 'visible', flex: 1, marginVertical: 100, backgroundColor: 'white', width: viewportWidth - 40, borderRadius: 10, alignSelf: 'center', }}>
            <Image style={{ width: 200, alignSelf: 'center', height: 100, marginTop: 30 }} resizeMode='contain' source={require('../../assets/images/img/rate_graphic.png')} />
            <Text style={{
              fontSize: 48,
              fontWeight: '600',
              marginLeft: 10,
              color: '#6559A0', 
              marginTop: 15,
              fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
            }}> Rate</Text>
            <Text style={{ marginHorizontal: 20, fontSize: 16, marginTop: 15, lineHeight: 30, opacity: 0.9 }}>
              Creating a diary your cannabis experiences is the faster way to know what works for you, so you can focus on what matters.
                </Text>
            <View style={{ position: 'absolute', bottom: 0, height: 60, borderWidth: 1, flexDirection: 'row', width: '100%', borderColor: '#AAB4AF' }}>
              {/* <Text style = {{textAlign:'center',fontSize:16,marginTop:-20,textAlign:'center'}}>1/2</Text> */}

              <TouchableOpacity onPress={() => this.onTrackAction()} style={{ backgroundColor: 'white', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: 'black', fontSize: 16 }}>Track</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.onRateAction()} style={{ backgroundColor: '#2C7DB3', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: 'white', fontSize: 16 }}> Rate</Text>
              </TouchableOpacity>

            </View>

          </View>
        }

      </Animated.View>
    );
  };
}

export const BottomBar = ({ destination }) => (
  <View style={styles.bottomBar}>
    <View style={styles.ports}>
    </View>

  </View>
);

const styles = StyleSheet.create({
  container: {
    width: width,
    height: height,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'visible',
  },
  planet: Platform.select({
    ios: {
      width: width - 25,
      height: width - 25,
    },
    android: {
      width: width - 50,
      height: width - 50,
    },
  }),
  title: Platform.select({
    ios: {
      fontFamily: 'dhurjati',
      fontSize: 32,
      position: 'absolute',
      bottom: 0,
      textAlign: 'center',
      fontWeight: 'bold',
      letterSpacing: 1.2,
      color: 'white',
      backgroundColor: 'transparent',
    },
    android: {
      fontFamily: 'inconsolata-regular',
      fontSize: 24,
      position: 'absolute',
      bottom: 20,
      textAlign: 'center',
      fontWeight: 'bold',
      letterSpacing: 1.2,
      color: 'white',
      backgroundColor: 'transparent',
    }
  }),
  tagLine: {
    color: 'white',
    fontFamily: 'inconsolata-regular',
    fontSize: 15,
  },
  tagLineContainer: {
    paddingBottom: 4,
    borderBottomWidth: 2,
    borderBottomColor: '#6D214F',
  },
  ports: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 16,
    height: 150,
    width: '90%',
  },
  port: {
    borderRadius: 4,
    backgroundColor: '#2C3A47',
    width: '48%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  portTitle: {
    color: 'white',
    fontSize: 48,
    fontFamily: 'inconsolata-bold',
  },
  bottomBar: {
    marginTop: 'auto',
    marginBottom: 16,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  bookVoyageButton: {
    backgroundColor: '#6D214F',
    height: 60,
    width: '90%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
  },
  bookVoyageTitle: {
    color: 'white',
    letterSpacing: 1.3,
    fontSize: 24,
    fontFamily: 'inconsolata-regular',
  },
});
