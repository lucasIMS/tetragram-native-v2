import React, { Component } from 'react';
import {
    Image,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    SafeAreaView,
    Keyboard,
    ScrollView,
    Dimensions,
    ImageBackground,
    Platform,
    TouchableWithoutFeedback
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { styles } from './styles';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";


// components
import BottomPopup from '../../common/bottomPopup';
import Loader from '../../common/loader';
import Carousel from 'react-native-snap-carousel';

// redux Action 
import { SignUp, ClearAction } from "../../actions/authAction";

function wp(percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
const slideHeight = viewportHeight * 0.36;
const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);
const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;


//Local images 
const imgBackground = require('../../assets/images/img/background.png')
const trackImg = require('../../assets/images/img/track_graphic.png')
const ratingImg = require('../../assets/images/img/rate_graphic.png')
const customImg = require('../../assets/images/img/customize_graphic.png')

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = { // initilize state
            email: '',
            ErrMsg: '',
            password: '',
            deviceToken: '',
            loader: false,
            isSecure: true,
            currentIndex: 0,
            isAction: false,

            ENTRIES1: [
                {
                    title: 'B',
                    type: 'info1'
                },
                {
                    title: 'Earlie',
                    type: 'info2'
                },
                {
                    title: 'Earlier ',
                    type: 'info3'
                },
                {
                    title: 'W',
                    type: 'login'
                },

            ]

        }
    }


    async componentWillReceiveProps(nextProps) {
        debugger
        if (Actions.currentScene == "loginscreen") await this.setState({ loader: nextProps.loader })
        if (nextProps.status === '5001') {
            setTimeout(() => {
                this.setState({ ErrMsg:'', })
            }, 2000);
            this.setState({ ErrMsg: nextProps.err_Message, })
        }
        else if (nextProps.status === '201') {
            setTimeout(() => {
                alert('Signup Successfully, Please check your email for verfication')
            }, 2000);
             Actions.reset('welcome')
            debugger
        }
    
        else if (nextProps.status === '401') {
            debugger
            setTimeout(() => {
                this.setState({ ErrMsg:'', })
            }, 2000);
            this.setState({ ErrMsg: nextProps.err_Message, })  
          }
    }

    //CREATE ACCOUNT ---------------------

    _createAccount = ()=>{
        if(this.state.email == ''){
            alert('email cannot be empty')
        }
        else if(this.state.password ==''){
            alert('password cannot be empty')
        }
        else if(this.state.zipcode == ''){
            alert('Zip code cannot be empty')
        }
        else{
            let postData = {
                "username":this.state.email,
                "password":this.state.password,
                "zipCode":this.state.zipcode,
                "displayname": this.state.email,
                "acknowledgedAge": true
                // "displayname": ''
            }
            this.props.action.SignUp(postData)
            debugger
                // Actions.Experince_Timeline()
        }

    }
    onRateAction() {
        this._carousel.snapToNext()
    }
    onRateAction1() {
        this._carousel.snapToNext()
    }

    onCreateAction() {
        this._carousel.snapToNext()
    }

    onTrackAction() {
        this._carousel.snapToPrev()

    }



    componentDidMount() {
    }

    OnSigninAction() {
        Actions.newLogin()
    }

    onCheckInvision() {
        setTimeout(() => {
            this.setState({ isSecure: !this.state.isSecure })
        }, 200);
    }

    _renderItem({ item, index }) {
        if (index == 0) {
            return (
                <View style={styles.sliderView}>
                    <Image style={styles.trackImg}
                        resizeMode='contain' source={trackImg} />
                    <Text style={styles.trackText}>Track</Text>
                    <Text style={styles.textDescription}>
                        Creating a diary of your cannabis experiences is the fastest way to know what works for you. We’ll help you keep track of what you’ve had and how it helps, so you can focus on what matters. </Text>
                    <View style={styles.stepperView}>
                        <Text style={styles.stepperText}>
                            1/3
                  </Text>
                    </View>
                    <View style={styles.bottomView}>
                        {/* <Text style = {{textAlign:'center',fontSize:16,marginTop:-20,textAlign:'center'}}>1/3</Text> */}

                        <TouchableOpacity onPress={() => this.OnSigninAction()} style={styles.leftButton}>
                            <Text style={styles.buttonText}> Back to sign in</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.onRateAction(index)} style={styles.rightButton}>
                            <Text style={[styles.buttonText, { color: 'white' }]}> Rate</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            )
        }
        else if (index == 1) {
            return (
                <View style={[styles.sliderView, { marginVertical: 90 }]}>
                    <Image style={styles.startImg} resizeMode='contain' source={ratingImg} />
                    <Text style={[styles.trackText]}>Rate</Text>
                    <Text style={styles.textDescription}>
                    Each cannabis strain has its own unique effect. Understand what works best for you, and find strains that work similarly. 
                  </Text>
                    <View style={styles.stepperView}>
                        <Text style={styles.stepperText}>
                            2/3
                  </Text>
                    </View>
                    <View style={styles.bottomView}>
                        <TouchableOpacity onPress={() => this.onTrackAction()} style={styles.leftButton}>
                            <Text style={styles.buttonText}>Track</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.onRateAction()} style={styles.rightButton}>
                            <Text style={[styles.buttonText, { color: 'white' }]}> Customize</Text>
                        </TouchableOpacity>

                    </View>

                </View>
            )
        }
        else if (index == 2) {
            return (
                <View style={[styles.sliderView, { marginVertical: 90 }]}>
                    <Image style={styles.customImg} resizeMode='contain' source={customImg} />
                    <Text style={styles.trackText}>Customize</Text>
                    <Text style={styles.textDescription}>
                    Make the experience uniquely yours with your own photography, custom symptoms, and full text reviews.
                  </Text>
                    <View style={styles.stepperView}>
                        <Text style={styles.stepperText}>
                            3/3
                  </Text>
                    </View>
                    <View style={styles.bottomView}>
                        {/* <Text style = {{textAlign:'center',fontSize:16,marginTop:-20}}>1/2</Text> */}

                        <TouchableOpacity onPress={() => this.onTrackAction()} style={styles.leftButton}>
                            <Text style={styles.buttonText}>Rate</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.onCreateAction()} style={styles.rightButton}>
                            <Text style={[styles.buttonText, { color: 'white' }]}> Create Account</Text>
                        </TouchableOpacity>

                    </View>

                </View>
            )
        }
        else if (index == 3) {
            return (
                <TouchableWithoutFeedback onPress={() => { Keyboard.dismiss() }}>

<View style={[styles.sliderView, { marginTop: 45,borderRadius: 12,paddingVertical:35 }]}>
                        <ScrollView>
                            <Text style={[styles.trackText, {
                                 marginBottom: 15,
                                 marginLeft: 20,
                                 lineHeight:34,
                                 paddingTop:10,
                                 fontWeight:'normal',
                                 fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                 }]}>Create your account</Text>
                            <Text style={{ fontSize: 18, color: 'black', marginHorizontal: 20, fontFamily: 'OpenSans',fontWeight:'normal' }}>Enter Your Email Address</Text>
                                    <TextInput 
                                                
                                                keyboardType = 'email-address'
                                                onChangeText={(email) => this.setState({email})}
                                                value={this.state.email}
                                                placeholder='johnny@appleseed.com' 
                                                style={{ borderWidth: 0.5, borderRadius: 10, height: 40, marginTop: 10, marginHorizontal: 20, paddingHorizontal: 10,fontFamily:'OpenSans',fontSize:12 }}>
                                    </TextInput>
                                    <Text style={{ fontSize: 14, color: 'black', marginHorizontal: 22, fontWeight: '500', marginTop: 5, fontFamily: 'OpenSans', opacity: 0.9,lineHeight:17  }}>This is how you'll login. We don't spam! </Text>

                                    {/* <Text style={{ fontSize: 18, color: 'black', marginHorizontal: 20, fontFamily: 'OpenSans',fontWeight:'normal',marginTop:15 }}>User Name</Text>
                                    <TextInput 
                                                
                                                keyboardType = 'default'
                                                onChangeText={(name) => this.setState({name})}
                                                value={this.state.name}
                                                placeholder='user name' 
                                                style={{ borderWidth: 0.5, borderRadius: 10, height: 40, marginTop: 10, marginHorizontal: 20, paddingHorizontal: 10,fontFamily:'OpenSans',fontSize:12 }}>
                                    </TextInput> */}

                            <Text style={{ fontSize: 18, color: 'black', marginLeft: 20, marginTop: 15, fontFamily: 'OpenSans',fontWeight:'normal' }}>Choose a Password</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center', alignItems: 'center', }}>

                                <TextInput
                                        keyboardType = 'default'
                                        onChangeText={(password) => this.setState({password})}
                                    value={this.state.password}
                                         secureTextEntry={this.state.isSecure} 
                                        style={{ flex: 1, borderWidth:0.5, borderRadius: 10, height: 40, marginTop: 10, marginHorizontal: 20, paddingLeft: 10, paddingRight: 40,fontFamily:'OpenSans',fontSize:12 }}>
                                </TextInput>

                                <TouchableOpacity style={{ alignSelf: 'center', position: 'absolute', right: 20, marginRight: 10, }} onPress={() => this.onCheckInvision()}>
                                    <Image source={this.state.isSecure ? require('../../assets/images/img/icon_eye_black.png') : require('../../assets/images/img/eyesCross.png')} style={{ height: 20, width: 20, marginRight: 5, marginTop: 8 }}></Image>
                                </TouchableOpacity>
                            </View>
                            <Text style={{fontSize: 14, color: 'black', marginHorizontal: 22, fontWeight: '500', marginTop: 5, fontFamily: 'OpenSans', opacity: 0.9  }}>Make it secure, it protects your stuff!</Text>

                            <Text style={{ fontSize: 18, color: 'black', marginLeft: 20, marginTop: 15, fontFamily: 'OpenSans' }}>Zip Code</Text>
                            <TextInput
                             onChangeText={(zipcode) => this.setState({zipcode})}
                            value={this.state.zipcode}
                             keyboardType='number-pad' 
                             style={{ borderWidth: 0.5, borderRadius: 10, height: 40, marginTop: 15, marginHorizontal: 20, paddingHorizontal: 10,fontFamily:'OpenSans',fontSize:12 }}>
                            </TextInput>
                            <Text style={{ fontSize: 14, color: 'black', marginHorizontal: 22, fontWeight: '500', marginTop: 5, fontFamily: 'OpenSans', opacity: 0.9 }}>We collect this to help us find suggest the closest dispensaries to you. </Text>
                            <TouchableOpacity onPress={() => this._createAccount()} 
                            style={{marginTop:21, borderWidth: 1, borderRadius: 10, height: 40, marginHorizontal: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: '#3089CA', borderColor: 'transparent' }}>
                                <Text style={[styles.buttonText, { color: '#fff',fontWeight:'bold',fontSize:16 }]}> Create My Account</Text>
                            </TouchableOpacity>
                        </ScrollView>
                    </View>
                </TouchableWithoutFeedback>
            );
        }
    }

    onPressDeleteCurrentItem = () => {
        if (this._carousel.current) {
            this.setState(prevState => {
                const id = prevState.data[this._carousel.current.currentIndex].id;
                const data = prevState.data.filter(item => item.id !== id);
                return { data }
            })
        }
    }
    _onSnapToItem(index) {
        Keyboard.dismiss()
    }

    render() {
        return (
            <ImageBackground style={styles.backgroundImg} resizeMode='cover' source={imgBackground}>

                <SafeAreaView style={{ flex: 1, }}>
                    <Carousel
                        ref={(c) => { this._carousel = c; }}
                        data={this.state.ENTRIES1}
                        renderItem={this._renderItem.bind(this)}
                        sliderWidth={sliderWidth}
                        itemWidth={itemWidth + 20}
                        hasParallaxImages={true}
                        onSnapToItem={this._onSnapToItem}
                        firstItem={this.props.PassData == 'CreateAccount' ? 3 : 0}
                    />
                       <BottomPopup message={this.state.ErrMsg} />
                    <Loader visible={this.state.loader} />
                </SafeAreaView>
            </ImageBackground>
        );
    }
}


const mapStateToProps = state => {
    return {
        signUpText: state.auth.signUpText,
        status: state.auth.status,
        err_Message: state.auth.err_Message,
        loader: state.auth.loader
    }
}

const mapDispatchToProps = dispatch => {
    return {
        action: bindActionCreators({ClearAction,SignUp }, dispatch)
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);