import { Dimensions,Platform } from 'react-native';
// device dimestion Action 
function wp(percentage) {
	const value = (percentage * viewportWidth) / 100;
	return Math.round(value);
}
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
const slideHeight = viewportHeight * 0.36;
const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);
const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;
export const styles = {
	backgroundImg: {
		height: '100%',
		width: '100%'
	},
	sliderView: {
		overflow: 'visible',
		flex: 1,
		marginVertical: 60,
		backgroundColor: 'white',
		width: viewportWidth - 50,
		borderRadius: 10,
		alignSelf: 'center'
	},
	trackImg: {
		width: 200,
		alignSelf: 'center',
		height: 100,
		marginVertical: 40
	},
	trackText: {
		fontSize: 36,
		color: '#6559A0', 
		marginHorizontal: 25,
		marginLeft: 15,
		fontWeight:'normal',
		fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
	},
	textDescription: {
		marginHorizontal: 20,
		fontSize: 15,
		marginTop: 15,
		lineHeight: 20,
		opacity: 0.8,
		fontFamily: 'OpenSans'
	},
	stepperView: {
		position: 'absolute',
		bottom: 40,
		width: '100%'
	},
	stepperText: {
		marginHorizontal: 20,
		fontSize: 10,
		marginTop: 15,
		lineHeight: 30,
		opacity: 0.6,
		fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
		textAlign: 'center'
	},
	bottomView: {
		position: 'absolute',
		bottom: 0,
		height: 40,
		borderWidth: 1,
		flexDirection: 'row',
		width: '100%',
		borderColor: '#AAB4AF',
		borderBottomLeftRadius: 10,
		borderBottomRightRadius: 10
	},
	buttonText: {
		color: 'black',
		fontSize: 16,
		fontWeight: '500',
		fontFamily: 'OpenSans'

	},
	leftButton: {
		backgroundColor: 'white',
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		borderBottomLeftRadius: 10,
	},
	startImg: {
		width: 200,
		alignSelf: 'center',
		height: 100,
		marginTop: 30
	},
	rightButton: {
		backgroundColor: '#2C7DB3',
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		borderBottomRightRadius: 10,
	},
	customImg: {
		alignSelf: 'center',
		height: 80,
		marginTop: 30,
		marginBottom: 10
	}
};

export default styles;