import React, { Component } from 'react';
import {
    Image,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    SafeAreaView,
    Platform,
    ScrollView,
    StyleSheet,
    ImageBackground,
    Dimensions,
    FlatList,
    RefreshControl,
    TouchableHighlight,
    TouchableWithoutFeedback
} from 'react-native';
import { Actions } from 'react-native-router-flux';
// import  styles  from './styles';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";

import Modal from "react-native-modal";
import RadioButton1 from '../../common/radioButton'
import RadioButton2 from '../../common/radionReview'
import ImageViewer from 'react-native-image-zoom-viewer';


// components
import BottomPopup from '../../common/bottomPopup';
import Loader from '../../common/loader';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from '../../common/actionSheet';
import {
     postSymptoms,
     getSymptoms, 
     searchStrain, 
     searchDispensaries, 
     getMethods,
      getSpecificMethods, 
      getSpecificType,
      saveSession, 
      postImages,
      postReview,
      getGallery,
      ClearAction
    } from "../../actions/authAction";
import DateTimePicker from "react-native-modal-datetime-picker";
import StarRating from 'react-native-star-rating';
import SegmentedControlTab from "react-native-segmented-control-tab";
// redux Action 
function wp(percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
import DraggableFlatList from 'react-native-draggable-flatlist'



class EditLoginSession extends Component {
    constructor(props) {
        super(props);
        this.state = { // initilize state
            isRating: true,
            loader: false,
            v1: 0,
            treatRating: 0,
            value: 0,
            experienceRating: 0,
            physicalRating: 0,
            isModalVisibleSymptomsScreen: false,
            isModalVisibleSearch: false,
            isReviewModal:false,
            isMoreSpecific: true,
            isMoreComppnent: true,
            isPickStrain: true,
            isActionSheetOpen: false,
            imgList: [],
            inhanld: false,
            typicaclly: false,
            Orient: false,
            eating: true,
            numberStatus: 0,
            isDeleted: -1,
            sysPhyscial: [],
            sysPain: [],
            sysPhychology: [],
            customInitalIndex: 0,
            refreshing: false,
            fetchSearchStrain: [],
            fetchdispensaries: [],
            filterStrain: [],
            filterDispancrie: [],
            isSativa: false,
            isIndica: false,
            isHybrid: false,
            showDate: '',
            thc: 0,
            cbd: 0,
            cbc:0,
            cbn:0,
            humulene:0,
            myrcene:0,
            terpinolene:0,
            pinene:0,
            thcv:0,
            thca:0,
            cbd:0,
            terpinolene:0,
            caryophyllene:0,
            selectedIndex: 0,
            isCheckedPhyscial: '',
            isCheckedPhys: '',
            isCheckedPain: '',
            selectedSymtoms: '',
            isPickSymtoms: true,
            pickStrainValue: '',
            terpinolene:0,
            caryophyllene:0,
            isHits:'Hits',
            entries: [
                {
                    'value': '1'
                },
                {
                    'value': '2'
                }, {
                    'value': '3'
                }, {
                    'value': '4'
                }, {
                    'value': '5'
                }, {
                    'value': '6'
                }, {
                    'value': '7'
                }, {
                    'value': '8'
                }, {
                    'value': '9'
                },
                {
                    'value': '10'
                }
            ],
            types8: [{ label: 'Psychological', value: 0 }, { label: 'Pain', value: 1 }, { label: 'Physical', value: 3 }],

 
            isDateTimePickerVisible: false,
            filterMethod: [],
            limonene:0,
            linalool:0,
            cbg:0,
            ocimene:0,
            isImageViewVisible:false,
            ImageViewer :[]

        }
        this.handleScroll = this.handleScroll.bind(this)

    }

    componentDidMount() {
        let uri = this.props.symptomPass['@id']
        let filterUri = uri.substring(1)
        this.props.action.getGallery(filterUri,this.props.userDetail)
        var date = new Date().getDate();
        var month = new Date().getMonth() + 1;
        var year = new Date().getFullYear();

        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var now = new Date();
        var thisMonth = months[now.getMonth()]; // 

        let currentDate = thisMonth + ' ' + date + ', ' + year


        this.setState({ showDate: currentDate, initalDate: currentDate, selectedDate: now })





        let data = this.props.fetchSymptions

            pain = data.filter(function (item) {
                return item.category == 'Pain';
            }).map(function (data) {
                return { name : data.name, category:data.category, id: data['@id'] };
            });

            physchology = data.filter(function (item) {
                return item.category == 'Psychological';
            }).map(function (data) {
                return { name : data.name, category:data.category, id: data['@id'] };
            });

            physical = data.filter(function (item) {
                return item.category == 'Physical';
            }).map(function (data) {
                return { name : data.name, category:data.category, id: data['@id'] };
            });



            let filterMethod = []
            var methodtype = this.props.methodList.filter(function (item) {
                filterMethod.push(item.name)
                return item.name
            });
            // this.props.action.getSpecificMethods(this.props.userDetail)
            


            setTimeout(() => {
                let  selectedSymtoms =this.props.symptomPass.session_symptoms
                let symptions = ''
                let isCheckedPhys= ''
                let isCheckedPain = ''
                let isCheckedPhyscial = ''
                let symptiomsPhysical = ''
                let selected_phys_symptoms= ''
                let selected_pain = ''
                let filterSessionSymptoms = selectedSymtoms.filter(function(item){
                    debugger
                    if(symptions == ''){
                        symptions = item.symptom.name
                    }
                    else{
                        symptions = symptions+ ',' + item.symptom.name
                    }
                    if(item.symptom.category == 'Psychological'){
                        isCheckedPhys=item.symptom.name
                        debugger
                        selected_phys_symptoms = item.symptom['@id']
                    }
                    if(item.symptom.category == 'Pain'){
                       isCheckedPain= item.symptom.name
                       selected_pain = item.symptom['@id']
            
                    }
                    if(item.symptom.category == 'Physical'){
                       isCheckedPhyscial = item.symptom.name
                       symptiomsPhysical = item.symptom['@id']
                    }
                  
                    return item.symptom.name;
                })
                let filterMethod = []
                var methodtype = this.props.methodList.filter(function (item) {
                    filterMethod.push(item.name)
                    return item.name
                });
                // this.props.action.getSpecificMethods(this.props.userDetail)
                this.setState({ method_list: this.props.methodList, 
                    filterMethod: filterMethod
                 })
            
                this.setState({ 
                    loader:false,
                    showDate: currentDate,
                     initalDate: currentDate,
                      selectedDate: now ,
                      treatRating: this.props.symptomPass.treatment_rating,
                      pickStrainValue:this.props.symptomPass.strain.search_name,
                       pickdispensaries:this.props.symptomPass.dispensary.name,
                      countAfterRating:this.props.symptomPass.after_pain,
                      countBeforeRating:this.props.symptomPass.before_pain,
                      selectedSymtoms:symptions,
                      isCheckedPhyscial:isCheckedPhyscial,
                      isCheckedPain: isCheckedPain,
                      isCheckedPhys:isCheckedPhys,
                      notesText : this.props.symptomPass.text,
                      thc:  this.props.symptomPass.thc,
                      thca: this.props.symptomPass.thca,
                      thcv: this.props.symptomPass.thcv,
                      limonene: this.props.symptomPass.limonene,
                      humulene: this.props.symptomPass.humulene,
                      myrcene: this.props.symptomPass.myrcene,
                      linalool: this.props.symptomPass.linalool,
                      ocimene: this.props.symptomPass.ocimene,
                      terpinolene: this.props.symptomPass.terpinolene,
                      pinene: this.props.symptomPass.pinene,
                      terpinolene: this.props.symptomPass.terpinolene,
                      caryophyllene: this.props.symptomPass.caryophyllene,
                      cbd: this.props.symptomPass.cbd,
                      selectedDispancries: this.props.symptomPass.dispensary['@id'],
                      filterMethodItem: this.props.symptomPass.method['@id'],
                      selected_method_data: this.props.symptomPass.specific_method,
                      selected_type_data: this.props.symptomPass.specific_type,
                      selectedStrain: this.props.symptomPass.strain,
                      symptiomsPhysical:symptiomsPhysical,
                      selected_pain:selected_pain,
                      selected_phys_symptoms:selected_phys_symptoms,
            
                      physicalRating:this.props.symptomPass.physical_rating,
                    
            
                      experienceRating:this.props.symptomPass.experience_rating,
                   
                    })
            }, 1000);
              
            let dataSys = this.props.fetchSymptions
            pain = dataSys.filter(function (item) {
                return item.category == 'Pain';
            }).map(function (dataSys) {
                return { name : dataSys.name, category:dataSys.category, id: dataSys['@id'] };
            });

            physchology = dataSys.filter(function (item) {
                return item.category == 'Psychological';
            }).map(function (dataSys) {
                return { name : dataSys.name, category:dataSys.category, id: dataSys['@id'] };
            });

            physical = dataSys.filter(function (item) {
                return item.category == 'Physical';
            }).map(function (dataSys) {
                return { name : dataSys.name, category:dataSys.category, id: dataSys['@id'] };
            });

            this.setState({ sysPain: pain, sysPhychology: physchology, sysPhyscial: physical,symptionData: this.props.fetchSymptions })


    }


    async componentWillReceiveProps(nextProps) {
        debugger
        if (Actions.currentScene == "editTimeline") await this.setState({ loader: nextProps.loader })
        if (nextProps.status === '2000') {
            let data = nextProps.fetchSymptions
            pain = data.filter(function (item) {
                return item.category == 'Pain';
            }).map(function (data) {
                return { name : data.name, category:data.category, id: data['@id'] };
            });

            physchology = data.filter(function (item) {
                return item.category == 'Psychological';
            }).map(function (data) {
                return { name : data.name, category:data.category, id: data['@id'] };
            });

            physical = data.filter(function (item) {
                return item.category == 'Physical';
            }).map(function (data) {
                return { name : data.name, category:data.category, id: data['@id'] };
            });

            this.setState({ sysPain: pain, sysPhychology: physchology, sysPhyscial: physical,symptionData: nextProps.fetchSymptions })
            this.props.action.getMethods(this.props.userDetail)
        }
        else if (nextProps.status === '2020') {
            this.props.action.getSymptoms('api/symptoms', this.props.userDetail)

            this.setState({ customSymp: '', selectedSys: 0, selectedSysIndex: 0 })
            // alert('Symptoms Added Successfully')
        }
        else if (nextProps.status === '3000') {

            this.setState({ symptionData: nextProps.fetchSymptions })
        }
        else if (nextProps.status === '2902') {
            let filterMethod = []
            var methodtype = nextProps.methodList.filter(function (item) {
                filterMethod.push(item.name)
                return item.name
            });
            this.props.action.getSpecificMethods(this.props.userDetail)
            this.setState({ method_list: nextProps.methodList, 
                filterMethod: filterMethod
             })
        }

        else if (nextProps.status === '2222') {
            let data = nextProps.fetchStrain
            if(data.length > 0){
            this.setState({ filterStrain: [],fetchSearchStrain: [] })
            this.setState({ fetchSearchStrain: nextProps.fetchStrain })
            }
            else{

            }
        }
        else if (nextProps.status === '2903') {
            debugger
            this.getmethodSpecific(nextProps)
          
        }

        else if (nextProps.status === '22100') { 
            this.props.action.getMethods(this.props.userDetail)

            let imgData = []
            let data = nextProps.editTimeline.galleries
            editTimeline = data.filter(function (item) {
                    debugger
                    imgData.push({ path: item.originalUrl, fileName: item.fileName ? item.fileName : new Date().getTime() + '.jpg' })

            })

            this.setState({imgList:imgData})
debugger
            console.log(this.state.imgList)
            
          
        }


        else if (nextProps.status === '2904') {
            debugger
            this.getTypeSpecfic(nextProps)
            
        }


        else if (nextProps.status === '2202') {
            let data = nextProps.fetchDispensaries
            if(data.length > 0){

            
            this.setState({ filterDispancrie: [],fetchdispensaries: []  })
            debugger

            this.setState({ fetchdispensaries: nextProps.fetchDispensaries })
            }
        }
        else if (nextProps.status === '2099') {
          debugger
          let formData = new FormData();
          
        //   formData.append('file_1', { uri: url.path, type: 'image/jpg', name: 'image.jpg'})

            let geturl = nextProps.saveSessionResult['@id']
            let posturl = geturl.substring(1)
            if(this.state.imgList.length>0){

            
            const photos = this.state.imgList
            let putValue = 0
            photos.forEach((photo) => {
                debugger
                putValue= putValue+1
                formData.append('file_'+putValue, {
                
                uri: Platform.OS === "android" ? photo.path : photo.path.replace("file://", ""),
                type: 'image/jpeg', // or photo.type
                name: 'file_'+putValue
              });  
            });

            this.setState({session_id:posturl})
          this.props.action.postImages(formData,posturl,this.props.userDetail)
        }
        else{

            this.setState({isReviewModal:true})
        }
            
        }
        else if (nextProps.status === '2343') {

            this.setState({isReviewModal:true})
            debugger
        }
        else if (nextProps.status === '2786') {
            this.setState({isReviewModal:false})
            Actions.Experince_Timeline()

            debugger
        }
        
        else if (nextProps.status === '4555') {
            alert('Internal server error')
            debugger
        }
    }


    componentWillUnmount() {
        this.clearReducerState();
    }
    clearReducerState = () => { // reset initial state.
        this.props.action.ClearAction()
    }



async getmethodSpecific(nextProps){
    this.setState({ methodSpecificList: nextProps.methodSpecificList })
    let selected_method = this.state.method_list[0]
    let selected_method_id = selected_method['@id']
    let filter_specific = this.state.methodSpecificList
    filter_specific_method = filter_specific.filter(function (item) {
        return item.method == selected_method_id;
        
    }).map(function (ddd) {
        debugger
        return { name: ddd.name,id: ddd['@id'], method: ddd.method };
    });
      await  this.setState({ filter_method_record: filter_specific_method })
    debugger
    this.props.action.getSpecificType(this.props.userDetail)
}



async getTypeSpecfic(nextProps){
    this.setState({ 
        typeSpecificList: nextProps.typeSpecificList })
            let selected_type = this.state.method_list[0]
            let selected_type_id = selected_type['@id']
            let filter_specific = this.state.typeSpecificList
            filter_specific_type = filter_specific.filter(function (item) {
                return item.type == selected_type_id;
            }).map(function (data) {
                debugger
                return { name:data.name,id:data['@id'], method: data.method, };
            });
         
             await this.setState({ filter_type_record: filter_specific_type })
           
}
    //date time picker

    showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    };

    hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
    };

    handleDatePicked = date => {
        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var thisMonth = months[date.getMonth()]; // 
        day = date.getDate();
        month = date.getMonth();
        year = date.getFullYear();
        let filterDate = thisMonth + ' ' + day + ', ' + year
        console.log("A date has been picked: ", date);
        this.setState({ showDate: filterDate, selectedDate: date })
        this.hideDateTimePicker();
    };






    customSymptons() {
        
        if (this.state.customSymp !== '') {
            let cat = ''
            if (this.state.selectedSysIndex == 1) {
                cat = 'Pain'
            }
            else if (this.state.selectedSysIndex == 2) {
                cat = 'Physical'
            }
            else {
                cat = 'Psychological'
            }


            let postData = {

                "name": this.state.customSymp,
                "category": cat

            }

            this.props.action.postSymptoms(postData, this.props.userDetail)
        }
    }

    _selectImage = async (type) => {
        await this.setState({ isActionSheetOpen: false })
        setTimeout(() => {
            this._selectImgFromGallery(type)
        }, 1000)
    }

    _removeImgFromSelectedImg = (i) => {
        this.setState({ isDeleted: -1 })
        let { imgList } = this.state;
        imgList.splice(i, 1)
        this.setState({ imgList: imgList })
    }

    onEatingClick() {
        this.setState({
            eating: true,
            inhanld: false,
            Orient: false,
            typicaclly: false
        }
        )
    }
    onInhadeClick() {
        this.setState({
            inhanld: true,
            eating: false,
            Orient: false,
            typicaclly: false
        }
        )
    }
    onTypicaclly() {
        this.setState({
            eating: false,
            inhanld: false,
            Orient: false,
            typicaclly: true
        }
        )
    }
    onOrient() {
        this.setState({
            eating: false,
            inhanld: false,
            Orient: true,
            typicaclly: false
        }
        )
    }
    onIncreament() {
        let temPCount = parseInt(this.state.numberStatus) + 1
        this.setState({ numberStatus: temPCount })

    }
    onDecrement() {
        let temPCount = parseInt(this.state.numberStatus) - 1
        if (temPCount >= 0) {
            this.setState({ numberStatus: temPCount })

        }

    }



    _storeImage = (images, type) => { // set the image on list from gallery and  camera.
        let { imgList,ImageViewer } = this.state;
        if (Platform.OS === 'ios' && type === 'ioscamera') {
            imgList.push({ path: images.path, fileName: images.filename ? images.filename : new Date().getTime() + '.jpg' })
            ImageViewer.push({
                // Simplest usage.
                url: images.path,
             
                // width: number
                // height: number
                // Optional, if you know the image size, you can set the optimization performance
             
                // You can pass props to <Image />.
                props: {
                    // headers: ...
            }})
        }
        
        else {
            for (let i in images) {
                imgList.push({ path: images[i].path, fileName: images[i].filename })
                ImageViewer.push({
                    // Simplest usage.
                    url: images[i].path,
                 
                    // width: number
                    // height: number
                    // Optional, if you know the image size, you can set the optimization performance
                 
                    // You can pass props to <Image />.
                    props: {
                        // headers: ...
                }})
            }
        }

        this.setState({ imgList: imgList,ImageViewer:ImageViewer })
    }

    //
    onChangeCustomText(customSymp) {
        this.setState({ customSymp })
        if (customSymp.length == 0) {
            this.setState({ isCustomButton: false })
        }
        else {
            this.setState({ isCustomButton: true })

        }
    }

    //Choose a image
    _selectImgFromGallery = (type) => { // open image picker for select photo from gallery.
        if (type === 'gallery') {
            ImagePicker.openPicker({
                multiple: true,
                maxFiles:3

            }).then(images => {
                this._storeImage(images, 'iosgallery')
            });
        } else {
            ImagePicker.openCamera({
                multiple: true,
                maxFiles:3
            }).then(images => {
                // console.log("image ===>", images)
                this._storeImage(images, 'ioscamera')
            });
        }
    }





    isPainChecked(item) {
        debugger
        let symptoms = 
        {
            'symptom': item.id,
        }
        this.setState({ isCheckedPain: item.name, selected_pain:symptoms})
    }


    isPhysChecked(item) {
        debugger
        let symptoms = 
        {
            'symptom': item.id,
        }
        this.setState({ isCheckedPhys: item.name,selected_phys_symptoms:symptoms })
    }

    isPhysCheckedMethod(item) {
        debugger
      
    
        this.setState({ isCheckedPhysMethod: item.name,selected_method_data:item.id})
    }
    isPhysCheckedType(item) {
        debugger
       
        this.setState({ isCheckedPhysType: item.name, selected_type_data:item.id })
    }

    isPhsycialChecked(item) {
        debugger
        let symptoms = 
        {
            'symptom': item.id,
        }

        this.setState({ isCheckedPhyscial: item.name,symptiomsPhysical:symptoms})
    }



    //render 
    renderPain = ({ item, index }) => {
        return (

            <View style={{ paddingHorizontal: 25, width: '50%', justifyContent: 'flex-start', marginTop: 5, }}>
                <RadioButton1 radioValue={item.name} isChecked={this.state.isCheckedPain == item.name} pressRadioBtn={() => this.isPainChecked(item)} />
            </View>
        )
    }


    renderPhys = ({ item, index }) => {
        return (

            <View style={{ paddingHorizontal: 25, width: '50%', justifyContent: 'flex-start', }}>
                <RadioButton1 radioValue={item.name} isChecked={this.state.isCheckedPhys == item.name} pressRadioBtn={() => this.isPhysChecked(item)} />
            </View>
        )
    }

    render_method_specific = ({ item, index }) => {
        return (

            <View style={{ paddingHorizontal: 25, width: '33.3%', justifyContent: 'flex-start', }}>
                <RadioButton1 radioValue={item.name} isChecked={this.state.isCheckedPhysMethod == item.name} pressRadioBtn={() => this.isPhysCheckedMethod(item)} />
            </View>
        )
    }


    render_type_specific = ({ item, index }) => {
        return (

            <View style={{ paddingHorizontal: 25, width: '33.3%', justifyContent: 'flex-start', marginTop: 5, }}>
                <RadioButton1 radioValue={item.name} isChecked={this.state.isCheckedPhysType == item.name} pressRadioBtn={() => this.isPhysCheckedType(item)} />
            </View>
        )
    }


    renderPhyscal = ({ item, index }) => {
        return (

            <View style={{ paddingHorizontal: 25, width: '50%', justifyContent: 'flex-start', marginTop: 5, }}>
                <RadioButton1 radioValue={item.name} isChecked={this.state.isCheckedPhyscial == item.name} pressRadioBtn={() => this.isPhsycialChecked(item)} />
            </View>
        )
    }




    renderBeforeRating = ({ item, index }) => {
        return (

            <TouchableOpacity onPress={() => this.setState({ countBeforeRating: index })}
                style={{ flexDirection: 'row', alignSelf: 'center', width: 30, height: 40, backgroundColor: index > this.state.countBeforeRating ? 'white' : '#2E7FAE', justifyContent: 'center', alignContent: 'center', borderTopLeftRadius: index == 0 ? 8 : 0, borderBottomLeftRadius: index == 0 ? 8 : 0, borderBottomEndRadius: index == 9 ? 8 : 0, borderWidth: 0.5, borderColor: '#2E7FAE', borderTopEndRadius: index == 9 ? 8 : 0 }}>
                <Text style={{ color: '#fff', textAlign: 'center', marginTop: 10, color: index > this.state.countBeforeRating ? '#2E7FAE' : 'white', }} >{item.value}</Text>
            </TouchableOpacity>
        )
    }

    renderAfterRating = ({ item, index }) => {
        return (
            <TouchableOpacity onPress={() => this.setState({ countAfterRating: index })}
                style={{ flexDirection: 'row', alignSelf: 'center', width: 30, height: 40, backgroundColor: index > this.state.countAfterRating ? 'white' : '#2E7FAE', justifyContent: 'center', alignContent: 'center', borderTopLeftRadius: index == 0 ? 8 : 0, borderBottomLeftRadius: index == 0 ? 8 : 0, borderBottomEndRadius: index == 9 ? 8 : 0, borderWidth: 0.5, borderColor: '#2E7FAE', borderTopEndRadius: index == 9 ? 8 : 0 }}>
                <Text style={{ color: '#fff', textAlign: 'center', marginTop: 10, color: index > this.state.countAfterRating ? '#2E7FAE' : 'white', }} >{item.value}</Text>
            </TouchableOpacity>
        )
    }




    async filterStrainData(data) {
        debugger
        if(data.type == "Hybrid"){
            this.setState({isHybrid:true,isIndica:false,isSativa:false})
        }
        else if(data.type == "Indica"){
            this.setState({isHybrid:false,isIndica:true,isSativa:false})

        }
        else if(data.type == "Sativa"){
            this.setState({isHybrid:false,isIndica:false,isSativa:true})

        }
        this.setState({ filterStrain: data, pickStrainValue: data.search_name,selectedStrain:data['@id'] })
    }


    async filterDispancriesData(data) {
        let selectedDispancries = data['@id'] 
        this.setState({ filterDispancrie: data, pickdispensaries: data.name,selectedDispancries:selectedDispancries})
    }


    renderStrain = ({ item, index }) => {
        return (

            <TouchableOpacity
                onPress={() => this.filterStrainData(item)}
                style={{ borderWidth: 0.5, padding: 10, paddingHorizontal: 20 }}>
                <Text style={{ color: '#007DB3', fontFamily: 'OpenSans', fontSize: 12 }}>{item.search_name}</Text>
            </TouchableOpacity>
        )
    }


    renderDispencries = ({ item, index }) => {
        return (

            <TouchableOpacity
                onPress={() => this.filterDispancriesData(item)}
                style={{ borderWidth: 0.5, padding: 10, paddingHorizontal: 20 }}>
                <Text style={{ color: '#007DB3', fontFamily: 'OpenSans', fontSize: 12 }}>{item.name}</Text>
            </TouchableOpacity>
        )
    }





    async handleScroll(event) {
        if (event.nativeEvent.contentOffset.y == -3 || event.nativeEvent.contentOffset.y == -2 || event.nativeEvent.contentOffset.y == -4 || event.nativeEvent.contentOffset.y == -5 || event.nativeEvent.contentOffset.y == -6) {
            this.setState({ isModalVisibleSymptomsScreen: false })

        }
        console.log(event.nativeEvent.contentOffset.y);
    }

    pickStrain(value) {
        this.setState({ pickStrainValue: value })
        if (value !== '') {
            this.setState({ fetchSearchStrain: [] })
            this.props.action.searchStrain(value, this.props.userDetail)
        }
    }

    pickpickdispensaries(value) {
        this.setState({ pickdispensaries: value, })
        if (value !== '') {
            this.props.action.searchDispensaries(value, this.props.userDetail)
        }
    }

    ratingCompleted(rating) {
        console.log("Rating is: " + rating)
    }

    onStarRatingPress(rating) {
        this.setState({
            treatRating: rating
        });
    }


    onQualityStarRatingPress(rating) {
        this.setState({
            experienceRating: rating
        });
    }


    onFlovourStarRatingPress(rating) {
        this.setState({
            physicalRating: rating
        });
    }



    async onSwipeDown() {
        await this.setState({ isModalVisibleSearch: false, isModalVisibleSymptomsScreen: false })
    }
    isSativaSelected() {
        this.setState({ isSativa: true, isIndica: false, isHybrid: false })
    }

    isisIndicaSelected() {
        this.setState({ isSativa: false, isIndica: true, isHybrid: false })
    }

    isHybridSelected() {
        this.setState({ isSativa: false, isIndica: false, isHybrid: true })
    }


    handleIndexChange = index => {
        this.setState({
            ...this.state,
            selectedIndex: index
        });
        let selected_method = this.state.method_list[index]
        let selected_method_id = selected_method['@id']
        let filter_specific = this.state.methodSpecificList
       


        filter_specific_method = filter_specific.filter(function (item) {
            return item.method == selected_method_id;
            
        }).map(function (ddd) {
            debugger
            return { name: ddd.name,id: ddd['@id'], method: ddd.method };
        });




        let selected_type_id = selected_method['@id']
        let filter_specific_id = this.state.typeSpecificList
        filter_specific_type = filter_specific_id.filter(function (item) {
            return item.method == selected_method_id;
            
        }).map(function (ddd) {
            debugger
            return { name: ddd.name,id: ddd['@id'], method: ddd.method };
        });


        if(index ==1 || index == 2){
            this.setState({isHits:'Mg'})
        }
        else{
            this.setState({isHits:'Hits'})

        }
        debugger
        this.setState({ filter_type_record: filter_specific_type,filterMethodItem: selected_type_id})
        this.setState({ filter_method_record: filter_specific_method })

    };


    _saveSymptomsClicked() {
        let selectedSymtoms = ''
        let {isCheckedPain,isCheckedPhyscial,isCheckedPhys} = this.state;
        if(isCheckedPhys !==0){
            if(selectedSymtoms == ''){
                selectedSymtoms = selectedSymtoms  + isCheckedPhys

            }
            else{
                selectedSymtoms = selectedSymtoms + ',' + isCheckedPhys
            }
        }
        if(isCheckedPain !== ''){

            if(selectedSymtoms == ''){
                selectedSymtoms = selectedSymtoms  + isCheckedPain

            }
            else{
                selectedSymtoms = selectedSymtoms + ',' + isCheckedPain
            }
        }
         if(isCheckedPhyscial !==''){
            if(selectedSymtoms == ''){
                selectedSymtoms = selectedSymtoms  + isCheckedPhyscial

            }
            else{
                selectedSymtoms = selectedSymtoms + ',' + isCheckedPhyscial
            }

        }
       

    
        debugger
        this.setState({ isModalVisibleSymptomsScreen: false, isRating: true, selectedSymtoms: selectedSymtoms })
    }

    _onSaveStrain() {
        if (this.state.pickStrainValue == '') {
            alert('Please fill strain value')
        }
        else {
            this.setState({
                isModalVisibleSearch: false,
                isModalVisibleSymptomsScreen: false,
                isPickStrain: true
            })
        }


    }

    saveReview(){
        this.setState({isReviewModal:false})
        let postData = {
            "relatedSession": this.state.session_id,
            // "consumer": "string",
            "text": this.state.notesText,
            // "relatedSession": "string",
          
            "overallRating": 1,
            "treatmentRating": this.state.treatRating,
            "experienceRating": this.state.experienceRating,
            "physicalRating": this.state.physicalRating,
            "public": this.state.isCheckedPublic
        }
        this.props.action.postReview(postData,this.props.userDetail)
        // Actions.Experince_Timeline()
    }
    moreSpecfifcClick(){
        this.handleIndexChange(0)
        this.setState({ isMoreSpecific: !this.state.isMoreSpecific })
    }

    _saveSession() {
        let {
            pickdispensaries,
            selectedDate,
            pickStrainValue,
            selectedStrain,
            value,
            terpinolene,
            treatRating,
            experienceRating,
            physicalRating,
            countBeforeRating,
            countAfterRating,
            thc,
            thca,
            thcv,
            cbc,
            cbd,
            cbg,
            cbn,
            pinene,
            myrcene,
            limonene,
            caryophyllene,
            linalool,
            humulene,
            ocimene,
            notesText,
            terpinlolene,
            filterMethodItem,
            isCheckedPhysType,
            isCheckedPhysMethod,
            symptiomsPhysical,selected_phys_symptoms, selected_pain,
            selectedDispancries,
            selected_method_data,
            selected_type_data,
           imgList

        } = this.state;
        let image = imgList[0]
        if (pickdispensaries == '') {
            alert('Please choose dispensary')
        }
        else if (pickStrainValue == '') {
            alert('Please choose strain')
        }
        else if(notesText == ''){
            alert('Please enter notes')
        }
        else{

        
        let postData = {
            "dispensary": selectedDispancries,
            "date": selectedDate,
            "strain": selectedStrain,
            "method": filterMethodItem,
            "specificMethod": selected_method_data,
            "specificType": selected_type_data,
            "specificAmount": value,
            "sessionSymptoms": 
                [symptiomsPhysical,selected_phys_symptoms,selected_pain]
            ,
            "overallRating":0,
            "treatmentRating": treatRating,
            "experienceRating": experienceRating,
            "physicalRating": physicalRating,
            "beforePain": countBeforeRating,
            "afterPain": countAfterRating,
            "thc":parseFloat(thc),
            "thca": parseFloat(thca),
            "thcv":parseFloat(thcv),
            "cbc": parseFloat(cbc),
            "cbd": parseFloat(cbd),
            "cbg": parseFloat(cbg),
            "cbn": parseFloat(cbn),
            "text": notesText,
            "pinene": parseFloat(pinene),
            "myrcene":parseFloat(myrcene),
            "limonene": parseFloat(limonene),
            "caryophyllene": parseFloat(caryophyllene),
            "linalool": parseFloat(linalool),
            "humulene": parseFloat(humulene),
            "ocimene": parseFloat(ocimene),
            "terpinolene": parseFloat(terpinolene)
        }
        this.setState({loader:true})
        this.props.action.saveSession(postData,this.props.userDetail,'PUT')
    }
        debugger
    }

    rederphotos = ({ item, index, move, moveEnd, isActive }) => {
        return (    
    <TouchableOpacity 
    onLongPress={move}
    onPressOut={moveEnd}
    style={styles.ImgStyle} key={index}>
    <TouchableOpacity onPress = {()=>this.setState({isfullImage:true,selectedImgIndex:index})}
     onLongPress={() => this.setState({ isDeleted: index })} >
        {this.state.isDeleted == index ?
            <TouchableOpacity activeOpacity={1.0} style={{ position: 'absolute', right: 0, top: 0, zIndex: 1, }} TouchableOpacity={0.2} onPress={() => this._removeImgFromSelectedImg(index)}>
                <Image style={{ height: 30, width: 30, }} source={require('../../assets/images/img/icon-trash--circle.png')} resizeMode="contain" />
            </TouchableOpacity>
            : null
        }
        <Image style={{ height: 110, width: 110, alignSelf: 'center', borderRadius: 10 }} source={{ uri: item.path }} />
    </TouchableOpacity>
</TouchableOpacity>
        )
}


    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
                <View style={{ flex: 1 }}>
                    <View style={{
                        flex: Platform.OS == 'ios' ? 0.07 : 0.1,
                        width: '100%',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center'
                    }}>
                        <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'center' }}
                            onPress={() => Actions.pop()}>

                            <Image resizeMode='contain' style={{ height: 20, width: 20, marginLeft: 25 }} source={require('../../assets/images/img/icon_arrow_left.png')} />
                            <Text style={{ color: '#2A7BB4', textAlign: 'auto', marginLeft: 5, fontFamily: 'OpenSans', fontWeight: 'bold' }}>Timeline</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ position: 'absolute', right: 0, marginRight: 25, }} onPress={this.showDateTimePicker}>
                            <Text style={{ color: '#2A7BB4', fontFamily: 'OpenSans', fontWeight: 'bold' }}>{this.state.showDate}</Text>
                        </TouchableOpacity>
                        <DateTimePicker
                            mode='date'
                            isVisible={this.state.isDateTimePickerVisible}
                            onConfirm={this.handleDatePicked}
                            onCancel={this.hideDateTimePicker}
                        />
                    </View>
                    <View style={{ flex: 1, backgroundColor: 'white' }}>
                        <ScrollView style={{ flex: 1 }}>
                            <Text style={{
                                fontSize: 26,
                                color: '#6559A0',
                                fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                marginLeft: 25,
                                marginTop: 15,
                               
                            }}>Log a Session</Text>
                            <Text style={{ fontSize: 18, color: 'black', marginLeft: 25, marginTop: 30, fontFamily: 'OpenSans' }}>What are you having ?</Text>

                            {this.state.isPickStrain ?
                                <TouchableOpacity
                                    onPress={() => this.setState({ isModalVisibleSearch: true })}
                                    style={{ marginHorizontal: 25, borderWidth: 1, borderRadius: 12, height: 60, marginTop: 20, justifyContent: 'center', alignItems: 'center', borderColor: '#66B2D2' }}>
                                    <Text
                                        style={{
                                            color: '#66B2D2',
                                            fontWeight: '600',
                                            fontSize: 16,
                                            fontFamily: 'OpenSans'
                                        }}>{
                                            this.state.pickStrainValue}
                                    </Text>

                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={() => this.setState({ isModalVisibleSearch: true })}
                                    style={{ marginLeft: 25, borderWidth: 1, borderRadius: 12, height: 40, marginTop: 15, justifyContent: 'center', alignItems: 'center', backgroundColor: '#2E7FAE', width: 160, borderColor: 'transparent' }}>
                                    <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 12, fontFamily: 'OpenSans' }}>Pick Strain</Text>
                                </TouchableOpacity>
                            }
                            <Text style={{
                                fontSize: 18,
                                color: 'black',
                                marginLeft: 20,
                                marginTop: 30,
                                fontFamily: 'OpenSans'
                            }}>Where is it from?</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <TextInput
                                    onChangeText={(pickdispensaries) => this.pickpickdispensaries(pickdispensaries)}
                                    value={this.state.pickdispensaries}
                                    placeholder='Search dispensaries'
                                    style={{
                                        flex: 1,
                                        borderWidth: 1,
                                        borderRadius: 10,
                                        height: 40,
                                        marginTop: 10,
                                        marginHorizontal: 20,
                                        paddingLeft: 10,
                                        paddingRight: 40
                                    }}>

                                </TextInput>

                                <TouchableOpacity style={{ alignSelf: 'center', position: 'absolute', right: 15, marginRight: 10, }} onPress={() => console.log('onPress')}>
                                    <Image source={require('../../assets/images/img/icon-search.png')} style={{ height: 20, width: 20, marginRight: 10, marginTop: 8 }}></Image>

                                </TouchableOpacity>
                            </View>
                            {this.state.fetchdispensaries.length !== 0 && this.state.filterDispancrie.length == 0 ?

                                <FlatList
                                    data={this.state.fetchdispensaries}
                                    renderItem={this.renderDispencries}
                                    style={{ borderWidth: 0.5, borderRadius: 10, margin: 20, marginTop: 10, height: 190 }}
                                    //Setting the number of column
                                    extraData={this.state}

                                    keyExtractor={(item, index) => index}
                                />
                                : null}

                            <Text style={{ fontSize: 18, color: 'black', marginLeft: 20, marginTop: 30, fontFamily: 'OpenSans' }}>How are you having it?</Text>
                            <View style={{ marginTop: 10, marginHorizontal: 20 }}>
                                <SegmentedControlTab
                                    tabsContainerStyle={{ height: 32, }}
                                    tabStyle={{ backgroundColor: 'white', borderColor: '#2E7FAE' }}
                                    tabTextStyle={{ color: '#2E7FAE', fontWeight: 'bold', fontSize: 12, fontFamily: 'OpenSans' }}
                                    activeTabStyle={{ backgroundColor: '#2E7FAE', }}
                                    values={this.state.filterMethod}
                                    tabTextStyle={{ color: '#000', fontWeight: 'bold', fontSize: 12, fontFamily: 'OpenSans' }}
                                    selectedIndex={this.state.selectedIndex}
                                    onTabPress={this.handleIndexChange}
                                />

                            </View>
                            <TouchableOpacity onPress={() =>
                                    this.moreSpecfifcClick()
                                } style={{ height: 40, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', width: 160 }}>
                                <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 12, color: '#007BB3', marginLeft: 10, fontFamily: 'OpenSans' }}> {!this.state.isMoreSpecific ? 'Get more specific' : 'Get Less specific'}</Text>
                                <Image source={require('../../assets/images/img/icon_arrow_right.png')} style={{ alignSelf: 'center', height: 20, width: 20, marginLeft: 10 }}></Image>
                            </TouchableOpacity>
                            {this.state.isMoreSpecific ?
                                <View>
                                    <FlatList
                                        data={this.state.filter_method_record}
                                        renderItem={this.render_method_specific}

                                        //Setting the number of column
                                        numColumns={3}
                                        extraData={this.state}

                                        keyExtractor={(item, index) => index}
                                    />

                                    {this.state.filter_type_record && this.state.filter_type_record.length > 0 ?
                                        <View style={{ height: 1, backgroundColor: 'gray', width: '80%', alignSelf: 'center', marginVertical: 5 }} /> : null
                                    }
                                    <FlatList
                                        data={this.state.filter_type_record}
                                        renderItem={this.render_type_specific}

                                        //Setting the number of column
                                        numColumns={3}
                                        extraData={this.state}

                                        keyExtractor={(item, index) => index}
                                    />
                                       <View style={{ marginLeft: 20, marginVertical: 10, flexDirection: 'row', alignItems: 'center' }}>

                                        <TouchableOpacity  hitSlop={{top: 10, bottom: 10, left: 10, right: 8}} onPress={() => this.onDecrement()}>
                                            <Text style={{ textAlign: 'center', fontSize: 24, color: '#3089CA' }}>-</Text>
                                        </TouchableOpacity>
                                        <View style={{ borderWidth: 1, borderRadius: 10, height: 30, width: 60, marginHorizontal: 8, justifyContent: 'center', alignItems: 'center', borderColor: '#3089CA' }}>
                                            <Text style={{ textAlign: 'center' }}>{this.state.numberStatus}
                                            </Text>
                                        </View>


                                        <TouchableOpacity hitSlop={{top: 10, bottom: 10, left: 8, right: 10}}  onPress={() => this.onIncreament()}>
                                            <Text style={{ fontSize: 24, color: '#3089CA' }}>+</Text>
                                        </TouchableOpacity>
                                        <Text style={{ fontFamily: 'OpenSans', fontSize: 16, textAlign: 'center', marginLeft: 5, fontSize: 12,marginLeft:10 }}>{this.state.isHits}</Text>
                                    </View>
                                </View>
                                : null
                            }

                            <Text style={{ fontSize: 18, color: 'black', marginLeft: 20, fontFamily: 'OpenSans', marginTop: 5 }}>What are you treating ?</Text>



                            {this.state.isRating ?

                                <View style={{ justifyContent: 'center', alignContent: 'center', marginHorizontal: 20 }}>
                                    <TouchableOpacity
                                        onPress={() => this.setState({ isModalVisibleSymptomsScreen: true,selectedSymtoms:'' })}
                                        style={{ 
                                            borderWidth: 1, 
                                        borderRadius: 12, 
                                        height: 60, 
                                        marginTop: 5, 
                                        justifyContent: 'center', 
                                        alignItems: 'center', 
                                        borderColor: '#66B2D2' 
                                        }}>
                                        <Text
                                            style={{ color: '#66B2D2', fontWeight: '600', fontSize: 16 }}>{
                                                this.state.selectedSymtoms}
                                        </Text>

                                    </TouchableOpacity>
                                    <Text style={{ fontSize: 18, color: 'black', marginTop: 30, fontFamily: 'OpenSans' }}>Rate your pain</Text>
                                    <Text style={{ fontSize: 12, color: 'black', marginTop: 10, fontFamily: 'OpenSans', marginBottom: 5 }}>Before use</Text>
                                    <FlatList
                                        style={{ flex: 1 }}
                                        data={this.state.entries}
                                        renderItem={this.renderBeforeRating}
                                        horizontal={true}
                                        scrollEnabled={false}
                                        //Setting the number of column
                                        extraData={this.state}

                                        keyExtractor={(item, index) => index}
                                    />
                                    <Text style={{ fontSize: 12, color: 'black', marginTop: 10, fontFamily: 'OpenSans', marginBottom: 5 }}>Before use</Text>

                                    <FlatList
                                        style={{ flex: 1 }}
                                        data={this.state.entries}
                                        renderItem={this.renderAfterRating}
                                        horizontal={true}
                                        //Setting the number of column
                                        extraData={this.state}
                                        scrollEnabled={false}

                                        keyExtractor={(item, index) => index}
                                    />
                                </View> :
                                <TouchableOpacity onPress={() => this.setState({ isModalVisibleSymptomsScreen: true })} style={{ marginLeft: 20, borderWidth: 1, borderRadius: 12, height: 40, marginTop: 15, justifyContent: 'center', alignItems: 'center', backgroundColor: '#2E7FAE', width: 160, borderColor: 'transparent' }}>
                                    <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 12, fontFamily: 'OpenSans' }}>Pick Symptoms</Text>
                                </TouchableOpacity>
                            }


                            <Text style={{ fontSize: 18, color: 'black', marginLeft: 20, marginTop: 30, fontFamily: 'OpenSans' }}>Ratings</Text>
                            <Text style={{ fontSize: 14, color: 'black', marginLeft: 20, marginTop: 10, fontFamily: 'OpenSans' }}>Treating of symptoms</Text>


                            <View style={{ marginHorizontal: 20, width: 90, marginTop: 5 }}>

                                <StarRating

                                    disabled={false}
                                    starSize={23}
                                    maxStars={5}
                                    starStyle={{
                                        marginRight: 5,
                                        color: '#2E7FAE'
                                    }}
                                    rating={this.state.treatRating}
                                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                                />

                            </View>


                            <Text style={{ fontSize: 14, color: 'black', marginLeft: 20, marginTop: 10, fontFamily: 'OpenSans' }}>Quality of experience </Text>


                            <View style={{ marginHorizontal: 20, width: 90, marginTop: 5 }}>

                                <StarRating

                                    disabled={false}
                                    starSize={23}
                                    maxStars={5}
                                    starStyle={{
                                        marginRight: 5,
                                        color: '#2E7FAE'
                                    }}
                                    rating={this.state.experienceRating}
                                    selectedStar={(rating) => this.onQualityStarRatingPress(rating)}
                                />

                            </View>


                            <Text style={{ fontSize: 14, color: 'black', marginLeft: 20, marginTop: 10, fontFamily: 'OpenSans'  }}>Flavor and appearance</Text>


                            <View style={{ marginHorizontal: 20, width: 90, marginTop: 5, marginBottom: 10 }}>

                                <StarRating

                                    disabled={false}
                                    starSize={23}
                                    maxStars={5}
                                    starStyle={{
                                        marginRight: 5,
                                        color: '#2E7FAE'
                                    }}
                                    rating={this.state.physicalRating}
                                    selectedStar={(rating) => this.onFlovourStarRatingPress(rating)}
                                />

                            </View>
                            {this.state.imgList && this.state.imgList.length > 0 ?
                                <View>
                                    <Text style={{ fontSize: 18, color: 'black', marginLeft: 20, fontFamily: 'OpenSans' }}>Photos</Text>
                                   


<DraggableFlatList
          style={{ marginLeft: 15 }}
          horizontal
          data={this.state.imgList}
          renderItem={this.rederphotos}
          marginVertical={5}
          extraData={this.state}
          marginRight={15}
          keyExtractor={(item, index) => `draggable-item-${item.key}`}
          scrollPercent={5}
          ItemSeparatorComponent={() => <View style={{ width: 10 }}></View>}

          onMoveEnd={({ imgList }) => this.setState({ imgList })}
        />
                                </View> : null
                            }
                            <TouchableOpacity 
                                disabled = {this.state.imgList.length < 3 ? false : true}
                              onPress={() => 
                             this.setState({ 
                                isActionSheetOpen: true 
                            })} style={{ 
                                flexDirection: 'row', 
                                borderWidth: 1, 
                            justifyContent: 'center', 
                            alignItems: 'center', 
                            borderRadius: 10, 
                            height: 40, 
                            marginTop: 5, 
                            marginHorizontal: 20, 
                            paddingHorizontal: 10,
                            opacity: this.state.imgList.length < 3 ? 1 : 0.6,
                             backgroundColor: '#6559A0',
                              borderColor: 'transparent' }}>
                                <Image source={require('../../assets/images/img/icon_camera.png')} style={{ alignSelf: 'center', height: 30, width: 30, position: 'absolute', left: 0, marginLeft: 5 }}></Image>
                                <Text style={{ color: 'white', fontFamily: 'OpenSans', fontWeight: 'bold', fontSize: 12 }}>Add Photos </Text>
                            </TouchableOpacity>
                            <Text style={{ fontSize: 18, color: 'black', marginLeft: 20, marginTop: 30, fontFamily: 'OpenSans' }}>Notes</Text>
                            <TextInput
                             placeholder='Write a note'
                             onChangeText={(text) => this.setState({ notesText: text })}
                             value={this.state.notesText}
                             multiline={true} style={{ 
                                 borderRadius: 8, 
                                borderWidth: 1,
                                 height: 90, 
                                marginHorizontal: 20, 
                             marginTop: 10, 
                             paddingLeft: 10 
                             }}>

                            </TextInput>



                            <TouchableOpacity onPress={() => this._saveSession()} style={{
                                borderWidth: 1,
                                borderRadius: 16,
                                height: 40,
                                marginTop: 40,
                                marginHorizontal: 20,
                                justifyContent: 'center',
                                alignItems: 'center',
                                backgroundColor: '#3089CA',
                                marginBottom: 20,
                                borderColor: 'transparent'
                            }}>
                                <Text style={{
                                    color: 'white',
                                    fontWeight: 'bold',
                                    fontSize: 12,
                                    fontFamily: 'OpenSans'
                                }}>Save Session</Text>
                            </TouchableOpacity>







                        </ScrollView>
                    </View>
                </View>
                {this.state.isModalVisibleSearch ?
                    <Modal
                        propagateSwipe={true}
                        onSwipeThreshold={20}
                        swipeDirection="down"
                        onSwipeComplete={() => this.setState({ isModalVisibleSearch: false })}
                        style={{ margin: 0, marginTop: 60 }}
                        isVisible={this.state.isModalVisibleSearch}>
                        <View style={{ flex: 1, backgroundColor: 'white' }}>

                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('../../assets/images/img/icon_pull_handle.png')} style={{ width: 90, height: 5, marginTop: 10 }} />
                            </View>
                            <ScrollView
                                onScroll={this.handleScroll.bind(this)}
                            >
                                <TouchableWithoutFeedback>
                                    <View>

                                        <Text style={{
                                            fontSize: 26,
                                            color: '#6559A0',
                                            fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                            marginLeft: 25,
                                            marginTop: 40
                                        }}>What are you having?</Text>


                                        <Text style={{
                                            fontSize: 18,
                                            color: 'black',
                                            fontFamily: 'OpenSans',
                                            marginLeft: 20,
                                            marginTop: 30
                                        }}>Search for a strain?</Text>
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <TextInput
                                                onChangeText={(pickStrainValue) => this.pickStrain(pickStrainValue)}
                                                value={this.state.pickStrainValue}
                                                placeholder='Search for a strain'
                                                style={{
                                                    flex: 1,
                                                    borderWidth: 1,
                                                    borderRadius: 10,
                                                    height: 40,
                                                    marginTop: 10,
                                                    marginHorizontal: 20,
                                                    paddingLeft: 10,
                                                    paddingRight: 40
                                                }}>

                                            </TextInput>
                                            <TouchableOpacity style={{ alignSelf: 'center', position: 'absolute', right: 15, marginRight: 10, }}>
                                                <Image source={require('../../assets/images/img/icon-search.png')} style={{ height: 20, width: 20, marginRight: 10, marginTop: 8 }}>

                                                </Image>

                                            </TouchableOpacity>
                                        </View>
                                        {this.state.fetchSearchStrain.length !== 0 && this.state.filterStrain.length == 0 ?
                                            <FlatList
                                                data={this.state.fetchSearchStrain}
                                                renderItem={this.renderStrain}
                                                style={{ borderWidth: 0.5, borderRadius: 10, margin: 20, marginTop: 15, height: 190 }}
                                                //Setting the number of column
                                                extraData={this.state}

                                                keyExtractor={(item, index) => index}
                                            />
                                            :

                                            <View>
                                                <View style={{ marginHorizontal: 15, paddingHorizontal: 10, justifyContent: 'flex-start', marginTop: 5, flexDirection: 'row', }}>
                                                    <RadioButton1 radioValue={'Sativa'} isChecked={this.state.isSativa} pressRadioBtn={() => this.isSativaSelected()} />
                                                    <RadioButton1 radioValue={'Indica'} isChecked={this.state.isIndica} pressRadioBtn={() => this.isisIndicaSelected()} />
                                                    <RadioButton1 radioValue={'Hybrid'} isChecked={this.state.isHybrid} pressRadioBtn={() => this.isHybridSelected()} />

                                                </View>

                                                <Text style={{ marginLeft: 20, marginTop: 15, fontSize: 18, color: 'black', fontFamily: 'OpenSans' }}>Strain Composition</Text>
                                                <View style={{ flexDirection: 'row', borderRadius: 10, height: 40, marginTop: 10, marginHorizontal: 20, }}>

                                                    <TextInput
                                                        placeholder='0%'
                                                        
                                                        value={this.state.thc}
                                                        onChangeText={(text) => this.setState({ thc: text })}
                                                        style={{ borderWidth: 1, borderRadius: 10, height: 40, padding: 5, width: 50, marginLeft: 10, fontFamily: 'OpenSans', fontSize: 14, justifyContent: 'center', alignItems: 'center' }}>
                                                    </TextInput>

                                                    <View style={{ justifyContent: 'center', width: 90 }}>
                                                        <Text style={{ fontFamily: 'OpenSans', marginLeft: 10,fontSize:14,color:'black' }}>THC</Text>
                                                    </View>

                                                    <TextInput
                                                        placeholder='0%'
                                                        value={String(this.state.cbd)}
                                                        onChangeText={(text) => this.setState({ cbd: text })}
                                                        style={{ borderWidth: 1, borderRadius: 10, height: 40, width: 50, padding: 5, marginLeft: 20, fontFamily: 'OpenSans', fontSize: 14 }}>
                                                    </TextInput>

                                                    <View style={{ justifyContent: 'center', alignItems: 'center', marginLeft: 5 }}>
                                                        <Text style={{ fontFamily: 'OpenSans', marginLeft: 10,fontSize:14,color:'black' }}>CBD</Text>
                                                    </View>
                                                </View>
                                                <TouchableOpacity onPress={() => this.setState({ isMoreComppnent: !this.state.isMoreComppnent })} style={{ height: 40, marginTop: 10, alignItems: 'center', flexDirection: 'row', }}>
                                                    <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 12, color: '#007BB3', marginLeft: 20, fontFamily: 'OpenSans' }}> {!this.state.isMoreComppnent ? 'More compounds' : 'Fewer compounds'}</Text>
                                                    <Image source={require('../../assets/images/img/icon_arrow_right.png')} style={{ alignSelf: 'center', height: 20, width: 20, marginLeft: 5 }}></Image>
                                                </TouchableOpacity>

                                                {this.state.isMoreComppnent ?
                                                    <View>
                                                        <View style={{
                                                            flexDirection: 'row',
                                                            borderRadius: 10,
                                                            height: 40,
                                                            marginTop: 10,
                                                            marginHorizontal: 20,
                                                            paddingHorizontal: 10
                                                        }}>
                                                            <TextInput
                                                                placeholder='0%'
                                                                value={this.state.thca}
                                                                onChangeText={(text) => this.setState({ thca: text })}
                                                                style={{
                                                                    borderWidth: 1,
                                                                    borderRadius: 10,
                                                                    height: 40,
                                                                    width: 50,
                                                                    padding: 5
                                                                }}>
                                                            </TextInput>
                                                            <View style={{ justifyContent: 'center', marginLeft: 5, width: 90 }}>
                                                                <Text
                                                                    style={{
                                                                        fontFamily: 'OpenSans',
                                                                        marginLeft: 10,
                                                                        fontSize:12,
                                                                    }}>THCA</Text>
                                                            </View>
                                                            <TextInput
                                                                placeholder='0%'
                                                                value={this.state.thcv}
                                                                onChangeText={(text) => this.setState({ thcv: text })}
                                                                style={{
                                                                    borderWidth: 1,
                                                                    borderRadius: 10,
                                                                    height: 40,
                                                                    padding: 5,
                                                                    marginLeft: 20,
                                                                    width: 50
                                                                }}>
                                                            </TextInput>
                                                            <View style={{
                                                                justifyContent: 'center',
                                                                marginLeft: 5
                                                            }}>
                                                                <Text style={{
                                                                    fontFamily: 'OpenSans',
                                                                    marginLeft: 10,
                                                                    fontSize:12,
                                                                }}>THCV</Text>

                                                            </View>
                                                        </View>


                                                        <View style={{
                                                            flexDirection: 'row',
                                                            borderRadius: 10,
                                                            height: 40,
                                                            marginTop: 10,
                                                            marginHorizontal: 20,
                                                            paddingHorizontal: 10
                                                        }}>
                                                            <TextInput 
                                                                placeholder='0%'
                                                                value={this.state.caryophyllene}
                                                                onChangeText={(text) => this.setState({ caryophyllene: text })}
                                                                style={{
                                                                    borderWidth: 1,
                                                                    borderRadius: 10,
                                                                    height: 40,
                                                                    padding: 5,
                                                                    width: 50
                                                                }}>
                                                            </TextInput>
                                                            <View style={{
                                                                justifyContent: 'center',
                                                                marginLeft: 5,
                                                                width: 90,
                                                            }}>
                                                                <Text style={{
                                                                    fontFamily: 'OpenSans',
                                                                    marginLeft: 10,
                                                                    fontSize:12,
                                                                }}>β-Caryophyllene
                                                                ​
                                                                </Text>
                                                            </View>
                                                            <TextInput
                                                                placeholder='0%'
                                                                value={this.state.humulene}
                                                                onChangeText={(text) => this.setState({ humulene: text })}
                                                                style={{
                                                                    borderWidth: 1,
                                                                    borderRadius: 10,
                                                                    height: 40,
                                                                    padding: 5,
                                                                    marginLeft: 20,
                                                                    width: 50
                                                                }}>
                                                            </TextInput>
                                                            <View style={{ justifyContent: 'center', marginLeft: 5 }}>
                                                                <Text style={{ fontFamily: 'OpenSans', marginLeft: 10,fontSize:12, }}>Humulene</Text>

                                                            </View>
                                                        </View>


                                                        <View style={{
                                                            flexDirection: 'row',
                                                            borderRadius: 10,
                                                            height: 40,
                                                            marginTop: 10,
                                                            marginHorizontal: 20,
                                                            paddingHorizontal: 10
                                                        }}>
                                                            <TextInput
                                                             placeholder='0%'
                                                                value={this.state.pinene}
                                                                onChangeText={(text) => this.setState({ pinene: text })}
                                                                style={{
                                                                    borderWidth: 1,
                                                                    borderRadius: 10,
                                                                    height: 40, padding: 5,
                                                                    width: 50,
                                                                }}>
                                                            </TextInput>
                                                            <View style={{ justifyContent: 'center', marginLeft: 5, width: 90 }}>
                                                                <Text
                                                                    style={{
                                                                        fontFamily: 'OpenSans',
                                                                        marginLeft: 10,
                                                                        fontSize:12,
                                                                    }}>α-Pinene</Text>
                                                            </View>
                                                            <TextInput
                                                                placeholder='0%'
                                                                value={this.state.myrcene}
                                                                onChangeText={(text) => this.setState({ myrcene: text })}
                                                                style={{
                                                                    borderWidth: 1,
                                                                    borderRadius: 10,
                                                                    height: 40,
                                                                    fontFamily:'OpenSans',
                                                                    fontSize:12,
                                                                    padding: 5,
                                                                    marginLeft: 20,
                                                                    width: 50
                                                                }}>
                                                            </TextInput>
                                                            <View style={{ justifyContent: 'center', marginLeft: 5 }}>
                                                                <Text style={{ 
                                                                    fontFamily: 'OpenSans', 
                                                                    marginLeft: 10,
                                                                    fontSize:12,
                                                                     }}>β-Myrcene</Text>

                                                            </View>
                                                        </View>

                                                        <View style={{
                                                            flexDirection: 'row',
                                                            borderRadius: 10,
                                                            height: 40, marginTop: 10,
                                                            marginHorizontal: 20,
                                                            paddingHorizontal: 10
                                                        }}>
                                                            <TextInput
                                                                placeholder='0%'
                                                                value={this.state.limonene}
                                                                onChangeText={(text) => this.setState({ limonene: text })}
                                                                style={{
                                                                    borderWidth: 1,
                                                                    borderRadius: 10,
                                                                    height: 40,
                                                                    padding: 5,
                                                                    width: 50
                                                                }}>
                                                            </TextInput>
                                                            <View style={{ justifyContent: 'center', marginLeft: 5, width: 90 }}>
                                                                <Text
                                                                    style={{
                                                                        fontFamily: 'OpenSans',
                                                                        fontSize:12,
                                                                        marginLeft: 10
                                                                    }}>Limonene</Text>
                                                            </View>
                                                            <TextInput
                                                                placeholder='0%'
                                                                value={this.state.terpinlolene}
                                                                onChangeText={(text) => this.setState({ terpinlolene: text })}
                                                                style={{
                                                                    borderWidth: 1,
                                                                    borderRadius: 10,
                                                                    height: 40,
                                                                    padding: 5,
                                                                    marginLeft: 20,
                                                                    width: 50
                                                                }}>
                                                            </TextInput>
                                                            <View style={{ justifyContent: 'center', marginLeft: 5 }}>
                                                                <Text style={{
                                                                    fontFamily: 'OpenSans',
                                                                    marginLeft: 10,
                                                                    fontSize:12
                                                                }}>Terpinlolene</Text>

                                                            </View>
                                                        </View>



                                                        <View style={{
                                                            flexDirection: 'row',
                                                            borderRadius: 10,
                                                            height: 40,
                                                            marginTop: 10,
                                                            marginHorizontal: 20,
                                                            paddingHorizontal: 10
                                                        }}>
                                                            <TextInput
                                                                placeholder='0%'
                                                                onChangeText={(text) => this.setState({ cbg: text })}
                                                                value={this.state.cbg}
                                                                style={{
                                                                    borderWidth: 1,
                                                                    borderRadius: 10,
                                                                    height: 40,
                                                                    padding: 5,
                                                                    width: 50
                                                                }}>
                                                            </TextInput>
                                                            <View style={{
                                                                justifyContent: 'center',
                                                                marginLeft: 5,
                                                                width: 90
                                                            }}>
                                                                <Text style={{
                                                                    fontFamily: 'OpenSans',
                                                                    marginLeft: 10,
                                                                    fontSize:12
                                                                }}>CBG</Text>
                                                            </View>
                                                            <TextInput
                                                                placeholder='0%'
                                                                onChangeText={(text) => this.setState({ cbn: text })}
                                                                value={this.state.cbn}
                                                                style={{
                                                                    borderWidth: 1,
                                                                    borderRadius: 10,
                                                                    height: 40,
                                                                    padding: 5,
                                                                    marginLeft: 20,
                                                                    width: 50
                                                                }}>
                                                            </TextInput>
                                                            <View style={{
                                                                justifyContent: 'center',
                                                                marginLeft: 5
                                                            }}>
                                                                <Text
                                                                    style={{
                                                                        fontFamily: 'OpenSans',
                                                                        marginLeft: 10,
                                                                        fontSize:12
                                                                    }}>CBN</Text>

                                                            </View>
                                                        </View>




                                                        <View style={{
                                                            flexDirection: 'row',
                                                            borderRadius: 10,
                                                            height: 40,
                                                            marginTop: 10,
                                                            marginHorizontal: 20,
                                                            paddingHorizontal: 10
                                                        }}>
                                                            <TextInput
                                                                placeholder='0%'
                                                                value={this.state.linalool}
                                                                onChangeText={(text) => this.setState({ linalool: text })}
                                                                style={{
                                                                    borderWidth: 1,
                                                                    borderRadius: 10,
                                                                    height: 40,
                                                                    padding: 5,
                                                                    width: 50
                                                                }}>
                                                            </TextInput>
                                                            <View style={{
                                                                justifyContent: 'center',
                                                                marginLeft: 5,
                                                                width: 90
                                                            }}>
                                                                <Text style={{
                                                                    fontFamily: 'OpenSans',
                                                                    marginLeft: 10,
                                                                    fontSize:12
                                                                }}>Linalool</Text>
                                                            </View>

                                                            <TextInput
                                                                placeholder='0%'
                                                                value={this.state.ocimene}
                                                                onChangeText={(text) => this.setState({ ocimene: text })}
                                                                style={{
                                                                    borderWidth: 1,
                                                                    borderRadius: 10,
                                                                    height: 40,
                                                                    padding: 5,
                                                                    marginLeft: 20,
                                                                    width: 50
                                                                }}>
                                                            </TextInput>
                                                            <View style={{
                                                                justifyContent: 'center',
                                                                marginLeft: 5
                                                            }}>
                                                                <Text style={{
                                                                    fontFamily: 'OpenSans',
                                                                    marginLeft: 10,
                                                                    fontSize:12
                                                                }}>Ocimene</Text>

                                                            </View>
                                                        </View>





                                                        <View style={{
                                                            flexDirection: 'row',
                                                            borderRadius: 10,
                                                            height: 40,
                                                            marginTop: 10,
                                                            marginHorizontal: 20,
                                                            paddingHorizontal: 10
                                                        }}>
                                                            <TextInput
                                                                placeholder='0%'
                                                                value={this.state.cbc}
                                                                onChangeText={(text) => this.setState({ cbc: text })}
                                                                style={{
                                                                    borderWidth: 1,
                                                                    borderRadius: 10,
                                                                    height: 40,
                                                                    padding: 5,
                                                                    width: 50
                                                                }}>
                                                            </TextInput>
                                                            <View style={{ justifyContent: 'center', marginLeft: 5, width: 90 }}>
                                                                <Text style={{
                                                                    fontFamily: 'OpenSans',
                                                                    marginLeft: 10,
                                                                    fontSize:12
                                                                }}>CBC</Text>
                                                            </View>


                        
                                                
                                                        </View>
                                                    </View>

                                                    : null
                                                }


                                            </View>
                                        }
                                        <TouchableOpacity onPress={() => this._onSaveStrain()} style={{
                                            borderWidth: 1,
                                            borderRadius: 16,
                                            height: 40,
                                            marginTop: 40,
                                            marginHorizontal: 20,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            backgroundColor: '#3089CA',
                                            marginBottom: 20,
                                            borderColor: 'transparent'
                                        }}>
                                            <Text style={{
                                                color: 'white',
                                                fontWeight: 'bold',
                                                fontSize: 16,
                                                fontFamily: 'OpenSans'
                                            }}>Save Strain</Text>
                                        </TouchableOpacity>
                                    </View>
                                </TouchableWithoutFeedback>
                            </ScrollView>
                        </View>
                    </Modal>
                    : null
                }
                {this.state.isModalVisibleSymptomsScreen ?

                    <Modal
                        propagateSwipe={true}
                        onSwipeComplete={() => this._saveSymptomsClicked()}
                        onSwipeThreshold={20}
                        swipeDirection="down"
                        style={{ margin: 0, marginTop: 60, paddingBottom: 20 }}
                        isVisible={this.state.isModalVisibleSymptomsScreen}>
                        <View style={{ flex: 1, backgroundColor: 'white' }}>

                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('../../assets/images/img/icon_pull_handle.png')} style={{ width: 90, height: 5, marginTop: 10 }} />
                            </View>
                            <ScrollView
                                onScroll={this.handleScroll.bind(this)}
                            >
                                <TouchableWithoutFeedback>
                                    <View>
                                        <Text style={{
                                            fontSize: 26,
                                            color: '#6559A0',
                                            fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                            marginLeft: 25,
                                            marginTop: 30
                                        }}>Select Symptoms</Text>
                                        <Text style={{
                                            fontSize: 16,
                                            color: 'black',
                                            marginLeft: 25, marginTop: 20,
                                            fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                            fontWeight: 'normal'
                                        }}>Psychological</Text>
                                        <FlatList
                                            data={this.state.sysPhychology}
                                            renderItem={this.renderPhys}

                                            //Setting the number of column
                                            numColumns={2}
                                            extraData={this.state}

                                            keyExtractor={(item, index) => index}
                                        />
                                        <Text style={{
                                            fontSize: 16,
                                            color: 'black', marginLeft: 25,
                                            marginTop: 20,
                                            fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                            fontWeight: 'normal'
                                        }}>Pain</Text>

                                        <View style={{ flexDirection: 'row' }}>

                                            <FlatList
                                                data={this.state.sysPain}
                                                renderItem={this.renderPain}


                                                //Setting the number of column
                                                numColumns={2}
                                                extraData={this.state}

                                                keyExtractor={(item, index) => index}
                                            />
                                        </View>

                                        <Text style={{
                                            fontSize: 16,
                                            color: 'black',
                                            marginLeft: 25,
                                            marginTop: 20,
                                            fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                            fontWeight: 'normal'
                                        }}>Physical</Text>
                                        <FlatList
                                            data={this.state.sysPhyscial}
                                            renderItem={this.renderPhyscal}

                                            extraData={this.state}

                                            //Setting the number of column
                                            numColumns={2}
                                            keyExtractor={(item, index) => index}
                                        />

                                        <View style={{ marginHorizontal: 24, height: 1, backgroundColor: '#D8D8D8', marginTop: 15 }}>

                                        </View>
                                        <Text style={{ fontSize: 18, color: 'black', marginLeft: 25, marginTop: 10, fontFamily: 'OpenSans' }}>Create your own</Text>

                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <TextInput
                                                value={this.state.customSymp}
                                                onChangeText={(customSymp) => this.onChangeCustomText(customSymp)}
                                                placeholder='Enter a symptom'
                                                style={{ flex: 1, borderWidth: 1, borderRadius: 10, height: 40, marginTop: 10, marginHorizontal: 20, paddingLeft: 10, paddingRight: 40 }}>

                                            </TextInput>
                                        </View>
                                        <RadioForm
                                            style={{ marginTop: 20, flex: 1, marginLeft: 20 }}
                                            ref="radioForm"
                                            radio_props={this.state.types8}
                                            initial={this.state.customInitalIndex}
                                            formHorizontal={true}
                                            labelHorizontal={true}
                                            buttonColor={'#2E7FAE'}
                                            labelColor={'#000'}
                                            animation={true}
                                            buttonSize={5}
                                            buttonOuterSize={15}
                                            labelStyle={{ marginLeft: 5, marginRight: 15 }}
                                            buttonWrapStyle={{ marginTop: 40 }}
                                            onPress={(value, index) => {
                                                this.setState({
                                                    selectedSys: value,
                                                    selectedSysIndex: index
                                                })
                                            }}
                                        />


                                        <TouchableOpacity activeOpacity={!this.state.isCustomButton ? 0.7 : 1} disabled={!this.state.isCustomButton} onPress={() => this.customSymptons()} style={{ marginLeft: 25, borderWidth: 1, borderRadius: 12, height: 40, marginTop: 15, justifyContent: 'center', alignItems: 'center', backgroundColor: !this.state.isCustomButton ? '#D0D1D3' : '#3089CA', width: 160, borderColor: 'transparent' }}>
                                            <Text style={{
                                                color: 'white',
                                                fontWeight: 'bold',
                                                fontSize: 12,
                                                fontFamily: 'OpenSans'
                                            }}>Save custom symptom</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity onPress={() => this._saveSymptomsClicked()}
                                            style={
                                                {
                                                    borderWidth: 1,
                                                    borderRadius: 16,
                                                    height: 40,
                                                    marginTop: 40,
                                                    marginHorizontal: 20,
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                    backgroundColor: '#3089CA',
                                                    marginBottom: 20,
                                                    borderColor: 'transparent'
                                                }}>
                                            <Text style={{
                                                color: 'white',
                                                fontWeight: 'bold',
                                                fontSize: 16,
                                                fontFamily: 'OpenSans'
                                            }}>Save Symptoms</Text>
                                        </TouchableOpacity>
                                    </View>
                                </TouchableWithoutFeedback>






                            </ScrollView>
                        </View>

                    </Modal>
                    : null
                }




{this.state.isReviewModal ?

<Modal
    propagateSwipe={true}
    onSwipeComplete={() => this.setState({ isReviewModal: false })}
    onSwipeThreshold={20}
    swipeDirection="down"
    style={{ margin: 0, marginTop: 60, paddingBottom: 20 }}
    isVisible={this.state.isReviewModal}>
    <View style={{ flex: 1, backgroundColor: 'white' }}>

        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Image source={require('../../assets/images/img/icon_pull_handle.png')} style={{ width: 90, height: 5, marginTop: 10 }} />
        </View>
        <ScrollView
            onScroll={this.handleScroll.bind(this)}
        >
            <TouchableWithoutFeedback>
                <View>
                    <Text style={{
                        fontSize: 26,
                        color: '#6559A0',
                        fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                        marginLeft: 25,
                        marginTop: 30
                    }}>Review this product?</Text>
                    
                    <Text style={{
                        fontSize: 20,
                        color: 'black',
                        marginLeft: 25, 
                        marginTop: 20,
                        fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                        fontWeight: 'bold'
                    }}>{this.state.pickStrainValue}</Text>
                    <View style={{ marginHorizontal: 24, height: 1, backgroundColor: '#D8D8D8', marginTop: 15 }}/>
                  

                  
                    
                    
 <Text style={{ fontSize: 14, color: 'black', marginLeft: 20, marginTop: 10, fontFamily: 'OpenSans' }}>Treating of symptoms</Text>


                            <View style={{ marginHorizontal: 20, width: 90, marginTop: 5 }}>

                                <StarRating

                                    disabled={false}
                                    starSize={23}
                                    maxStars={5}
                                    starStyle={{
                                        marginRight: 5,
                                        color: '#2E7FAE'
                                    }}
                                    rating={this.state.treatRating}
                                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                                />

                            </View>


                            <Text style={{ fontSize: 14, color: 'black', marginLeft: 20, marginTop: 10, fontFamily: 'OpenSans' }}>Quality of Experience </Text>


                            <View style={{ marginHorizontal: 20, width: 90, marginTop: 5 }}>

                                <StarRating

                                    disabled={false}
                                    starSize={23}
                                    maxStars={5}
                                    starStyle={{
                                        marginRight: 5,
                                        color: '#2E7FAE'
                                    }}
                                    rating={this.state.experienceRating}
                                    selectedStar={(rating) => this.onQualityStarRatingPress(rating)}
                                />

                            </View>


                            <Text style={{ fontSize: 14, color: 'black', marginLeft: 20, marginTop: 10, fontFamily: 'OpenSans'  }}>Flavor and appearance</Text>


                            <View style={{ marginHorizontal: 20, width: 90, marginTop: 5, marginBottom: 10 }}>

                                <StarRating

                                    disabled={false}
                                    starSize={23}
                                    maxStars={5}
                                    starStyle={{
                                        marginRight: 5,
                                        color: '#2E7FAE'
                                    }}
                                    rating={this.state.physicalRating}
                                    selectedStar={(rating) => this.onFlovourStarRatingPress(rating)}
                                />
</View>

                            <Text style={{ fontSize: 18, color: 'black', marginLeft: 20, marginTop: 30, fontFamily: 'OpenSans' }}>Wrtie a reviews</Text>
                            <TextInput
                             placeholder='Leave a review'
                             onChangeText={(text) => this.setState({ reviewText: text })}
                             value={this.state.reviewText}
                             multiline={true} style={{ 
                                 borderRadius: 8, 
                                borderWidth: 1,
                                 height: 90, 
                                marginHorizontal: 20, 
                             marginTop: 10, 
                             paddingLeft: 10 
                             }}>

                            </TextInput>
                    
                            <View style={{ marginHorizontal: 15, paddingHorizontal: 10, justifyContent: 'flex-start', marginTop: 5, height:40}}>
                            <RadioButton2 radioValue={'Share your review with the community?'} isChecked={this.state.isCheckedPublic} pressRadioBtn={() => this.setState({isCheckedPublic:!this.state.isCheckedPublic})} />
                            </View>

                    <TouchableOpacity onPress={() => this.saveReview()}
                        style={
                            {
                                borderWidth: 1,
                                borderRadius: 16,
                                height: 40,
                                marginTop: 40,
                                marginHorizontal: 20,
                                justifyContent: 'center',
                                alignItems: 'center',
                                backgroundColor: '#3089CA',
                                marginBottom: 20,
                                borderColor: 'transparent'
                            }}>
                        <Text style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 16,
                            fontFamily: 'OpenSans'
                        }}>Save Review</Text>
                    </TouchableOpacity>
                </View>
            </TouchableWithoutFeedback>






        </ScrollView>
    </View>

</Modal>
: null
}

<Modal
    isVisible={this.state.isfullImage}
    transparent={true}>
        <SafeAreaView style={{flex:1}}>
                <ImageViewer
                onSwipeDown={() => {
                    this.setState({isfullImage:false});
                  }}
                  style = {{flex:1}}
                  index={this.state.selectedImgIndex}

                  onMove={data => console.log(data)}
                  enableSwipeDown={true}
                imageUrls={this.state.ImageViewer}/>
                </SafeAreaView>
            </Modal>







                <ActionSheet
                    visible={this.state.isActionSheetOpen}
                    selectOptions={this._selectImage}
                    closeActionSheet={() => this.setState({ isActionSheetOpen: false })}
                />


                <BottomPopup message={this.state.ErrMsg} />
                <Loader visible={this.state.loader} />
            </SafeAreaView>

        );
    }


}



const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    },

    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderBottomWidth: .5,
        borderColor: '#000',
        height: 40,
        marginHorizontal: 20,
        marginVertical: 10,
    },

    ImageStyle: {
        padding: 10,
        margin: 5,
        height: 20,
        width: 20,
        resizeMode: 'stretch',
        alignItems: 'center'
    },

});

const createFormData = (photo) => {
    const data = new FormData();
  debugger
    data.append("file_1", {
      name: photo.fileName,
      type:  'image/jpg',
      uri:
        photo.path 
    });
  
   
  
    return data;
  };

const mapStateToProps = state => {
    return {
        editTimeline: state.auth.editTimeline,
        reiewSucess: state.auth.reiewSucess,
        saveSessionResult: state.auth.saveSessionResult,
        typeSpecificList: state.auth.typeSpecificList,
        methodList: state.auth.methodList,
        methodSpecificList: state.auth.methodSpecificList,
        fetchDispensaries: state.auth.fetchDispensaries,
        fetchStrain: state.auth.fetchStrain,
        fetchSymptions: state.auth.fetchSymptions,
        userDetail: state.auth.userDetail,
        status: state.auth.status,
        err_Message: state.auth.err_Message,
        loader: state.auth.loader
    }
}

const mapDispatchToProps = dispatch => {
    return {
        action: bindActionCreators({ 
            postSymptoms, 
            getSymptoms,
             searchStrain, 
            searchDispensaries,
             getMethods, 
            getSpecificMethods, 
            getSpecificType,
            saveSession,
            postImages,
            postReview,
            getGallery,
            ClearAction
        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditLoginSession);