import React, { Component } from 'react';
import {
    StyleSheet,
    Image,
    View,
    Text,
    TextInput,
    Dimensions,
    TouchableOpacity,
    ImageBackground,
    SafeAreaView,
    Platform
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { styles } from './styles';
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";
import Loader from '../../common/loader';

// components
import Button from '../../common/button';
import BottomPopup from '../../common/bottomPopup';

// redux Action 
import { ForgotPass, ClearAction } from "../../actions/authAction";
function wp (percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const slideHeight = viewportHeight * 0.36;
const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);

 const sliderWidth = viewportWidth;
 const itemWidth = slideWidth + itemHorizontalMargin * 2;
class NewPassword extends Component {
    state = {
        email: '',
        message: '',
        loader: false
    }

    _resetPassword = () => {
        this._resetPasswordAPI()
    }

   

    render() {
        return (
            <ImageBackground style={{ height: '100%', width: '100%' }} resizeMode='cover' source={require('../../assets/images/img/background.png')}>

            <SafeAreaView style={{ flex: 1 }}>
            <View style={{ overflow: 'visible',marginTop:60,backgroundColor:'white',width:viewportWidth-40,borderRadius:10,alignSelf:'center'}}>

     <Text style = {{
         fontSize:34,
        color:'#6559A0',
        margin:20,
        marginRight:40,
        fontFamily: Platform.OS == "ios" ? 'Merriweather':'Merriweather-Light'
        }}>Choose a new password</Text>
                <Text style = {{fontSize:24,color:'black',marginLeft:20}}>Enter your Email Address</Text>
                <View style = {{flexDirection:'row',alignItems:'center'}}>
            <TextInput secureTextEntry  style = {{flex:1,borderWidth:1,borderRadius:10,height:40,marginTop:10,marginHorizontal:20,paddingLeft:10,paddingRight:40}}>
              
                </TextInput>
                            <Image source = {require('../../assets/images/img/icon_eye_black.png')} style = {{alignSelf:'center',height:20,width:20,position:'absolute',right:20,marginRight:10}}></Image>
                            </View>
                <Text style = {{fontSize:14,color:'black',marginHorizontal:20,fontWeight:'600',marginTop:5}}>Make it secure,it protect your stuff</Text>
                <TouchableOpacity onPress = {()=>Actions.forgotPasswordSent()} style = {{borderWidth:1,borderRadius:10,height:44,marginTop:40,marginHorizontal:20,justifyContent:'center',alignItems:'center',backgroundColor:'#3089CA',marginTop:20,marginBottom:20}}>
                    <Text style = {{color:'white',fontWeight:'600',fontSize:16}}> Send password reset</Text>
                </TouchableOpacity>                   
                    {/* <BottomPopup message={this.state.message} /> */}
                    {/* <Loader visible={this.state.loader} /> */}
                    </View>
            </SafeAreaView>
            </ImageBackground>
        );
    }
}

const mapStateToProps = state => {
    return {
        status: state.auth.status,
        err_Message: state.auth.err_Message,
        loader: state.auth.loader
    }
}

const mapDispatchToProps = dispatch => {
    return {
        action: bindActionCreators({ ForgotPass, ClearAction }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewPassword);