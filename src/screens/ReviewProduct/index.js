import React, { Component } from 'react';
import {
    Image,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    SafeAreaView,
    Platform,
    ScrollView,
    StyleSheet,
    ImageBackground,
    Dimensions,
    FlatList,

} from 'react-native';
import { Actions } from 'react-native-router-flux';
// import  styles  from './styles';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";

import Modal from "react-native-modal";
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import { reviewDetails, ClearAction } from "../../actions/authAction";
import StarRating from 'react-native-star-rating';

// components
import Button from '../../common/button';
import BottomPopup from '../../common/bottomPopup';
import Loader from '../../common/loader';

// redux Action 
function wp(percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const slideHeight = viewportHeight * 0.36;
const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);

const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;


class ReviewProduct extends Component {
    state = { // initilize state
        email: '',
        ErrMsg: '',
        password: '',
        deviceToken: '',
        loader: false,
        isSecure: true,
        types1: [{ label: 'Feture', value: 0 }, { label: 'By rating', value: 1 }, { label: 'X-Z', value: 3 }],
        types2: [{ label: 'Depression', value: 0 }],
        types3: [{ label: 'Headache', value: 0 }, { label: 'Joint', value: 1 }],
        types4: [{ label: 'Muscle', value: 0 }, { label: 'Nerve', value: 1 }],
        types5: [{ label: 'Injury', value: 0 }, { label: 'Cramping', value: 1 }],
        types6: [{ label: 'Seizure', value: 0 }, { label: 'Muscle Spasms', value: 1 }],
        types7: [{ label: 'Tremors', value: 0 }, { label: 'Vertigo', value: 1 }],
        types8: [{ label: 'Psychological', value: 0 }, { label: 'pain', value: 1 }, { label: 'Physical', value: 3 }],
        types9: [{ label: 'Bong', value: 0 }, { label: 'Joint', value: 1 }, { label: 'Pipe', value: 3 }],
        reviewList: [],
        reviewImg: [],
        users:
            [{
                "name": "Proxima Midnight",
                "email": "proxima@appdividend.com"
            },
            {
                "name": "Proxima Midnight",
                "email": "proxima@appdividend.com"
            },
            {
                "name": "Proxima Midnight",
                "email": "proxima@appdividend.com"
            },

            ]

    }

    componentDidMount() {
        let uri = this.props.postData['@id']
        debugger
        let filterUri = uri.substring(1)
        this.props.action.reviewDetails(this.props.userDetail, filterUri)
    }



    componentWillUnmount() {
        this.clearReducerState();
    }
    clearReducerState = () => { // reset initial state.
        this.props.action.ClearAction()
    }
    async componentWillReceiveProps(nextProps) {
        debugger
        if (Actions.currentScene == "ReviewProduct") await this.setState({ loader: nextProps.loader })
        if (nextProps.status === '2333') {
            debugger
            this.setState({ reviewList: nextProps.reviewDetails, reviewImg: nextProps.reviewDetails.related_session.galleries })
            debugger
        }
        else if (nextProps.status === '4543') {

            debugger
        }

    }


    onCheckInvision() {
        setTimeout(() => {
            this.setState({ isSecure: !this.state.isSecure })
        }, 200);
    }
    renderMactItem = ({ item, index }) => {
        return (
            <TouchableOpacity style={{ marginHorizontal: 25, marginTop: 20 }}>
                <Image style={{ height: 140, width: viewportWidth - 50, borderRadius: 10 }} resizeMode='cover' source={{ uri: item.originalUrl }} />
            </TouchableOpacity>

        )
    }

    renderReview = ({ item, index }) => {
        return (
            <View style={{ marginHorizontal: 15, marginTop: 10, paddingBottom: 10 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ fontSize: 16, fontWeight: 'bold', color: 'black', fontFamily: 'OpenSans' }}>{item.symptom.name}</Text>
                    <Image source={require('../../assets/images/img/icon-ellipse.png')} style={styles.ImageStyle} />

                </View>
                <View style={{ width: 90, marginTop: 5, marginBottom: 10, marginLeft: 5 }}>

                    <StarRating

                        disabled={true}
                        starSize={18}
                        maxStars={5}
                        starStyle={{ marginRight: 5, color: '#2E7FAE' }}
                        rating={0}
                    />

                </View>

                <Text style={{ fontSize: 14, fontWeight: '400', color: 'black', fontFamily: 'OpenSans', marginLeft: 5 }}>
                    {this.props.postData.text}
                </Text>


            </View>

        )
    }

    async onSwipeDown() {
        await this.setState({ isModalVisibleSymptomsScreen: false, isModalVisibleSymptomsScreen: false })
    }


    render() {
        let post = this.props.postData
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#2C7DB3' }}>
                <View style={{ flex: 1 }}>
                    <View style={{
                        flex: Platform.OS == 'ios' ? 0.07 : 0.1,
                        width: '100%',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        backgroundColor: 'white'
                    }}>

                        <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'center' }}
                            onPress={() => Actions.pop()}>

                            <Image resizeMode='contain' style={{ height: 20, width: 20, marginLeft: 25 }} source={require('../../assets/images/img/icon_arrow_left.png')} />
                            <Text style={{ color: '#2D7DAE', textAlign: 'auto', marginLeft: 5, fontFamily: 'OpenSans', fontWeight: 'bold' }}>Product Reviews</Text>
                        </TouchableOpacity>

                    </View>
                    <View style={{ flex: 1, backgroundColor: 'white' }}>
                        <Text style={{
                            fontSize: 24,
                            color: '#6559A0',
                            marginHorizontal: 25,
                            marginTop: 13,
                            ontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                        }}>{this.props.postData.strain.name}</Text>

                        <ScrollView style={{ flex: 1 }}>

                            <FlatList
                                style={{ flex: 1, backgroundColor: 'white' }}
                                data={this.state.reviewImg}
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                showsVerticalScrollIndicator={false}
                                renderItem={this.renderMactItem
                                }
                                keyExtractor={item => item.email}

                            />
                            <View style={{ flexDirection: 'row', marginTop: 15 }}>
                                <View style={{ flex: 1 }}>
                                    <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'black', fontFamily: 'OpenSans', marginLeft: 20, }}>Overall Rating:</Text>
                                    <View style={{ marginHorizontal: 20, width: 90, marginTop: 5 }}>

                                        <StarRating

                                            disabled={true}
                                            starSize={18}
                                            maxStars={5}
                                            starStyle={{ marginRight: 5, color: '#2E7FAE' }}
                                            rating={this.props.postData.overall_rating}
                                        />

                                    </View>
                                    <Text style={{ fontSize: 12, fontWeight: '400', color: 'black', fontFamily: 'OpenSans', marginLeft: 20, marginTop: 10 }}>Treating your symptions</Text>
                                    <View style={{ marginHorizontal: 20, width: 90, marginTop: 5 }}>

                                        <StarRating

                                            disabled={true}
                                            starSize={18}
                                            maxStars={5}
                                            starStyle={{ marginRight: 5, color: '#2E7FAE' }}
                                            rating={this.props.postData.overall_rating}
                                        />

                                    </View>

                                    <Text style={{ fontSize: 12, fontWeight: '400', color: 'black', fontFamily: 'OpenSans', marginLeft: 20, marginTop: 10 }}>Treating your symptions</Text>
                                    <View style={{ marginHorizontal: 20, width: 90, marginTop: 5 }}>

                                        <StarRating

                                            disabled={true}
                                            starSize={18}
                                            maxStars={5}
                                            starStyle={{ marginRight: 5, color: '#2E7FAE' }}
                                            rating={this.props.postData.overall_rating}
                                        />

                                    </View>

                                    <Text style={{ fontSize: 12, fontWeight: '400', color: 'black', fontFamily: 'OpenSans', marginLeft: 20, marginTop: 10 }}>Treating your symptions</Text>
                                    <View style={{ marginHorizontal: 20, width: 90, marginTop: 5 }}>

                                        <StarRating

                                            disabled={true}
                                            starSize={18}
                                            maxStars={5}
                                            starStyle={{ marginRight: 5, color: '#2E7FAE' }}
                                            rating={this.props.postData.overall_rating}
                                        />

                                    </View>


                                </View>


                                <View style={{ flex: 1, }}>
                                    <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'black', fontFamily: 'OpenSans', }}>Popular for</Text>
                                    {this.props.postData.review_symptoms.map((data) => {
                                        return (
                                            <Text style={{ fontSize: 14, fontWeight: '400', color: 'black', fontFamily: 'OpenSans' }}>{data.symptom.name}</Text>
                                        )
                                    })}



                                    <Text style={{ fontSize: 14, fontWeight: '800', color: 'black', fontFamily: 'OpenSans', marginTop: 10 }}>Populartity</Text>
                                    <Text style={{ fontSize: 14, fontWeight: '400', color: 'black', fontFamily: 'OpenSans', marginTop: 5 }}>0 meters</Text>

                                </View>
                            </View>
                            <Text style={{
                                fontSize: 20,
                                color: '#6559A0',
                                marginHorizontal: 15,
                                marginTop: 30,
                                fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                            }}> Reviews</Text>
                            <View style={{ height: 1, marginHorizontal: 20, borderWidth: 0.5, borderColor: 'gray', marginTop: 5 }}></View>
                            <FlatList
                                style={{ flex: 1, marginBottom: 15 }}
                                data={this.state.reviewList}
                                showsVerticalScrollIndicator={true}
                                renderItem={this.renderReview
                                }
                                extraData={this.state}

                            />
                        </ScrollView>
                    </View>
                </View>

                {this.state.isModalVisibleSymptomsScreen ?
                    <GestureRecognizer
                        onSwipeDown={(state) => this.onSwipeDown(state)}

                        config={config}
                        style={{
                            backgroundColor: 'red'
                        }}
                    >
                        <Modal
                            style={{ margin: 0, marginTop: 60 }}
                            isVisible={this.state.isModalVisibleSymptomsScreen}>
                            <View style={{ flex: 1, backgroundColor: 'white' }}>
                                <ScrollView
                                    onScroll={this.handleScroll.bind(this)}
                                    contentContainerStyle={{ flexGrow: 1 }}
                                    style={{ flex: 1, }}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Image source={require('../../assets/images/img/icon_pull_handle.png')} style={{ width: 90, height: 5, marginTop: 10 }} />

                                    </View>

                                    <Text style={{
                                        fontSize: 26,
                                        color: '#6559A0',
                                        fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                        marginLeft: 25,
                                        marginTop: 30
                                    }}>Filter and sort strains</Text>
                                    <Text style={{
                                        fontSize: 18,
                                        color: 'black',
                                        marginLeft: 25,
                                        marginTop: 30,
                                        fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                        fontWeight: '800'
                                    }}>Sort by</Text>
                                    <FlatList
                                        data={this.state.isModalVisibleSymptomsScreen}
                                        renderItem={this.renderPhys}

                                        //Setting the number of column
                                        numColumns={2}
                                        extraData={this.state}

                                        keyExtractor={(item, index) => index}
                                    />
                                    <Text style={{
                                        fontSize: 16,
                                        color: 'black',
                                        marginLeft: 25,
                                        marginTop: 20,
                                        fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                        fontWeight: 'normal'
                                    }}>Pain</Text>

                                    <View style={{ flexDirection: 'row' }}>

                                        <FlatList
                                            data={this.state.sysPain}
                                            renderItem={this.renderPain}


                                            //Setting the number of column
                                            numColumns={2}
                                            extraData={this.state}

                                            keyExtractor={(item, index) => index}
                                        />
                                    </View>

                                    <Text style={{
                                        fontSize: 16,
                                        color: 'black',
                                        marginLeft: 25,
                                        marginTop: 20,
                                        fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                        fontWeight: 'normal'
                                    }}>Physical</Text>
                                    <FlatList
                                        data={this.state.sysPhyscial}
                                        renderItem={this.renderPhyscal}

                                        extraData={this.state}

                                        //Setting the number of column
                                        numColumns={2}
                                        keyExtractor={(item, index) => index}
                                    />

                                    <View style={{ marginHorizontal: 24, height: 1, backgroundColor: '#D8D8D8', marginTop: 15 }}>

                                    </View>
                                    <Text style={{ fontSize: 18, color: 'black', marginLeft: 25, marginTop: 10, fontFamily: 'OpenSans' }}>Create your own</Text>

                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <TextInput
                                            value={this.state.customSymp}
                                            onChangeText={(customSymp) => this.onChangeCustomText(customSymp)}
                                            placeholder='Enter a symptom'
                                            style={{ flex: 1, borderWidth: 1, borderRadius: 10, height: 40, marginTop: 10, marginHorizontal: 20, paddingLeft: 10, paddingRight: 40 }}>

                                        </TextInput>
                                    </View>
                                    <RadioForm
                                        style={{ marginTop: 20, flex: 1, marginLeft: 20 }}
                                        ref="radioForm"
                                        radio_props={this.state.types8}
                                        initial={this.state.customInitalIndex}
                                        formHorizontal={true}
                                        labelHorizontal={true}
                                        buttonColor={'#2E7FAE'}
                                        labelColor={'#000'}
                                        animation={true}
                                        buttonSize={5}
                                        buttonOuterSize={15}
                                        labelStyle={{ marginLeft: 5, marginRight: 15 }}
                                        buttonWrapStyle={{ marginTop: 40 }}
                                        onPress={(value, index) => {
                                            this.setState({
                                                selectedSys: value,
                                                selectedSysIndex: index
                                            })
                                        }}
                                    />


                                    <TouchableOpacity activeOpacity={!this.state.isCustomButton ? 0.7 : 1} disabled={!this.state.isCustomButton} onPress={() => this.customSymptons()} style={{ marginLeft: 25, borderWidth: 1, borderRadius: 12, height: 40, marginTop: 15, justifyContent: 'center', alignItems: 'center', backgroundColor: !this.state.isCustomButton ? '#D0D1D3' : '#3089CA', width: 160, borderColor: 'transparent' }}>
                                        <Text style={{ color: 'white', fontWeight: '600', fontSize: 12, fontFamily: 'OpenSans' }}>Save custom Symptom</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => this.setState({ isModalVisibleSymptomsScreen: false, isRating: true })} style={{ borderWidth: 1, borderRadius: 16, height: 40, marginTop: 40, marginHorizontal: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: '#3089CA', marginBottom: 20, borderColor: 'transparent' }}>
                                        <Text style={{ color: 'white', fontWeight: '600', fontSize: 16, fontFamily: 'OpenSans' }}>Save Symptions</Text>
                                    </TouchableOpacity>

                                </ScrollView>
                            </View>

                        </Modal>
                    </GestureRecognizer> : null
                }
                <BottomPopup message={this.state.ErrMsg} />
                <Loader visible={this.state.loader} />
            </SafeAreaView>


        );
    }


}



const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    },

    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderBottomWidth: .5,
        borderColor: '#000',
        height: 40,
        marginHorizontal: 20,
        marginVertical: 10,
    },

    ImageStyle: {
        padding: 5,
        marginVertical: 5,
        height: 13,
        width: 13,
        resizeMode: 'stretch',
        alignItems: 'center'
    },

});

const mapStateToProps = state => {
    return {
        reviewDetails: state.auth.reviewDetails,
        userDetail: state.auth.userDetail,
        status: state.auth.status,
        err_Message: state.auth.err_Message,
        loader: state.auth.loader
    }
}

const mapDispatchToProps = dispatch => {
    return {
        action: bindActionCreators({ reviewDetails, ClearAction }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReviewProduct);