import { Dimensions } from 'react-native';

export const styles = {
	backgroundImg: {
		flex:1
	},
	img_container: {
		flex: 1,
		backgroundColor: 'white'
	},
	splash: {
		height: Dimensions.get('window').height,
		width: Dimensions.get('window').width
	},
	loginButtonView: {
		marginTop:'10.35%',
		justifyContent: 'center',
		alignItems: 'center',
		width: '80%',
		height: 60,
		borderRadius: 1
	},
	buttonLogin: {
		backgroundColor: 'white',
		width: '100%',
		padding: 13,
		borderRadius: 30,
		alignItems: 'center',
		justifyContent: 'center'
	},
	loginText: {
		color: '#6559A0',
		fontWeight: 'bold',
		fontSize: 16,
		fontFamily: 'OpenSans',
	},
	logoView: {
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: 57,
		marginBottom:'5.28%'
	},

	SignUpButtonView: {
		marginTop: '2.28%',
		justifyContent: 'center',
		alignItems: 'center',
		width: '80%',
		height: 60,
		borderRadius: 1,
	},
	buttonSignUp: {
		backgroundColor: 'white',
		width: '100%',
		padding: 13,
		borderRadius: 30,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: 'transparent',
		borderWidth: 1,
		borderColor: 'white',


	},
	signupText: {
		color: 'white',
		fontWeight: 'bold',
		fontSize: 16,
		fontFamily: 'OpenSans',

	},
	swipeView: {
		marginTop:'12.67%',
		marginBottom:'10.35%',
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',

	},
	swipeText: {
		color: 'white',
		textAlign: 'center',
		fontFamily: 'OpenSans',
		fontSize:12

	},
	imgLogo: {
	
	},
	arrowButton: {
		alignSelf: 'center',
		height: 13,
		width: 13,
		marginLeft: '0.52%',
		opacity: 0.5
	},
	signInView: {
		position: 'absolute',
		bottom: 0,
		marginBottom: 40,
		width: '100%',
		alignItems: 'center'
	}

};

export default styles;