import React, { Component } from 'react';
import {
    Image,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    SafeAreaView,
    Platform,
    AsyncStorage,
    ImageBackground
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { styles } from './styles';

// components
import Button from '../../common/button';
import BottomPopup from '../../common/bottomPopup';
import Loader from '../../common/loader';

import SplashScreen from 'react-native-splash-screen'
import { getToken, getUserData } from '../../services/utils';

const imgBackground = require('../../assets/images/img/background.png')
const imgLogo = require('../../assets/images/img/Logo.png')
const arrowButton = require('../../assets/images/img/icon_chevron_right.png')

import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { setUserDetails } from "../../actions/authAction";
import GestureRecognizer, { swipeDirections } from 'react-native-swipe-gestures';

class WelcomeScreen extends Component {
    state = { // initilize state
        email: '',
        ErrMsg: '',
        password: '',
        deviceToken: '',
        loader: false,
    }

    componentDidMount() {
        this.getSession();


    }

    async componentWillReceiveProps(nextProps) {
        debugger
        if (Actions.currentScene == "welcome") {
            await this.setState({ loader: nextProps.loader })
            if (nextProps.status === '5001') {

            }
            else if (nextProps.status === '2000') {
                debugger
                Actions.reset('Experince_Timeline')
                debugger
            }

            else if (nextProps.status === '401') {
                debugger
                setTimeout(() => {
                    this.setState({ ErrMsg: '', })
                }, 2000);
                this.setState({ ErrMsg: nextProps.err_Message, })
            }
        }
    }
    getSession = async () => {
        getUserData().then(response => {
            console.log("getToken()==>", response)
            if (response) {

                let data = JSON.parse(response)

                this.props.action.setUserDetails(data)

            }
            else {
                setTimeout(() => {
                    SplashScreen.hide();
                }, 3000);
            }
        })

    }


    async onSwipeLeft(gestureName) {
        debugger
        Actions.loginscreen()

    }

    onSwipe(gestureName, gestureState) {
        debugger
        Actions.loginscreen()
      }



    render() {
        const config = {
            velocityThreshold: 0.3,
            directionalOffsetThreshold: 200
        };
        return (
            <ImageBackground style={styles.backgroundImg} resizeMode='cover' source={imgBackground}>
                <GestureRecognizer
                    onSwipeLeft={(state) => this.onSwipeLeft(state)}
                    onSwipe={(direction, state) => this.onSwipe(direction, state)}
                    config={config}
                    style={{
                        flex: 1,
                    }}
                >

                    <View style={{ flex: 1 }}>
                        <SafeAreaView style={{ flex: 1 }}>
                            <View style={styles.logoView}>
                                <Image style={{
                                    height: 140,
                                    width: 140
                                }} resizeMode='contain' source={imgLogo} />
                            </View>
                            <Text style={{
                                fontFamily: Platform.OS == "ios" ? 'Merriweather' : 'Merriweather-Light',
                                fontSize: 24,
                                color: 'white',
                                textAlign: 'center',
                            }}>Tetragram</Text>
                            <View style={{ flex: 1, alignItems: 'center' }}>
                                <TouchableOpacity onPress={() => Actions.loginscreen()}>
                                    <View style={styles.swipeView}>
                                        <Text style={styles.swipeText}>
                                            Swipe to learn more
                    </Text>
                                        <Image style={styles.arrowButton} source={arrowButton} />
                                    </View>
                                </TouchableOpacity>
                                <View style={styles.signInView}>
                                    <View style={[styles.loginButtonView]}>
                                        <TouchableOpacity style={styles.buttonLogin} onPress={() => Actions.newLogin()}>
                                            <Text style={styles.loginText}>Sign In </Text>
                                        </TouchableOpacity>
                                    </View>

                                    <View style={[styles.SignUpButtonView]}>
                                        <TouchableOpacity style={styles.buttonSignUp} onPress={() => Actions.loginscreen({ 'PassData': 'CreateAccount' })}>
                                            <Text style={styles.signupText}>Create An Account</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                            <BottomPopup message={this.state.ErrMsg} />
                            <Loader visible={this.state.loader} />
                        </SafeAreaView>
                    </View>
                </GestureRecognizer>

            </ImageBackground>


        );
    }
}

const mapStateToProps = state => {
    return {
        userDetail: state.auth.userDetail,
        status: state.auth.status,
        err_Message: state.auth.err_Message,
        loader: state.auth.loader
    }
}

const mapDispatchToProps = dispatch => {
    return {
        action: bindActionCreators({ setUserDetails }, dispatch)
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(WelcomeScreen);