import APIServices from '../services/apiService';
import { validateEmail, validatePassword, validatName, validatConfirmPass } from '../services/validation';
import { networkStatus } from '../services/networkStatus';
import { NetworkStatus, AcceptenceMsg } from '../assets/default_text';
import { AsyncStorage } from 'react-native'
import {
    SIGNUP_SUCCESS,
    SIGNUP_FAILURE,
    LOGIN_SUCCESS,
    LOGIN_TOKEN,
    LOGIN_FAILURE,
    FORGOT_PASS_SUCCESS,
    FORGOT_PASS_FAILURE,
    CLEAR_STATE,
    ERR_MESSAGE,
    LOADER,
    GET_CUSTOM_INFO_SUCCESS,
    GET_CUSTOM_INFO_FAILURE,
    RESET_PASS_SUCCESS,
    RESET_PASS_FAILURE,
    VERIFY_ACCOUNT_SUCCESS,
    VERIFY_ACCOUNT_FAILURE,
    GET_SYMPTIONS_SUCCESS,
    GET_SYMPTIONS_FAILURE,
    UPDATE_SYMPTIONS_SUCCESS,
    ADD_SYMPTIONS_SUCCESS,
    EDIT_SYMPTIONS_SUCCESS,
    DEL_SYMPTIONS_SUCCESS,
    LOGIN_SUCCESS1,
    SEARCH_STRAIN,
    DEL_SYMPTIONS_FAIL,
    SEARCH_DISPENSARIES,
    FETCH_REVIEW_FAIL,
    FETCH_REVIEW,
    GET_METHOD_LIST,
    GET_SPECIFIC_METHOD_LIST,
    GET_SPECIFIC_TYPE_LIST,
    SAVE_SESSION,
    GET_SESSION,
    DEL_SESSION,
    SAVE_Image,
    SAVE_SESSION_FAIL,
    DEL_SESSION_FAIL,
    REVIEW_POST,
    REVIEW_POST_FAIL,
    FETCH_REVIEW_DETAILS,
    FETCH_REVIEW_DETAILS_FAIL,
    GET_EDIT_TIMELINE_FAIL,
    GET_EDIT_TIMELINE

} from './type';
import { setToken,setUserData } from '../services/utils';
// Signin api functionality    

export const setUserDetails = (postData) => {
    return dispatch => {
        debugger
        dispatch({ type: LOGIN_SUCCESS1, payload: postData })

    }
}






export const SignIn = (data) => {
    return dispatch => {
        // if (!validateEmail(data.username).status) dispatch({ type: ERR_MESSAGE, payload: validateEmail(data.username).message })
        // // else if (!validatePassword(data.password).status) dispatch({ type: ERR_MESSAGE, payload: validatePassword(data.password).message })
        // else {
            //dispatch({ type: LOGIN_SUCCESS, payload: "" })
            debugger
            networkStatus()
                .then(isConnected => {
                    if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                    else {
                        dispatch({ type: CLEAR_STATE })
                        dispatch({ type: LOADER, payload: true })
                        APIServices(data, 'login', 'POST',{},'multipart/form-data')
                            .then(responseJson => {
                                console.log("test===>", responseJson)
                                debugger
                                if (responseJson.status === 200) {
                                    debugger
                                    dispatch({ type: LOGIN_SUCCESS, payload: responseJson.data })
                                    dispatch({ type: LOGIN_TOKEN, payload: responseJson.data })
                                    setUserData (JSON.stringify(responseJson.data))

                                }
                                else if (responseJson.status === 202) dispatch({ type: LOGIN_FAILURE, payload: responseJson.data })
                                else if (responseJson.status === 401 || responseJson.data.status == 1002) dispatch({ type: LOGIN_FAILURE, payload: responseJson.data.message })
                            })
                    }
                })
        }
}



// signup api functionality
export const SignUp = (data) => {
    debugger
    let authorization =''
    return dispatch => {
         if (!validateEmail(data.username).status) dispatch({ type: ERR_MESSAGE, payload: validateEmail(data.username).message })
        else if (!validatePassword(data.password).status) dispatch({ type: ERR_MESSAGE, payload: validatePassword(data.password).message })
        else {
            networkStatus()
                .then(isConnected => {
                    if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                    else {
                        dispatch({ type: CLEAR_STATE })
                        dispatch({ type: LOADER, payload: true })
                        APIServices(JSON.stringify(data), 'api/consumers', 'POST',authorization)
                            .then(responseJson => {
                               debugger
                               
                                console.log("responseJson==>", JSON.stringify(responseJson))
                                if (responseJson.status === 201) dispatch({ type: SIGNUP_SUCCESS, payload: responseJson.data.message })
                                else if (responseJson.status === 400) dispatch({ type: SIGNUP_FAILURE, payload: responseJson.data.detail ? responseJson.data.detail : 'Something went wrong.' })
                                else if (responseJson.status === 401) dispatch({ type: SIGNUP_FAILURE, payload: responseJson.data.detail ? responseJson.data.detail : 'Something went wrong.' })
                            })
                    }
                })
        }
    }
}

// forgot password api functionality
export const ForgotPass = (data) => {
    return dispatch => {
        // if (!validateEmail(data.email).status) dispatch({ type: ERR_MESSAGE, payload: validateEmail(data.email).message })
        // else {
            networkStatus()
                .then(isConnected => {
                    if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                    else {
                        dispatch({ type: CLEAR_STATE })
                        dispatch({ type: LOADER, payload: true })
                        APIServices(data, 'forgot_password', 'POST')
                            .then(responseJson => {
                                debugger
                                console.log("responseJson==>", JSON.stringify(responseJson))
                                if (responseJson.status === 200 || responseJson.status === 202) dispatch({ type: FORGOT_PASS_SUCCESS, payload: responseJson.data })
                                // else if (responseJson.data.status_code === 202) dispatch({ type: FORGOT_PASS_SUCCESS, payload: responseJson.data })
                                else if (responseJson.status === 401) dispatch({ type: FORGOT_PASS_FAILURE, payload: responseJson.data.message ? responseJson.data.message : 'Something went wrong.' })
                                else if (responseJson.status === 422) dispatch({ type: FORGOT_PASS_FAILURE, payload: responseJson.data.message ? responseJson.data.message: 'Something went wrong.' })
                            })
                    }
                })
        // }
    }
}

// ResetPassword api functionality    
export const getProfileDetails = (data) => {
    debugger
    return dispatch => {
        networkStatus()
            .then(isConnected => {
                if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                else {
                    dispatch({ type: CLEAR_STATE })
                    dispatch({ type: LOADER, payload: true })
                    debugger
                    APIServices({}, 'api/consumers/'+data.user, 'GET',data.token)
                        .then(responseJson => {
                            console.log("test===>", responseJson)
                            if (responseJson.status === 200) {
                                debugger
                                dispatch({ type: GET_CUSTOM_INFO_SUCCESS, payload: responseJson.data })
                            }
                            else if (responseJson.status === 401) dispatch({ type: GET_CUSTOM_INFO_FAILURE, payload: responseJson.data.message })
                        })
                }
            })

    }
}


export const PutProfileDetails = (postData,data) => {
    debugger
    return dispatch => {
        networkStatus()
            .then(isConnected => {
                if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                else {
                    dispatch({ type: CLEAR_STATE })
                    dispatch({ type: LOADER, payload: true })
                    APIServices(JSON.stringify(postData), 'api/consumers/'+data.user, 'PUT',data.token)
                        .then(responseJson => {
                            console.log("test===>", responseJson)
                            if (responseJson.status === 200) {
                                debugger
                                dispatch({ type: GET_CUSTOM_INFO_SUCCESS, payload: responseJson.data })
                                dispatch({ type: ERR_MESSAGE, payload: '' })
                            }
                            else if (responseJson.status === 401) dispatch({ type: GET_CUSTOM_INFO_FAILURE, payload: responseJson.data.message })
                        })
                }
            })

    }
}






export const editSymptoms = (postData,data,userDetails) => {
    debugger
    return dispatch => {
        networkStatus()
            .then(isConnected => {
                if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                else {
                    dispatch({ type: CLEAR_STATE })
                    dispatch({ type: LOADER, payload: true })
                    APIServices(JSON.stringify(postData), data, 'PUT',userDetails.token)
                        .then(responseJson => {
                            console.log("test===>", responseJson)
                            if (responseJson.status === 200 || responseJson.status === 201) {
                                debugger
                                dispatch({ type: EDIT_SYMPTIONS_SUCCESS, payload: responseJson.data })
                            }
                            else if (responseJson.status === 401) dispatch({ type: GET_CUSTOM_INFO_FAILURE, payload: responseJson.data.message })
                        })
                }
            })

    }
}


export const delSymptoms = (data,userDetails) => {
    debugger
    return dispatch => {
        networkStatus()
            .then(isConnected => {
                if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                else {
                    dispatch({ type: CLEAR_STATE })
                    dispatch({ type: LOADER, payload: true })
                    APIServices({}, data, 'DELETE',userDetails.token)
                        .then(responseJson => {
                            debugger
                            console.log("test===>", responseJson)
                            if (responseJson.status === 200 ||responseJson.status === 204 ) {
                                debugger
                                dispatch({ type: DEL_SYMPTIONS_SUCCESS, payload: responseJson.data })
                            }
                            else if (responseJson.data.status == 500 || responseJson.data.status == 1002){
                                debugger
                                dispatch({ type: DEL_SYMPTIONS_FAIL, payload: responseJson.data.message })
                            } 
                        })
                }
            })

    }
}


export const getSymptoms = (api,data) => {
    debugger
    return dispatch => {
        networkStatus()
            .then(isConnected => {
                if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                else {
                    dispatch({ type: CLEAR_STATE })
                    dispatch({ type: LOADER, payload: true })
                    APIServices({}, api, 'GET',data.token)
                        .then(responseJson => {
                            debugger
                            console.log("test===>", responseJson)
                            if (responseJson.status === 200) {
                                debugger
                                dispatch({ type: GET_SYMPTIONS_SUCCESS, payload: responseJson.data })
                            }
                            else if (responseJson.status === 401) dispatch({ type: GET_SYMPTIONS_FAILURE, payload: responseJson.data.message })
                        })
                }
            })

    }
}


export const postSymptoms = (postData,userDetails) => {
    debugger
    return dispatch => {
        networkStatus()
            .then(isConnected => {
                if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                else {
                    dispatch({ type: CLEAR_STATE })
                  
                    APIServices(JSON.stringify(postData), 'api/symptoms', 'POST',userDetails.token)
                        .then(responseJson => {
                            debugger
                            console.log("test===>", responseJson)
                            if (responseJson.status === 201 || responseJson.status === 200) {
                                debugger
                                dispatch({ type: ADD_SYMPTIONS_SUCCESS, payload: 'Symptoms added sucessfully' })

                            }
                            else if (responseJson.status === 401) dispatch({ type: GET_CUSTOM_INFO_FAILURE, payload: responseJson.data.message })
                        })
                }
            })

    }
}



//Search strain api

export const searchStrain = (postData,userDetails) => {
    debugger
    return dispatch => {
        networkStatus()
            .then(isConnected => {
                if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                else {
                    dispatch({ type: CLEAR_STATE })
                    dispatch({ type: LOADER, payload: true })
                    APIServices({}, 'api/strains?name'+postData, 'GET',userDetails.token)
                        .then(responseJson => {
                            debugger
                            console.log("test===>", responseJson)
                            if (responseJson.status === 201 || responseJson.status === 200) {
                                debugger
                                dispatch({ type: SEARCH_STRAIN, payload: responseJson.data })

                            }
                            else if (responseJson.status === 401) dispatch({ type: GET_CUSTOM_INFO_FAILURE, payload: responseJson.data.message })
                        })
                }
            })

    }
}



export const searchDispensaries = (postData,userDetails) => {
    debugger
    return dispatch => {
        networkStatus()
            .then(isConnected => {
                if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                else {
                    dispatch({ type: CLEAR_STATE })
                    APIServices({}, 'api/dispensaries?name'+postData+'&dba='+postData, 'GET',userDetails.token)
                        .then(responseJson => {
                            debugger
                            console.log("test===>", responseJson)
                            if (responseJson.status === 201 || responseJson.status === 200) {
                                debugger
                                dispatch({ type: SEARCH_DISPENSARIES, payload: responseJson.data })

                            }
                            else if (responseJson.status === 401) dispatch({ type: GET_CUSTOM_INFO_FAILURE, payload: responseJson.data.message })
                        })
                }
            })

    }
}



//methods



export const getMethods = (userDetails) => {
    debugger
    return dispatch => {
        networkStatus()
            .then(isConnected => {
                if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                else {
                    dispatch({ type: CLEAR_STATE })
                    APIServices({}, 'api/methods', 'GET',userDetails.token)
                        .then(responseJson => {
                            debugger
                            console.log("test===>", responseJson)
                            if (responseJson.status === 201 || responseJson.status === 200) {
                                debugger
                                dispatch({ type: GET_METHOD_LIST, payload: responseJson.data })

                            }
                            else if (responseJson.status === 401) dispatch({ type: GET_CUSTOM_INFO_FAILURE, payload: responseJson.data.message })
                        })
                }
            })

    }
}



//Specific method


export const getSpecificMethods = (userDetails) => {
    debugger
    return dispatch => {
        networkStatus()
            .then(isConnected => {
                if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                else {
                    dispatch({ type: CLEAR_STATE })
                    APIServices({}, 'api/specific_methods', 'GET',userDetails.token)
                        .then(responseJson => {
                            debugger
                            console.log("test===>", responseJson)
                            if (responseJson.status === 201 || responseJson.status === 200) {
                                debugger
                                dispatch({ type: GET_SPECIFIC_METHOD_LIST, payload: responseJson.data })

                            }
                            else if (responseJson.status === 401) dispatch({ type: GET_CUSTOM_INFO_FAILURE, payload: responseJson.data.message })
                        })
                }
            })

    }
}



export const getSpecificType = (userDetails) => {
    debugger
    return dispatch => {
        networkStatus()
            .then(isConnected => {
                if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                else {
                    dispatch({ type: CLEAR_STATE })
                    APIServices({}, 'api/specific_types', 'GET',userDetails.token)
                        .then(responseJson => {
                            debugger
                            console.log("test===>", responseJson)
                            if (responseJson.status === 201 || responseJson.status === 200) {
                                debugger
                                dispatch({ type: GET_SPECIFIC_TYPE_LIST, payload: responseJson.data })

                            }
                            else if (responseJson.status === 401) dispatch({ type: GET_CUSTOM_INFO_FAILURE, payload: responseJson.data.message })
                        })
                }
            })

    }
}


// Review ........................

export const ratingReview = (userDetails,api) => {
    debugger
    return dispatch => {
        networkStatus()
            .then(isConnected => {
                if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                else {
                    dispatch({ type: CLEAR_STATE })
                    dispatch({ type: LOADER, payload: true })
                    APIServices({}, api, 'GET',userDetails.token)
                        .then(responseJson => {
                            debugger
                            console.log("test===>", responseJson)
                            if (responseJson.status === 201 || responseJson.status === 200) {
                                debugger
                                dispatch({ type: FETCH_REVIEW, payload: responseJson.data})

                            }
                            else if (responseJson.status === 401) dispatch({ type: FETCH_REVIEW_FAIL, payload: responseJson.data.message })
                        })
                }
            })

    }
}


export const reviewDetails = (userDetails,api) => {
    debugger
    return dispatch => {
        networkStatus()
            .then(isConnected => {
                if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                else {
                    dispatch({ type: CLEAR_STATE })
                    dispatch({ type: LOADER, payload: true })
                    APIServices({}, api, 'GET',userDetails.token)
                        .then(responseJson => {
                            debugger
                            console.log("test===>", responseJson)
                            if (responseJson.status === 201 || responseJson.status === 200) {
                                debugger
                                dispatch({ type: FETCH_REVIEW_DETAILS, payload: responseJson.data})

                            }
                            else if (responseJson.status === 401) dispatch({ type: FETCH_REVIEW_DETAILS_FAIL, payload: responseJson.data.message })
                        })
                }
            })

    }
}


export const saveSession = (postData,userDetails,method) => {
    debugger
    return dispatch => {
        networkStatus()
            .then(isConnected => {
                if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                else {
                    dispatch({ type: CLEAR_STATE })
                    dispatch({ type: LOADER, payload: true })
                    APIServices(JSON.stringify(postData), 'api/sessions', method,userDetails.token)
                        .then(responseJson => {
                            debugger
                            console.log("test===>", responseJson)
                            if (responseJson.status === 201 || responseJson.status === 200) {
                                debugger
                                dispatch({ type: SAVE_SESSION,  payload: responseJson.data})

                            }
                            else if (responseJson.status === 401 || responseJson.status == 400 || responseJson.data.status == 1002 || responseJson.status == 500) dispatch({ type: SAVE_SESSION_FAIL, payload: responseJson.data.message })
                        })
                }
            })

    }
}


export const postImages = (postData,sessionID,userDetails) => {
    debugger
    return dispatch => {
        networkStatus()
            .then(isConnected => {
                if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                else {
                    dispatch({ type: CLEAR_STATE })
                    dispatch({ type: LOADER, payload: true })
                    APIServices(postData,sessionID+'/images', 'POST',userDetails.token,'multipart/form-data')
                        .then(responseJson => {
                            debugger
                            console.log("test===>", responseJson)
                            if (responseJson.status === 201 || responseJson.status === 200) {
                                debugger
                                dispatch({ type: SAVE_Image,  payload: responseJson.data})

                            }
                            else if (responseJson.status === 401) dispatch({ type: GET_CUSTOM_INFO_FAILURE, payload: responseJson.data.message })
                        })
                }
            })

    }
}



export const postReview = (postData,userDetails) => {
    debugger
    return dispatch => {
        networkStatus()
            .then(isConnected => {
                if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                else {
                    dispatch({ type: CLEAR_STATE })
                  
                    APIServices(JSON.stringify(postData), 'api/reviews', 'POST',userDetails.token)
                        .then(responseJson => {
                            debugger
                            console.log("test===>", responseJson)
                            if (responseJson.status === 201 || responseJson.status === 200) {
                                debugger
                                dispatch({ type: REVIEW_POST, payload: 'Review added sucessfully' })

                            }
                            else if (responseJson.status === 401) dispatch({ type: REVIEW_POST_FAIL, payload: responseJson.data.message })
                        })
                }
            })

    }
}


export const getGallery = (postData,userDetails) => {
    debugger
    return dispatch => {
        networkStatus()
            .then(isConnected => {
                if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                else {
                    dispatch({ type: CLEAR_STATE })
                  
                    APIServices({}, postData, 'GET',userDetails.token)
                        .then(responseJson => {
                            debugger
                            console.log("test===>", responseJson)
                            if (responseJson.status === 201 || responseJson.status === 200) {
                                debugger
                                dispatch({ type: GET_EDIT_TIMELINE, payload: responseJson.data})

                            }
                            else if (responseJson.status === 401) dispatch({ type: GET_EDIT_TIMELINE_FAIL, payload: responseJson.data.message })
                        })
                }
            })

    }
}




export const getTimelineList = (userDetails) => {
    debugger
    return dispatch => {
        networkStatus()
            .then(isConnected => {
                if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                else {
                    dispatch({ type: CLEAR_STATE })
                    dispatch({ type: LOADER, payload: true })
                    APIServices({}, 'api/sessions', 'GET',userDetails.token)
                        .then(responseJson => {
                            debugger
                            console.log("test===>", responseJson)
                            if (responseJson.status === 201 || responseJson.status === 200) {
                                debugger
                                dispatch({ type: GET_SESSION, payload: responseJson.data})

                            }
                            else if (responseJson.status === 401) dispatch({ type: FETCH_REVIEW_FAIL, payload: responseJson.data.message })
                        })
                }
            })

    }
}



export const delTimline = (data,userDetails) => {
    debugger
    return dispatch => {
        networkStatus()
            .then(isConnected => {
                if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                else {
                    dispatch({ type: CLEAR_STATE })
                    dispatch({ type: LOADER, payload: true })
                    APIServices({}, data, 'DELETE',userDetails.token)
                        .then(responseJson => {
                            debugger
                            console.log("test===>", responseJson)
                            if (responseJson.status === 200 ||responseJson.status === 204 ) {
                                debugger
                                dispatch({ type: DEL_SESSION, payload: responseJson.data })
                            }
                            else if (responseJson.status == 500 || responseJson.status == 404  | responseJson.data.status == 1002){
                                debugger
                                dispatch({ type: DEL_SESSION_FAIL, payload: responseJson })
                            } 
                        })
                }
            })

    }
}



export const ResetPass = (data) => {
    return dispatch => {
        console.log("resetPassword===>", data)
        if (!validatePassword(data.password).status) dispatch({ type: ERR_MESSAGE, payload: validatePassword(data.password).message })
        else if (!validatConfirmPass(data.password, data.password_confirmation).status) dispatch({ type: ERR_MESSAGE, payload: validatConfirmPass(data.password, data.confirmPass).message })
        else {
            networkStatus()
                .then(isConnected => {
                    if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                    else {
                        dispatch({ type: CLEAR_STATE })
                        dispatch({ type: LOADER, payload: true })
                        APIServices(data, 'reset/password', 'POST')
                            .then(responseJson => {
                                console.log("responseJson==>", JSON.stringify(responseJson))
                                if (responseJson.data.status_code === 200) dispatch({ type: RESET_PASS_SUCCESS, payload: responseJson.data })
                                else if (responseJson.data.status_code === 401) dispatch({ type: RESET_PASS_FAILURE, payload: responseJson.data.message ? responseJson.data.message : 'Something went wrong.' })
                                else if (responseJson.status === 422) dispatch({ type: RESET_PASS_FAILURE, payload: responseJson.data.email ? responseJson.data.email[0] : 'Something went wrong.' })
                                else if (responseJson.data.status === 1002) dispatch({ type: RESET_PASS_FAILURE, payload: responseJson.data.email ? responseJson.data.email[0] : 'Network Request Failed.' })
                            })
                    }
                })
        }
    }
}

export const VerifyAccount = (data) => {
    return dispatch => {
        networkStatus()
            .then(isConnected => {
                if (!isConnected) dispatch({ type: ERR_MESSAGE, payload: NetworkStatus })
                else {
                    debugger
                    dispatch({ type: CLEAR_STATE })
                    dispatch({ type: LOADER, payload: true })
                    APIServices(data, 'verify/account', 'POST')
                        .then(responseJson => {
                            console.log("responseJson==>", JSON.stringify(responseJson))
                            if (responseJson.data.status_code === 200) {
                                debugger
                                dispatch({ type: VERIFY_ACCOUNT_SUCCESS, payload: responseJson.data })
                                setToken("bareer_key", responseJson.data.user.access_token ? responseJson.data.user.access_token : null)

                            }
                            else if (responseJson.data.status_code === 401) dispatch({ type: VERIFY_ACCOUNT_FAILURE, payload: responseJson.data.message ? responseJson.data.message : 'Something went wrong.' })
                            else if (responseJson.status === 422) dispatch({ type: VERIFY_ACCOUNT_FAILURE, payload: responseJson.data.email ? responseJson.data.email[0] : 'Something went wrong.' })
                            else if (responseJson.data.status === 1002) dispatch({ type: VERIFY_ACCOUNT_FAILURE, payload: responseJson.data.email ? responseJson.data.email[0] : 'Network Request Failed.' })
                        })
                }
            })
    }
}





export const ClearAction = () => {
    return dispatch => dispatch({ type: CLEAR_STATE })
}