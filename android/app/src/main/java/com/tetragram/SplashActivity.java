package com.tetragram;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import org.devio.rn.splashscreen.SplashScreen; // here
import android.os.Bundle; // here
import com.facebook.react.ReactActivity;
public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.launch_screen);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent=new Intent(SplashActivity.this, MainActivity.class)  ;
                startActivity(intent);
                finish();
            }
        },4000);
    }
}
