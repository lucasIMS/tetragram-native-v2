/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src/App';
import {name as appName} from './app.json';
//import KeyboardManager from 'react-native-keyboard-manager'

AppRegistry.registerComponent(appName, () => App);
console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];
console.ignoredYellowBox = ['Warning:'];
//KeyboardManager.setEnable(true);
